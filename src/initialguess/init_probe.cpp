#include "./init_probe.hpp"

#define sq(A) A*A
#define Npx _Wconf._num_pixels


template <>
void prguess<double>::prinit_ph(int npr, int pp) {
	std::cout << "Initializing pinhole probe... THIS WILL CRASH!" << std::endl;
	double position;
	int xx,yy;
	/**Initialize pinhole**/
	for(yy=-Npx/2; yy<Npx/2; yy++) {
		for(xx=-Npx/2; xx<Npx/2; xx++) {
			position=sq(_Wconf._dPX*double(xx))+sq(_Wconf._dPX*double(yy));
			if(4*position<sq(_iconf._ipro_diameter)) {
				PGUESS[npr][pp][xx+Npx/2+(yy+Npx/2)*Npx]=1.0;
			} else PGUESS[npr][pp][xx+Npx/2+(yy+Npx/2)*Npx]=0.00001;
		}
	}

	/**Propagate to sample plane**/
	fftw_allprec<double> PlanFFT( Npx );

	PlanFFT.execute_fw( PGUESS[npr][pp].dvec );
	propagate_angspectr(_iconf._ipro_distance,npr,pp);
	PlanFFT.execute_bw( PGUESS[npr][pp].dvec );
	fftw_norm( PGUESS[npr][pp].dvec, Npx );
	fftw_norm( PGUESS[npr][pp].dvec, Npx );

}



