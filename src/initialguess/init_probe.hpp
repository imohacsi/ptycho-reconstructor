#ifndef PTREC_INIT_PROBE_H_
#define PTREC_INIT_PROBE_H_

#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <complex>
#include <fftw3.h>
#include <random>
#include <sys/stat.h>
#include "./guess_format.hpp"
#include "../utilities/fft_utils.hpp"
#include "../utilities/fileio.hpp"
#include "../reconstruction/worker_config.hpp"
#include "../utilities/h5_utils.hpp"

#define sq(A) A*A
#define Npx _Wconf._num_pixels


/**Class for generating initial probe values
* The expected initialization order is:
* IMG -> PRO -> OBJ -> PSI
*/
template <typename TY>
class prguess {
protected:
	ptyconf _Wconf;
	ptydata<TY>   _rdata;
	guess_format   _iconf;
	trifield<complex<TY>> PGUESS;

	TY drand32();
    TY scalingmode(std::string, int);

	void prinit_switch();
	void prinit_fzp(int,int);
	void prinit_mll(int,int);
	void prinit_ph(int,int);
	void prinit_gs(int,int);
	void prinit_rnd(int,int);
	void prinit_load(int);
	void prinit_load_h5(int);
	void prinit_salt_probes(int);

	std::array<double,5> get_modeplacement(int);
	void get_totalint();
	void scale_probe(int,double);
	void propagate_probe(int);
	void orthogonalize_modes(int);
	void propagate_angspectr(double,int,int);

public:
	prguess(ptyconf wconf, ptydata<TY> rdata, guess_format iconf): _Wconf(wconf),_rdata(rdata),_iconf(iconf) { };
	trifield<complex<TY>> get_probe_guess();
};

template <typename TY>
trifield<complex<TY>> prguess<TY>::get_probe_guess() {
	printf("%s\tInitializing probe...%s\n",A_C_CYAN,A_C_RESET);
	fflush(stdout);
	PGUESS.init( _Wconf._num_pro, _Wconf._num_pmodes, Npx,Npx );
	prinit_switch();
	printf("%s\t\t...done.%s\n",A_C_GREEN,A_C_RESET);
	fflush(stdout);
	return PGUESS;
}

/**Select the probe initialization method**/
template <typename TY>
void prguess<TY>::prinit_switch() {
	srand(123);

	for(int npr=0; npr<_Wconf._num_pro; npr++) {
		for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
			if(_iconf._ipro_init_type=="ZP") { prinit_fzp(npr,pp); }
			else if(_iconf._ipro_init_type=="PH") { prinit_ph(npr,pp); }
			else if(_iconf._ipro_init_type=="GS") { prinit_gs(npr,pp); }
			else if(_iconf._ipro_init_type=="RND") {
				if(pp==0) { prinit_fzp(npr,pp);	}
				else { prinit_rnd(npr,pp);	}
			}
		}
		if(_iconf._ipro_init_type=="LOAD") {
			prinit_load(npr);
//            prinit_load_h5(npr);
		}

        /**Post-initialization operations**/
		if(_iconf._ipro_do_saltmodes) { prinit_salt_probes(npr); }
		if(_iconf._ipro_do_orthmodes) { orthogonalize_modes(npr); }
		if(_iconf._ipro_do_propagate) { propagate_probe(npr); }

		if(_iconf._ipro_do_intnorm) { scale_probe(npr,1.4*_Wconf._avg_counts); }
	}
}

/**Phase shift from angular spectrum propagator**/
template <typename TY>
void prguess<TY>::propagate_angspectr(double Zdist, int npr, int pp) {
    const std::complex<TY> iI(0.0,1.0);
    const std::complex<TY> iR(1.0,0.0);
    const double PI=4.0*atan(1.0);
    const double TR=_Wconf._dPX*(double)Npx;
    const double k0=2.0*PI/_Wconf._lambda;
    double Qx,Qy,Qxy;
    int xx,yy;
    std::complex<double> mu,ASP;
    #pragma omp parallel for private(xx,mu,Qx,Qy,Qxy,ASP)
    for(yy=0; yy<Npx; yy++) {
        if(yy<Npx/2) { Qy=2.0*PI*double(yy)/TR; }
        else {         Qy=-(2.0*PI*double(Npx-1-yy))/TR; }
        for(xx=0; xx<Npx; xx++) {
            if(xx<Npx/2) { Qx=2.0*PI*double(xx)/TR; }
            else {         Qx=-(2.0*PI*double(Npx-1-xx))/TR; }
            Qxy=sqrt((Qx*Qx)+(Qy*Qy));
            mu=sqrt(iR-((Qxy*Qxy)/(k0*k0)));
            ASP=exp(iI*k0*Zdist*mu);
            _rdata.PRO[npr][pp].dvec[xx+yy*Npx]*=ASP;
        }
    }
}

template <typename TY>
std::array<double,5> prguess<TY>::get_modeplacement(int pp) {
    double xprshift=0.0,yprshift=0.0,zprshift=0.0, vel_x=0.0,vel_y=0.0;
	double flystep = _iconf._ipro_mmode_speed/(_Wconf._dPX*double(_Wconf._num_pmodes));
	double focstep = _iconf._ipro_mmode_focal/(_Wconf._dPX*double(_Wconf._num_pmodes));

	/**Default: FOC**/
    zprshift = double(2.0*pp+1.0-_Wconf._num_pmodes)/2.0 * focstep;


	if( _iconf._ipro_mmode_placement == "HOR" ){
        xprshift = double(2.0*pp+1.0-_Wconf._num_pmodes)/2.0 * flystep;
        yprshift = 0.0;
        zprshift = 0.0;
        vel_x = flystep/2.0;
	}
	if( _iconf._ipro_mmode_placement == "VER" ){
        xprshift = 0.0;
        yprshift = double(2.0*pp+1.0-_Wconf._num_pmodes)/2.0 * flystep;
        zprshift = 0.0;
        vel_y = flystep/2.0;
	}
	if( _iconf._ipro_mmode_placement == "FOC" ){
        xprshift = 0.0;
        yprshift = 0.0;
        zprshift = double(2.0*pp+1.0-_Wconf._num_pmodes)/2.0 * focstep;
	}
	if( _iconf._ipro_mmode_placement == "CROSS" ){
        int ring = (pp+3)/4;
        int r_max = (_Wconf._num_pmodes+2)/4;
        flystep = _iconf._ipro_mmode_speed/(_Wconf._dPX*double(r_max));
        focstep = _iconf._ipro_mmode_focal/(_Wconf._dPX*double(r_max));
        zprshift = 0.0;
        if( pp==0 ){
            xprshift = 0.0;
            yprshift = 0.0;
        }else{
            if( pp%4 == 1 ){
                xprshift = double(ring) * flystep;
                yprshift = 0.0;
                vel_x = flystep/2.0;
            }else if( pp%4 == 2){
                xprshift = 0.0;
                yprshift = double(ring) * flystep;
                vel_y = flystep/2.0;
            }else if( pp%4 == 3){
                xprshift = -double(ring) * flystep;
                yprshift = 0.0;
                vel_x = flystep/2.0;
            }else if( pp%4 == 0){
                xprshift = 0.0;
                yprshift = -double(ring) * flystep;
                vel_y = flystep/2.0;
            }
        }
	}

    return {xprshift,yprshift,zprshift,vel_x,vel_y};
}

//Initialize pinhole illumination probe
template <typename TY>
void prguess<TY>::prinit_ph(int npr, int pp) {
    printf("%s\tERROR: Pinhole probe is not implemented for this datatype!%s\n",A_C_YELLOW,A_C_RESET );
    fflush(stdout);
    exit(-1);
}



/**Initialize zone plate probe with lens-like phasemap.**/
template <typename TY>
void prguess<TY>::prinit_fzp(int npr, int pp) {
	printf("%s\t\tInitializing zone plate probe...%s\n",A_C_CYAN,A_C_RESET);
	fflush(stdout);
	const std::complex<TY> iI(0.0,1.0);
	const double PI=4.0*atan(1.0);
	const double k0=2.0*PI/_Wconf._lambda;
	double rposition;
	int xx,yy;

    /**Geting probe mode placement**/
    std::array<double,5> shift = this->get_modeplacement(pp);
	double z_foc = _iconf._ipro_distance + shift[2];
//	double velx = shift[3];
//	double vely = shift[4];
//	printf("\t\tProbe mode %d in %s placement at pixels: %g %g %g velx: %0.3g vely: %0.3g\n",pp,_iconf._ipro_mmode_placement.c_str(), shift[0],shift[1],shift[2], velx, vely);
	printf("\t\tProbe mode %d in %s placement at pixels: %g %g %g\n",pp,_iconf._ipro_mmode_placement.c_str(), shift[0],shift[1],shift[2]);

	if( pp==0 ){
        #pragma omp parallel for private(xx,rposition)
        for(yy=-Npx/2; yy<Npx/2; yy++) {
            for(xx=-Npx/2; xx<Npx/2; xx++) {
                rposition=sqrt(sq(_Wconf._dPX*(double(xx)+shift[0]))+sq(_Wconf._dPX*(double(yy)+shift[1])));
                PGUESS[npr][pp][xx+Npx/2+(yy+Npx/2)*Npx]=(TY)(erf(10.0*(_iconf._ipro_diameter/2.0-rposition)/_iconf._ipro_diameter)+1.00000000001)*exp(iI*(TY)(k0*sq(rposition)/(2.0*z_foc)));
            }
        }
	}else{
        #pragma omp parallel for private(xx,rposition)
        for(yy=-Npx/2; yy<Npx/2; yy++) {
            for(xx=-Npx/2; xx<Npx/2; xx++) {
                rposition=sqrt(sq(_Wconf._dPX*(double(xx)+shift[0]))+sq(_Wconf._dPX*(double(yy)+shift[1])));
                PGUESS[npr][pp][xx+Npx/2+(yy+Npx/2)*Npx]+=(TY)(erf(10.0*(_iconf._ipro_diameter/2.0-rposition)/_iconf._ipro_diameter)+(TY)1.00000000001)*(  exp(iI*(TY)(k0*sq(rposition)/(2.0*z_foc)))  );      //JustShift
            }
        }
	}
}

/**Initialize gaussian probe with lens-like phasemap.**/
template <typename TY>
void prguess<TY>::prinit_gs(int npr, int pp) {
	const std::complex<double> iI(0.0,1.0);
	const double PI=4.0*atan(1.0);
	const double k0=2.0*PI/_Wconf._lambda;
	double rposition;

    /**Geting probe mode placement**/
    std::array<double,5> shift = this->get_modeplacement(pp);
	double z_foc = _iconf._ipro_distance + shift[2];

	for(int yy=-Npx/2; yy<Npx/2; yy++) {
		for(int xx=-Npx/2; xx<Npx/2; xx++) {
			rposition=sqrt(sq(_Wconf._dPX*(double(xx)+shift[0]))+sq(_Wconf._dPX*(double(yy)+shift[1])));
			PGUESS[npr][pp][xx+Npx/2+(yy+Npx/2)*Npx]=exp(-rposition/(0.3606*sq(_iconf._ipro_diameter)))*exp(iI*k0*rposition/(2.0*z_foc));
		}
	}
}

template <typename TY>
TY prguess<TY>::drand32() {
	return TY(rand())/TY(RAND_MAX);
}

template <typename TY>
void prguess<TY>::prinit_salt_probes(int npr) {
	const std::complex<TY> iI(0.0,1.0);
	const double PI=4.0*atan(1.0);

	for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
		for(int xy=0; xy<Npx*Npx; xy++) {
			PGUESS[npr][pp][xy]*=(TY)0.9+((TY)0.1)*exp( iI*(TY)(2.0*PI*drand32()) );
		}
	}
}

template <typename TY>
void prguess<TY>::prinit_rnd(int npr, int pp) {
	const std::complex<TY> iI(0.0,1.0);
	const double PI=4.0*atan(1.0);

	for(int xy=0; xy<Npx*Npx; xy++) {
		PGUESS[npr][pp][xy]=(TY)(0.04*drand32())*exp( iI*(TY)(PI*drand32()) );
	}
}

template <typename TY>
void prguess<TY>::prinit_load(int npr) {
	char prname[512];

	for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
		sprintf(prname,"./Reconstructions/Probe_B%d_P%d_M%d_N%d.bin",0,0,pp,(int)Npx);
		printf("\t\tLoading previous probe: %s\n",prname);
		fflush(stdout);
		try{
            LoadBinFile(PGUESS[npr][pp].dvec,Npx*Npx,prname);
		}catch(std::exception& e){
            prinit_fzp(npr,pp);
		}
	}
}


template <typename TY>
void prguess<TY>::prinit_load_h5(int npr) {
	char ipath[512];
	h5py loadfile("./Reconstructions/OnP_B0.h5","r");
	for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
		sprintf(ipath,"/probe_p%d_m%d",0,pp); //Always reads the 0th probe!
		loadfile.dset_read_cdouble(PGUESS[npr][pp].dvec,ipath);
	}
}


template <typename TY>
TY prguess<TY>::scalingmode(std::string RULE, int pp) {
	double modescale=1.0;
	if(RULE=="FLAT") { modescale=1.0; }
	else if(RULE=="LINEAR") { modescale=1.0/double(1+pp); }
	else if(RULE=="QUADR ") { modescale=1.0/sq(double(1+pp)); }
	else if(RULE=="SYM_LINEAR") { modescale=1.0/double(1.0+(pp+1)/2); }
	else if(RULE=="SYM_QUADR") { modescale=1.0/sq(double(1.0+(pp+1)/2)); }
	else { printf("%s\tERROR: Unknown probe scaling: %s%s\n",A_C_RED,RULE.c_str(),A_C_RESET); exit(-1); }
	return (TY)modescale;
}

/**Scale total probe intensity on detector to target int**/
template <typename TY>
void prguess<TY>::scale_probe(int npr, double targetint) {
    printf("%s\tWARNING: Probe scaling is not implemented for this datatype!%s\n",A_C_YELLOW,A_C_RESET );
    fflush(stdout);
}

template <>
inline void prguess<double>::scale_probe(int npr, double targetint) {
	Field2D<complex<double>> wFIELD(Npx,Npx);
	fftw_allprec<double> PlanFFT( wFIELD.dvec, Npx );

	for(int64_t pp=0; pp<_Wconf._num_pmodes; pp++) {
		double modescale=scalingmode(_iconf._ipro_scaling_rule,pp);
		PGUESS[npr][pp].par_mul(modescale);
	}

	double OrigProbeInt=0.0;
	for(int64_t pp=0; pp<_Wconf._num_pmodes; pp++) {
		wFIELD.imp( PGUESS.maps[npr][pp] );
		PlanFFT.execute_fw();
		fftw_norm(wFIELD.dvec,Npx);
		OrigProbeInt+= sum( wFIELD );
	}

	double prInorm=sqrt(targetint/OrigProbeInt);

	for(int64_t pp=0; pp<_Wconf._num_pmodes; pp++) {
		PGUESS[npr][pp].par_mul(prInorm);
	}
}


/**Propagate probe along the optical axis**/
template <typename TY>
void prguess<TY>::propagate_probe(int npr ) {
    printf("%s\tWARNING: Probe propagation is not implemented for this datatype!%s\n",A_C_YELLOW,A_C_RESET );
    fflush(stdout);
}


template <>
inline void prguess<double>::propagate_probe(int npr) {
	fftw_allprec<double> PlanFFT( Npx );
	for(int64_t pp=0; pp<_Wconf._num_pmodes; pp++) {
        PlanFFT.execute_fw( PGUESS[npr][pp].dvec );
		propagate_angspectr( _iconf._ipro_prop_distance,npr,pp);
        PlanFFT.execute_bw( PGUESS[npr][pp].dvec );
        fftw_norm( PGUESS[npr][pp].dvec, Npx );
        fftw_norm( PGUESS[npr][pp].dvec, Npx );
        }
}

/**Orthogonalize probe modes for better convergence**/
template <typename TY>
void prguess<TY>::orthogonalize_modes(int npr) {
    if(_Wconf._num_pmodes<=1) { return; }

    printf("Orthogonalizing probes using the Gram-Schmidt process...\n");

    complex<TY> bufferU;
    TY bufferD;
    for(int pp=1; pp<_Wconf._num_pmodes; pp++) {
        for(int proj=0; proj<pp; proj++) {
            bufferU=0;
            bufferD=0;
            for(int xy=0; xy<Npx*Npx; xy++) {
                bufferU+=PGUESS[npr][pp][xy]*conj(PGUESS[npr][proj][xy]);
                bufferD+=norm(PGUESS[npr][proj][xy]);
            }
            #pragma omp parallel for
            for(int xy=0; xy<Npx*Npx; xy++) {
                PGUESS[npr][pp][xy]-= bufferU/bufferD*PGUESS[npr][proj][xy];
            }
        }
    }
}

#undef Npx
#endif // DESYPTYCHO_INITIAL_PROBE_H_

