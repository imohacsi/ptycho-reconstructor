#ifndef DESYPTYCHO_INITIAL_GUESS_H_
#define DESYPTYCHO_INITIAL_GUESS_H_

#include "../preproc_data/preproc_data.hpp"
#include "../reconstruction/worker_config.hpp"
#include "./guess_format.hpp"
#include "./init_probe.hpp"
#include "./init_object.hpp"
#include "./init_psi.hpp"

/**Ensemble class to calculate initial guesses for the probe, object and views. **/
template <typename TY>
class initial_guess {
    private:
        void get_objsize();
        void init_pro();
        void init_obj();
        void init_psi();
        void meminfo();
        void print_initial_guess();

    public:
        ptyconf _Wconf;
        ptydata<TY>   _rdata;
        guess_format    _conf;

        void make_initial_guess();
        initial_guess(ptyconf conf, ptydata<TY> rdata, guess_format igconf): _Wconf(conf),_rdata(rdata),_conf(igconf) {
            _conf._do_init_pro=true;
            _conf._do_init_obj=true;
            _conf._do_init_psi=true;
        };

        void set_initlist(int pro, int obj, int psi) {
            _conf._do_init_pro=pro;
            _conf._do_init_obj=obj;
            _conf._do_init_psi=psi;
        }
};


/**Run the corresponding algorithms to determine a
*   reasonable initial guess for the probe, object
*   and the views.
*/
template <typename TY>
void initial_guess<TY>::make_initial_guess() {
	printf("Calculating initialization parameters:\n");
	fflush(stdout);
	meminfo();
	get_objsize();

	printf("Calculating initial guess...\n");
	if(_conf._do_init_pro) {
		prguess<TY> probeguess(_Wconf,_rdata,_conf);
		_rdata.PRO.free();
		_rdata.PRO=probeguess.get_probe_guess();
	}

	if(_conf._do_init_obj) {
		obguess<TY> objectguess(_Wconf,_rdata,_conf);
		_rdata.OBJ.free();
		_rdata.OBJ=objectguess.get_object_guess();
	}

	if(_conf._do_init_psi) {
		psiguess<TY> viewguess(_Wconf,_rdata,_conf);
		_rdata.PSI.free( );
		_rdata.PSI=viewguess.get_psi_guess();
	}

	_rdata.BGROUND=(double*)calloc(_Wconf._num_pixels*_Wconf._num_pixels,sizeof(double));
	print_initial_guess();
}


template <typename TY>
void initial_guess<TY>::meminfo() {
	double memsize=0;
	memsize+=4.0*double(_Wconf._num_frames)*double(_Wconf._num_pixels*_Wconf._num_pixels);                                          /**For raw image data*/
	memsize+=16.0*double(_Wconf._num_frames*_Wconf._num_pmodes*_Wconf._num_omodes)*double(_Wconf._num_pixels*_Wconf._num_pixels);    /**For view data (PSI)*/
	memsize+=16.0*double(_Wconf._num_pro*_Wconf._num_pmodes)*double(_Wconf._num_pixels*_Wconf._num_pixels);                          /**For probes*/
	memsize+=16.0*double(_Wconf._num_obj*_Wconf._num_omodes)*double(_Wconf._OX*_Wconf._OY);                                          /**For objects*/
	memsize+=16.0*double(_Wconf._num_THREADS)*double(_Wconf._num_pixels*_Wconf._num_pixels);                                         /**For fields*/
	memsize+=16.0*double(_Wconf._num_THREADS)*double(_Wconf._num_pixels*_Wconf._num_pixels);                                         /**For sumint*/
	memsize+=16.0*double(_Wconf._num_THREADS*_Wconf._num_pmodes*_Wconf._num_omodes)*double(_Wconf._num_pixels*_Wconf._num_pixels);   /**For oxp*/
	memsize+=16.0*double(_Wconf._num_THREADS*_Wconf._num_pmodes*_Wconf._num_omodes)*double(_Wconf._num_pixels*_Wconf._num_pixels);   /**For buffer*/

	printf("%sEstimated memory size for this batch is: %.3f MB%s\n",A_C_YELLOW,memsize/(1048576.0),A_C_RESET);
	printf("%sPlease make sure you have enough RAM!%s\n",A_C_YELLOW,A_C_RESET);
	fflush(stdout);
}


/**Re-Evaluate and assign the object size to the
*   particular batch. By this time the coordinates
*   are already in pixels. This will simplify the
*   position refinement process!
*/
template <typename TY>
void initial_guess<TY>::get_objsize() {

	/**Allocating and initializing personal sizes for all objects**/
	double omincord[_Wconf._num_obj][2];
	double omaxcord[_Wconf._num_obj][2];
	for(int64_t nob=0; nob<_Wconf._num_obj; nob++) {
		omincord[nob][0] = +1e300;
		omincord[nob][1] = +1e300;
		omaxcord[nob][0] = -1e300;
		omaxcord[nob][1] = -1e300;
	}

	/**Calculating the lowest coordinate values for each object*/
	for(int64_t ff=0; ff<_Wconf._num_frames; ff++) {
		omincord[_rdata._olist[ff]][0] = min( omincord[_rdata._olist[ff]][0], _rdata._pos2D[ff][0]);
		omincord[_rdata._olist[ff]][1] = min( omincord[_rdata._olist[ff]][1], _rdata._pos2D[ff][1]);
	}

	/**Shifting positions to start at border.**/
	for(int64_t ff=0; ff<_Wconf._num_frames; ff++) {
		_rdata._pos2D[ff][0]=(_rdata._pos2D[ff][0]-omincord[_rdata._olist[ff]][0])+double(_Wconf._border);
		_rdata._pos2D[ff][1]=(_rdata._pos2D[ff][1]-omincord[_rdata._olist[ff]][1])+double(_Wconf._border);
	}

	/**Calculating the scan area in pixels for each object*/
	for(int64_t ff=0; ff<_Wconf._num_frames; ff++) {
		omaxcord[_rdata._olist[ff]][0] = max( omaxcord[_rdata._olist[ff]][0], _rdata._pos2D[ff][0]);
		omaxcord[_rdata._olist[ff]][1] = max( omaxcord[_rdata._olist[ff]][1], _rdata._pos2D[ff][1]);
	}

	/**Taking the largest object for the actual reconstruction**/
	double mincord[2] = {omincord[0][0],omincord[0][1]};
	double maxcord[2] = {omaxcord[0][0],omaxcord[0][1]};
	for(int64_t nob=0; nob<_Wconf._num_obj; nob++) {
		maxcord[0] = max( maxcord[0],omaxcord[nob][0]);
		maxcord[1] = max( maxcord[1],omaxcord[nob][1]);
		mincord[0] = min( mincord[0],omincord[nob][0]);
		mincord[1] = min( mincord[1],omincord[nob][1]);
	}


	/**Adding reconstruction margin to the actually scanned area*/
	maxcord[0]+=double(_Wconf._border)+double(_Wconf._num_pixels);
	maxcord[1]+=double(_Wconf._border)+double(_Wconf._num_pixels);
	_Wconf._OX=16*ceil(maxcord[0]/16.0);
	_Wconf._OY=16*ceil(maxcord[1]/16.0);

	fprintf(stdout,"%s\nReconstruction: %s \n%s",A_C_GREEN, _Wconf._batch_name, A_C_RESET);
	fprintf(stdout,"\t%s%d pixels, %f nm/pixel \tFoV: %g%s\n",A_C_CYAN, (int)_Wconf._num_pixels, 1e9*_Wconf._dPX, 1e6*_Wconf._num_pixels*_Wconf._dPX, A_C_RESET );
	printf("\tCurrent object size is: %d x%d px ",(int)_Wconf._OX,(int)_Wconf._OY);
	printf("\n\t\t(mapped: %d x%d)\n", (int)(_Wconf._OX-_Wconf._num_pixels-2*_Wconf._border), (int)(_Wconf._OY-_Wconf._num_pixels-2*_Wconf._border) );
	printf("\tObject area is: %f x%f um ",1e6*_Wconf._OX*_Wconf._dPX,1e6*_Wconf._OY*_Wconf._dPX);
	printf("\n\t\t(mapped: %f x%f)\n",1e6*_Wconf._dPX*(_Wconf._OX-_Wconf._num_pixels-2*_Wconf._border),1e6*_Wconf._dPX*(_Wconf._OY-_Wconf._num_pixels-2*_Wconf._border));

	double mappedpx=double((_Wconf._OX-_Wconf._num_pixels-2*_Wconf._border)*(_Wconf._OY-_Wconf._num_pixels-2*_Wconf._border));
	printf("%s\tAverage of %g photons per pattern\n\t\t( %g photons per mapped pixel)%s\n",A_C_CYAN,double(_Wconf._avg_counts), double(_Wconf._num_frames)*_Wconf._avg_counts/mappedpx,A_C_RESET);

	/**Assigning 1D coordinates*/
	for(int ff=0; ff<_Wconf._num_frames; ff++) {
		_rdata._pos1D[ff]=int(round(_rdata._pos2D[ff][0]))+int(round(_rdata._pos2D[ff][1]))*_Wconf._OX;
	}
}


/**If debugging mode active, prints out the initial
*   guesses for each batch.
*/
template <typename TY>
void initial_guess<TY>::print_initial_guess() {
	char filename[512];
	for(int npr=0; npr<_Wconf._num_pro; npr++) {
		for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
			sprintf(filename,"./output/InitProbe_P%d_M%d_I.tif",npr,pp);
			ImWrite(_rdata.PRO[npr][pp],"lin","int","TIFF",filename);
			sprintf(filename,"./output/InitProbe_P%d_M%d_P.tif",npr,pp);
			ImWrite(_rdata.PRO[npr][pp],"lin","arg","TIFF",filename);
		}
	}

	for(int nob=0; nob<_Wconf._num_obj; nob++) {
		for(int oo=0; oo<_Wconf._num_omodes; oo++) {
			sprintf(filename,"./output/InitObject_O%d_M%d_I.tif",(int)nob,(int)oo);
			ImWrite(_rdata.OBJ[nob][oo],"lin","int","TIFF",filename);
			sprintf(filename,"./output/InitObject_O%d_M%d_P.tif",(int)nob,(int)oo);
			ImWrite(_rdata.OBJ[nob][oo],"lin","arg","TIFF",filename);
		}
	}

	if(_conf._do_init_psi) {
		sprintf(filename,"./output/InitPsi1st_I.tif");
		ImWrite(_rdata.PSI[0][0],"lin","int","TIFF",filename);
		sprintf(filename,"./output/InitPsi1st_P.tif");
		ImWrite(_rdata.PSI[0][0],"lin","arg","TIFF",filename);
	}

}


#endif // DESYPTYCHO_INITIAL_GUESS_H_
