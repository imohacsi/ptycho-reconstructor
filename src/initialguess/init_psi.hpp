#ifndef DESYPTYCHO_INITIAL_VIEWS_H_
#define DESYPTYCHO_INITIAL_VIEWS_H_

#include <iostream>
#include <string>
#include <vector>
#include <complex>
#include <fftw3.h>
#include <random>
#include "./guess_format.hpp"
#include "../utilities/fft_utils.hpp"
#include "../utilities/fileio.hpp"
#include "../reconstruction/worker_config.hpp"


/**Class for views initialization settings
*   from the config file.
*/
template <typename TY>
class psiguess {
  protected:
    ptyconf _Wconf;
    ptydata<TY>   _rdata;
    guess_format   _iconf;
    trifield<std::complex<TY>> WGUESS;
    int64_t _num_psi_frames;

    double drand32();

    void psinit_switch();
    void psinit_pro();
    void psinit_oxp();

  public:
    psiguess(ptyconf wconf, ptydata<TY> rdata, guess_format iconf): _Wconf(wconf),_rdata(rdata),_iconf(iconf) {};
    trifield<std::complex<TY>> get_psi_guess();
};

template <typename TY>
trifield<std::complex<TY>> psiguess<TY>::get_psi_guess() {
	printf("%s\tInitializing views...%s ",A_C_CYAN,A_C_RESET);
	fflush(stdout);

	_num_psi_frames=0;
	if(_iconf._ipsi_batchtype=="BATCH") {
		_num_psi_frames = _Wconf._num_frames;
	}
	if(_iconf._ipsi_batchtype=="MINIBATCH") {
		_num_psi_frames = min(_Wconf._batch_size,_Wconf._num_frames);
	}

	WGUESS.init(_Wconf._num_omodes*_Wconf._num_pmodes,_num_psi_frames,_Wconf._num_pixels,_Wconf._num_pixels);
	psinit_switch();
	printf("%s\t...done.%s\n",A_C_GREEN,A_C_RESET);
	fflush(stdout);
	return WGUESS;
}

template <typename TY>
void psiguess<TY>::psinit_switch() {
	srand(123);

	for(int oopp=0; oopp<_Wconf._num_pmodes*_Wconf._num_omodes; oopp++) {
		if(_iconf._ipsi_init_type=="PRO") { psinit_pro(); }
		else if(_iconf._ipsi_init_type=="OXP") {	psinit_oxp(); }
		else{ printf("%s\n\tERROR: Unknown method for initialization of PSI: %s%s\n",A_C_RED,_iconf._ipsi_init_type.c_str(),A_C_RESET); exit(-1); }
	}
}

template <typename TY>
void psiguess<TY>::psinit_pro() {
	for(int oo=0; oo<_Wconf._num_omodes; oo++) {
		for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
			for(int ff=0; ff<_num_psi_frames; ff++) {
				WGUESS[pp+oo*_Wconf._num_pmodes][ff] =_rdata.PRO[_rdata._plist[ff]][pp];
			}
		}
	}
}

template <typename TY>
void psiguess<TY>::psinit_oxp() {
	for(int oo=0; oo<_Wconf._num_omodes; oo++) {
		for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
			for(int ff=0; ff<_num_psi_frames; ff++) {
				WGUESS[pp+oo*_Wconf._num_pmodes][ff].impsel( _rdata.OBJ[_rdata._olist[ff]][oo],_rdata._pos1D[ff] );
				WGUESS[pp+oo*_Wconf._num_pmodes][ff]*=_rdata.PRO[_rdata._plist[ff]][pp];
			}
		}
	}
}


#endif // DESYPTYCHO_INITIAL_VIEWS_H_
