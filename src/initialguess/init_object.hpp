#ifndef DESYPTYCHO_INITIAL_OBJECT_H_
#define DESYPTYCHO_INITIAL_OBJECT_H_

#include <iostream>
#include <string>
#include <vector>
#include <complex>
#include <fftw3.h>
#include <random>
#include "../utilities/fft_utils.hpp"
#include "../utilities/fileio.hpp"
#include "./guess_format.hpp"
#include "../reconstruction/worker_config.hpp"


/**Class for generating initial object layout
* The expected initialization order is:
* IMG -> PRO -> OBJ -> PSI
*/
template <typename TY>
class obguess {
private:
	ptyconf   _Wconf;
	ptydata<TY>     _rdata;
	guess_format    _iconf;
	trifield<complex<TY>> OGUESS;

	TY drand32();

	void obinit_switch();
	void obinit_rnd(int,int);
	void obinit_stxm(int,int);
	void obinit_flat(int,int);
	void obinit_load(int);

public:
	obguess(ptyconf wconf, ptydata<TY> rdata, guess_format iconf): _Wconf(wconf),_rdata(rdata),_iconf(iconf) { };
	trifield<complex<TY>> get_object_guess();
};


template <typename TY>
trifield<complex<TY>> obguess<TY>::get_object_guess() {
	printf("%s\tInitializing object...%s ",A_C_CYAN,A_C_RESET);
	fflush(stdout);
	OGUESS.init(_Wconf._num_obj,_Wconf._num_omodes,_Wconf._OX,_Wconf._OY);
	obinit_switch();

	printf("%s\t...done.%s\n",A_C_GREEN,A_C_RESET);
	fflush(stdout);
	return OGUESS;
}

template <typename TY>
void obguess<TY>::obinit_switch() {
	srand(123);

	for(int nob=0; nob<_Wconf._num_obj; nob++) {
		for(int oo=0; oo<_Wconf._num_omodes; oo++) {
			if(_iconf._iobj_init_type=="RND") {
				obinit_rnd(nob,oo);
			}
			if(_iconf._iobj_init_type=="FLAT") {
				obinit_flat(nob,oo);
			}
			if(_iconf._iobj_init_type=="LOAD") {
				obinit_load(nob);
			}
		}
	}
}

template <typename TY>
TY obguess<TY>::drand32() {
	return TY(rand())/TY(RAND_MAX);
}

template <typename TY>
void obguess<TY>::obinit_flat(int nob,int oo) {
	OGUESS[nob][oo]= 0.7;
}

template <typename TY>
void obguess<TY>::obinit_rnd(int nob, int oo) {
	const std::complex<TY> iI(0.0,1.0);
	const TY PI=4.0*atan(1.0);
	for(int xy=0; xy<_Wconf._OX*_Wconf._OY; xy++) {
		OGUESS[nob][oo][xy]=(TY)0.5+((TY)0.5)*exp( iI*(TY)(2.0*PI*drand32()) );
	}
}

template <typename TY>
void obguess<TY>::obinit_load(int nob) {
    char obname[512];
    for(int oo=0; oo<_Wconf._num_omodes; oo++) {
        sprintf(obname,"./Reconstructions/Object_%02d_%dpx.bin",(int)oo,(int)_Wconf._num_pixels);
        LoadBinFile(_rdata.OBJ[nob][oo].dvec,_Wconf._OX*_Wconf._OY,obname);
    }
}


#endif // DESYPTYCHO_INITIAL_OBJECT_H_

