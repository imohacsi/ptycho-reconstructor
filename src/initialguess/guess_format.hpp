#ifndef PTYREC_GUESS_FORMAT_H_
#define PTYREC_GUESS_FORMAT_H_

class guess_format {
public:
	bool _do_init_psi;
	bool _do_init_pro;
	bool _do_init_obj;

	/**Probe initial guess**/
	std::string _ipro_init_type;
	std::string _ipro_scaling_rule;
	std::string _ipro_mmode_placement;
	double _ipro_prop_distance = 0.0;
	double _ipro_distance;
	double _ipro_diameter;
	double _ipro_mmode_speed;
	double _ipro_mmode_focal;
	bool _ipro_do_propagate = 0;
	bool _ipro_do_intnorm;
	bool _ipro_do_saltmodes;
	bool _ipro_do_orthmodes;

	/**View initial guess**/
	std::string _iobj_init_type;

	/**View initial guess**/
	std::string _ipsi_init_type;
	std::string _ipsi_batchtype;
};

#endif // PTYREC_GUESS_FORMAT_H_
