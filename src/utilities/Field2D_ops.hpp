#ifndef PTYREC_UTILS_FIELD2D_OPS_HPP_
#define PTYREC_UTILS_FIELD2D_OPS_HPP_

#include <cstdint>
#include <vector>
#include <cmath>
#include <fftw3.h>
#include <omp.h>

#include "../imageprocessing/imageprocessing.h"
#include "../utilities/colorconsole.hpp"
#include "../utilities/fft_utils.hpp"
#include "../utilities/Field2D.hpp"


/**One operand operators**/
template <typename TY>
double sum( Field2D<TY>& dmap2D ) {
	double acc=0.0;
	for(int64_t idx=0; idx<dmap2D.n_pix; idx++) {
		acc+=norm( dmap2D[idx] );
	}
	return acc;
}

template <typename TY>
double par_sum( Field2D<TY>& dmap2D ) {
	double acc=0.0;
	#pragma omp parallel for reduction(+:acc)
	for(int64_t idx=0; idx<dmap2D.n_pix; idx++) {
		acc+=norm( dmap2D[idx] );
	}
	return acc;
}

template <typename TY>
void sqrt( Field2D<TY>& dmap2D ) {
	for(int64_t idx=0; idx<dmap2D.n_pix; idx++) {
		dmap2D[idx]= sqrt(dmap2D[idx]);
	}
}

template <typename TY>
void par_cap( Field2D<TY>& dmap2D ) {
	#pragma omp parallel for
	for(int64_t idx=0; idx<dmap2D.n_pix; idx++) {
		if( abs(dmap2D.dvec[idx]) > 1.0 ) {
			dmap2D.dvec[idx]/=abs(dmap2D.dvec[idx]);
		}
	}
}

template <typename TY>
void ImWrite( Field2D<TY>& dmap2D, const char* mode, const char* par, const char*format, const char* filename) {
	WriteImOut( dmap2D.dvec, dmap2D.npx_x, dmap2D.npx_y, mode, par, format, filename);
}

#endif
