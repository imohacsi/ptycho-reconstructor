#ifndef DESYPTYCHO_MEMUTILS_H_
#define DESYPTYCHO_MEMUTILS_H_

#include <stdlib.h>


template <typename TY>
TY*** calloc3D(int Nslow, int Nmid, int Nfast) {
	TY*** tpointer = (TY***)calloc(Nslow,sizeof(TY**));
	for(int ss=0; ss<Nslow; ss++) {
		tpointer[ss]=(TY**)calloc(Nmid,sizeof(TY*));
		for(int mm=0; mm<Nmid; mm++) {
			tpointer[ss][mm]=(TY*)calloc(Nfast,sizeof(TY));
		}
	}
	return tpointer;
}


template <typename TY>
TY** calloc2D(int Nslow, int Nfast) {
	TY** tpointer = (TY**)calloc(Nslow,sizeof(TY*));
	for(int ss=0; ss<Nslow; ss++) {
		tpointer[ss]=(TY*)calloc(Nfast,sizeof(TY));
	}
	return tpointer;
}

template <typename TY>
TY* calloc1D(int Nfast) {
	TY* tpointer = (TY*)calloc(Nfast,sizeof(TY));
	return tpointer;
}

template <typename TY>
void free3D(TY*** tpointer, int Nslow, int Nmid) {
	for(int ss=0; ss<Nslow; ss++) {
		for(int mm=0; mm<Nmid; mm++) {
			free( tpointer[ss][mm] );
		}
		free( tpointer[ss] );
	}
	free(tpointer);
	tpointer=nullptr;
}

template <typename TY>
void free2D(TY** tpointer, int Nslow) {
	for(int ss=0; ss<Nslow; ss++) {
		free( tpointer[ss] );
	}
	free(tpointer);
	tpointer=nullptr;
}

template <typename TY>
void free1D(TY* tpointer) {
	free(tpointer);
	tpointer=nullptr;
}

template <typename TY>
void memcopy3D(TY*** from, TY*** to, int Nslow, int Nmid, int Nfast) {
	for(int ss=0; ss<Nslow; ss++) {
		for(int mm=0; mm<Nmid; mm++) {
			memcpy(to[ss][mm],from[ss][mm],Nfast*sizeof(TY));
		}
	}
}





#endif // DESYPTYCHO_MEMUTILS_H_
