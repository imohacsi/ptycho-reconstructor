#ifndef PTYREC_COLOR_CONSOLE_H_
#define PTYREC_COLOR_CONSOLE_H_

/**Console output colors**/
#define A_C_RED     "\x1b[31m"
#define A_C_GREEN     "\x1b[32m"
#define A_C_YELLOW     "\x1b[33m"
#define A_C_BLUE     "\x1b[34m"
#define A_C_MAGENTA     "\x1b[35m"
#define A_C_CYAN     "\x1b[36m"
#define A_C_RESET   "\x1b[0m"

#define LIKELY(condition) __builtin_expect( (condition), 1 )


#endif // PTYREC_COLOR_CONSOLE_H_
