#include "fileio.hpp"

void checkFileExist(char* filename, int severity) {
	struct stat filestat;
	if( stat(filename,&filestat)!=0 ) {
		if(severity==0) {
			printf("%sWARNING: File %s does not exist!%s\n",A_C_YELLOW,filename,A_C_RESET);
			fflush(stdout);
		} else if(severity==1) {
			printf("%sERROR: File %s does not exist!%s\n",A_C_RED,filename,A_C_RESET);
			fflush(stdout);
		}
		throw file_not_exists();
	}
}
