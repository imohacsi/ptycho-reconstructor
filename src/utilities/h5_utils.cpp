#include "h5_utils.hpp"

//std::vector<int64_t> h5py::dset_read_float(float* Field,char* ipath) {
//	if(Field!=nullptr) {
//		free(Field);
//	}
//
//	hid_t dataset_id = H5Dopen(file_id, ipath, H5P_DEFAULT);
//	hid_t dataspace_id=H5Dget_space(dataset_id);
//
//	/**Reading dataset dimensions**/
//	int ndims=H5Sget_simple_extent_ndims(dataspace_id);
//	hsize_t     dims[ndims];
//	H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
//
//	std::vector<int64_t> dsize(ndims-1);
//	int64_t tsize = 1;
//	for(int dd=0; dd<ndims-1; dd++) {
//		dsize[dd]=dims[dd];
//		tsize*=dims[dd];
//	}
//	Field = (std::complex<double>*)calloc(tsize,sizeof(std::complex<double>));
//
//	H5Dread(dataset_id,H5T_NATIVE_DOUBLE,0,H5S_ALL,H5P_DEFAULT,Field);
//	H5Dclose(dataset_id);
//	return dsize;
//}


std::vector<int64_t> h5py::dset_read_cdouble(std::complex<double>* Field,char* ipath) {
	if(Field!=nullptr) {
		free(Field);
	}

	hid_t dataset_id = H5Dopen(file_id, ipath, H5P_DEFAULT);
	hid_t dataspace_id=H5Dget_space(dataset_id);

	/**Reading dataset dimensions**/
	int ndims=H5Sget_simple_extent_ndims(dataspace_id);
	hsize_t     dims[ndims];
	H5Sget_simple_extent_dims(dataspace_id, dims, NULL);

	std::vector<int64_t> dsize(ndims-1);
	int64_t tsize = 1;
	for(int dd=0; dd<ndims-1; dd++) {
		dsize[dd]=dims[dd];
		tsize*=dims[dd];
	}
	Field = (std::complex<double>*)calloc(tsize,sizeof(std::complex<double>));

	H5Dread(dataset_id,H5T_NATIVE_DOUBLE,0,H5S_ALL,H5P_DEFAULT,Field);
	H5Dclose(dataset_id);
	return dsize;
}




void h5py::dset_write_cdouble(std::complex<double> *Field, std::vector<int64_t> shape, char* ipath) {
	int64_t n_dim = shape.size();
	hsize_t     dim[n_dim+1];
	dim[n_dim]=2;                //Complex
	for(int64_t dd=0; dd<n_dim; dd++) {
		dim[dd]=shape[dd];
	}

	/* Allocating containers for the data */
	hid_t dataspace_id = H5Screate_simple(n_dim+1,dim, NULL);
	hid_t dataset_id = H5Dcreate2(file_id,ipath,H5T_NATIVE_DOUBLE,dataspace_id,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);

	/* Writing array to hdf5 file */
	H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, Field);

	/* End access to the dataset and the dataspace. */
	H5Dclose(dataset_id);
	H5Sclose(dataspace_id);
}

void h5py::dset_write_double(double *Field, std::vector<int64_t> shape, char* ipath) {
	int64_t n_dim = shape.size();
	hsize_t     dim[n_dim];
	for(int64_t dd=0; dd<n_dim; dd++) {
		dim[dd]=shape[dd];
	}

	/* Allocating containers for the data */
	hid_t dataspace_id = H5Screate_simple(n_dim,dim, NULL);
	hid_t dataset_id = H5Dcreate2(file_id,ipath,H5T_NATIVE_DOUBLE,dataspace_id,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);

	/* Writing array to hdf5 file */
	H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, Field);

	/* End access to the dataset and the dataspace. */
	H5Dclose(dataset_id);
	H5Sclose(dataspace_id);
}

void h5py::dset_write_int(int *Field, std::vector<int64_t> shape, char* ipath) {
	int64_t n_dim = shape.size();
	hsize_t     dim[n_dim];
	for(int64_t dd=0; dd<n_dim; dd++) {
		dim[dd]=shape[dd];
	}

	/* Allocating containers for the data */
	hid_t dataspace_id = H5Screate_simple(n_dim,dim, NULL);
	hid_t dataset_id = H5Dcreate2(file_id,ipath,H5T_NATIVE_INT,dataspace_id,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);

	/* Writing array to hdf5 file */
	H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, Field);

	/* End access to the dataset and the dataspace. */
	H5Dclose(dataset_id);
	H5Sclose(dataspace_id);
}


void h5py::dset_write_double(double val, char* ipath) {
	hsize_t     dim[1] = {1};

	/* Allocating containers for the data */
	hid_t dataspace_id = H5Screate_simple(1,dim, NULL);
	hid_t dataset_id = H5Dcreate2(file_id,ipath,H5T_NATIVE_DOUBLE,dataspace_id,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);

	/* Writing array to hdf5 file */
	H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &val);

	/* End access to the dataset and the dataspace. */
	H5Dclose(dataset_id);
	H5Sclose(dataspace_id);
}


void h5py::dset_write_int(int val, char* ipath) {
	hsize_t     dim[1] = {1};

	/* Allocating containers for the data */
	hid_t dataspace_id = H5Screate_simple(1,dim, NULL);
	hid_t dataset_id = H5Dcreate2(file_id,ipath,H5T_NATIVE_INT,dataspace_id,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);

	/* Writing array to hdf5 file */
	H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &val);

	/* End access to the dataset and the dataspace. */
	H5Dclose(dataset_id);
	H5Sclose(dataspace_id);
}
