#ifndef PTYREC_WFIELD_HPP_
#define PTYREC_WFIELD_HPP_

#include <iostream>
#include <cstring>
#include <cstdint>
#include <vector>

#include "../utilities/colorconsole.hpp"
#include "../utilities/Field2D.hpp"
#include "../utilities/Field2D_ops.hpp"


template <typename TY>
class trifield {
public:
	int64_t _num_modes;
	int64_t _num_field;
	int64_t _pix_x;
	int64_t _pix_y;
	int64_t _n_pix;
	Field2D<TY> **maps;
    void linalg_orth();

    std::vector<int64_t> shape(){
        std::vector<int64_t> shape= {_num_field,_num_modes,_pix_x,_pix_y};
        return shape; }


	trifield() {
		maps=nullptr;
	};
	trifield(int64_t mod, int64_t fie, int64_t pxx, int64_t pxy) {
		this->init( mod,fie,pxx,pxy);
	};
	~trifield() { }
	void free() {
		if( maps!=nullptr ) {
			for(int64_t mm=0; mm<_num_modes; mm++) {
				delete[] maps[mm];
			}
		}
	};

	inline Field2D<TY>* operator[]( int64_t idx) {
		return maps[idx];
	};

	void init(int64_t nmodes, int64_t nfields, int64_t px_x, int64_t px_y) {
		_num_modes = nmodes;
		_num_field = nfields;
		_pix_x = px_x;
		_pix_y = px_y;
		_n_pix = _pix_x*_pix_y;

		this->free();

		maps = new Field2D<TY>*[_num_modes];
		for(int64_t mm=0; mm<_num_modes; mm++) {
			maps[mm] = new Field2D<TY>[_num_field];
			for(int64_t ff=0; ff<_num_field; ff++) {
				maps[mm][ff].init(_pix_x,_pix_y);
			}
		}
	}
};



template <typename TY>
inline void trifield<TY>::linalg_orth(){ };

template <>
inline void trifield<complex<double>>::linalg_orth(){
    printf("Orthogonalizing probes using the Gram-Schmidt process...\n");

	if(_num_field>1) {
		complex<double> bufferU;
		complex<double> bufferD;
        complex<double> proj;
        for(int mm=0; mm<_num_modes;mm++){
            //Now the fields are the probe modes
            for(int fproj=1; fproj<_num_field; fproj++) {
                //We are referencing to earlier modes
                for(int fbase=0; fbase<fproj; fbase++){

                    bufferU=0;
                    bufferD=0;
                    for(int64_t xy=0; xy<_n_pix; xy++) {
                        bufferU+=maps[mm][fbase][xy]*conj(maps[mm][fproj][xy]);
                        bufferD+=norm(maps[mm][fbase][xy]);
                    }

                    proj = bufferU/bufferD;
                    for(int64_t xy=0; xy<_n_pix; xy++) {
                        maps[mm][fproj][xy]-= proj*maps[mm][fbase][xy];
                    }
                    maps[mm][fbase]*=(1.0+proj);
                }
            }
        }
    }
}

#endif
