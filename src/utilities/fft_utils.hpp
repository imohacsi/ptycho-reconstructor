#ifndef PTYREC_UTILS_FFTUTILS_HPP_
#define PTYREC_UTILS_FFTUTILS_HPP_
#include <fftw3.h>
#include <complex>

/**Perform a circular shift of a 2D feature map with the help of a temporary buffer**/
template <typename TY>
void circshift(TY *inmap, int64_t xdim, int64_t ydim, int64_t xshift, int64_t yshift) {
	int64_t ix,iy,xx,yy;
	TY *Norm=(TY *)calloc(xdim*ydim,sizeof(TY));

	for (int64_t ix =0; ix < xdim; ix++) {
		xx = (ix + xshift) % xdim;
		for (int64_t iy = 0; iy < ydim; iy++) {
			yy = (iy + yshift) % ydim;
			Norm[yy+xx*ydim] = inmap[iy+ix * ydim];
		}
	}

	for (ix =0; ix < xdim; ix++) {
		for (iy = 0; iy < ydim; iy++) {
			inmap[ix * ydim + iy] = Norm[ix * ydim + iy];
		}
	}
	free(Norm);
}

/**Perform the fftshift by setting the circular shift parameters**/
template <typename TY>
void fftshift(TY *inmap, int64_t Nx, int64_t Ny) {
	circshift<TY>(inmap, Nx, Ny, (Nx/2), (Ny/2));
}

/**Normalize a single-direction 2D FFT with the number of pixels**/
template <typename TY>
void fftw_norm(TY* inmap, int64_t Nx) {
	const double Norm=1.0/(double)(Nx);
	for(int64_t idx=0; idx<Nx*Nx; idx++) {
		inmap[idx]*=Norm;
	}
}


/**Combine the 2D fft of various complex feature maps into an umbrella class**/
template <typename TY>
class fftw_allprec {
protected:
    int64_t _copy_mode;
	int64_t _prec;
	int64_t _num_pix;
	fftwf_plan  f_fftplan_fw;
	fftwf_plan  f_fftplan_bw;
	fftw_plan   d_fftplan_fw;
	fftw_plan   d_fftplan_bw;

    std::complex<TY> *_intr_ptr=nullptr;
	void plan_fft();

public:
	fftw_allprec() { };
	fftw_allprec( int64_t npix ) { this->init( npix ); };
	fftw_allprec(std::complex<TY> *inmap, int64_t npix) { this->init( inmap, npix );	};

	void init(int64_t npix) {
		_num_pix= npix;
		_copy_mode = 1;
		if( _intr_ptr!=nullptr ){ free(_intr_ptr); }
		_intr_ptr = (std::complex<TY>*)calloc(_num_pix*_num_pix,sizeof(std::complex<TY>));
		plan_fft();
	};

	void init(std::complex<TY>* ptr, int64_t npix) {
		_num_pix= npix;
		_copy_mode = 0;
		if( _intr_ptr!=nullptr ){ free(_intr_ptr); }
		_intr_ptr = ptr;
		plan_fft();
	};

	~fftw_allprec() {
		clear();
	}
	void clear() {
		if(_prec==0) {
//            fftwf_destroy_plan(f_fftplan_fw);
//            fftwf_destroy_plan(f_fftplan_bw);
		} else if(_prec==1) {
			fftw_destroy_plan(d_fftplan_fw);
			fftw_destroy_plan(d_fftplan_bw);
		}
		if( _copy_mode && _intr_ptr!=nullptr ){ free(_intr_ptr); }
	};

	void execute_fw( std::complex<TY>* );
	void execute_fw( );
	void execute_bw( std::complex<TY>* );
	void execute_bw( );
};

//template <typename TY>
//void fftw_allprec<TY>::plan_fft() {}
//template <typename TY>
//void fftw_allprec<TY>::execute_fw( std::complex<TY> *dptr ) { }
//template <typename TY>
//void fftw_allprec<TY>::execute_bw( std::complex<TY> *dptr ) { }

#endif  //PTYREC_UTILS_FFTUTILS_HPP_

