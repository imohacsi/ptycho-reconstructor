#ifndef PTYREC_UTILS_FIELD2D_TEMPLATE_HPP_
#define PTYREC_UTILS_FIELD2D_TEMPLATE_HPP_

#include <cstdint>
#include <cmath>
#include <omp.h>
#include <immintrin.h>

#include "../utilities/colorconsole.hpp"

template <typename TY>
class Field2D {
public:
	int64_t npx_x;
	int64_t npx_y;
	int64_t n_pix;
//    __m256d*    vptr;
	TY*         dvec;

	/**Constructors and destructor**/

	Field2D( const Field2D<TY>& SRC       ) {
//    if( LIKELY( this->n_pix == SRC.n_pix && this->npx_x == SRC.npx_x && this->npx_y == SRC.npx_y ) ){
		std::memcpy(this->dvec,SRC.dvec,this->n_pix*sizeof(TY));
//    }else{
//        this->init(SRC.npx_x,SRC.npx_y);
//        std::memcpy(this->dvec,SRC.dvec,this->n_pix*sizeof(TY));
//    }
	};
////    Field2D(                              ) { vptr=nullptr; dvec=nullptr; this->init(0,0);         };
////    Field2D( int64_t pix_x                ) { vptr=nullptr; dvec=nullptr; this->init(pix_x,pix_x); };
////    Field2D( int64_t pix_x, int64_t pix_y ) { vptr=nullptr; dvec=nullptr; this->init(pix_x,pix_y); };
	Field2D(                              ) {
		dvec=nullptr;
		this->init(0,0);
	};
//    Field2D( int64_t pix_x                ) { dvec=nullptr; this->init(pix_x,pix_x); };
	Field2D( int64_t pix_x, int64_t pix_y ) {
		dvec=nullptr;
		this->init(pix_x,pix_y);
	};
	~Field2D() {
		fflush(stdout);
		free(dvec);
	};

	/**Initializer and zeroing**/
	void init( int64_t pix_x, int64_t pix_y );
	void zero( ) {
		memset(dvec,0,n_pix*sizeof(TY));
	}

	/**************************************************************************************/
	/**Import data from other map without new allocation**/
	void imp( Field2D<TY>& SRC );
	void imp( TY* SRC );
	/**Region selection from 2D array**/
	void impsel(Field2D<TY>& SRC, int64_t pos_x, int64_t pos_y);
	void impsel(Field2D<TY>& SRC, int64_t startpos);

	/**************************************************************************************/
	/**Binary operators**/
	void operator=( const Field2D<TY>& VAL);
	void operator=( Field2D<TY>& VAL);
	template <typename TP>
	void  operator=( TP VAL );
	template <typename TP>
	void operator+=( Field2D<TP>& VAL);
	template <typename TP>
	void operator+=( TP VAL);
	template <typename TP>
	void operator-=( Field2D<TP>& VAL);
	template <typename TP>
	void operator-=( TP VAL);
	template <typename TP>
	void operator*=( Field2D<TP>& VAL);
	template <typename TP>
	void operator*=( TP VAL);
	template <typename TP>
	void operator/=( Field2D<TP>& VAL);
	template <typename TP>
	void operator/=( TP VAL);
	inline TY& operator[]( int64_t idx) {
		return this->dvec[idx];
	}


	/**Parallelized binary operators**/
	template <typename TP>
	void par_add(Field2D<TP>& val);
	template <typename TP>
	void par_add(TP VAL);
	template <typename TP>
	void par_sub(Field2D<TP>& val);
	template <typename TP>
	void par_sub(TP VAL);
	template <typename TP>
	void par_mul(Field2D<TP>& val);
	template <typename TP>
	void par_mul(TP VAL);
	template <typename TP>
	void par_div(Field2D<TP>& val);
	template <typename TP>
	void par_div(TP VAL);
	template <typename TA, typename TB>
	void par_fma(TA rate, Field2D<TB>& aval);
	template <typename TA, typename TB>
	void par_fma(Field2D<TA> aval, Field2D<TB>& bval);
	template <typename TP>
	void par_fma(Field2D<TP> aval, Field2D<TP>& bval, Field2D<TP>& cval);


};

template <typename TY>
void Field2D<TY>::init( int64_t pix_x, int64_t pix_y ) {
	npx_x = pix_x;
	npx_y = pix_y;
	n_pix = npx_x*npx_y;
	if( dvec!=nullptr ) {
		free(dvec);
	}
	dvec = (TY*)calloc(n_pix,sizeof(TY));
//    vptr = (__m256d*)dvec;
}



/***********************************************************************************************************/
/**Import data from other map without new allocation**/
template <typename TY>
void Field2D<TY>::imp( Field2D<TY>& SRC ) {
	/**Import directly from another field of same size**/
	if( LIKELY(this->n_pix == SRC.n_pix) ) {
		std::memcpy(this->dvec,SRC.dvec,this->n_pix*sizeof(TY));
	} else {
		perror(A_C_RED "ERROR: Attempting to copy fields of different size!" A_C_RESET);
	}
}

template <typename TY>
void Field2D<TY>::imp( TY* SRC ) {
	/**Import directly from pointer**/
	std::memcpy(this->dvec,SRC,this->n_pix*sizeof(TY));
}

/*****************************************************************************************/
/**Region selection from 2D array**/
template <typename TY>
void Field2D<TY>::impsel(Field2D<TY>& SRC, int64_t pos_x, int64_t pos_y) {
	int64_t startpos = pos_x+pos_y*SRC.npx_x;
//        int64_t idxmax = (startpos+(target.npx_x+target.npx_y*this->npx_x))-1;
//        if(  idxmax > SRC.n_pix ) {
//            perror(A_C_RED "ERROR: Region selection is out of range!" A_C_RESET);
//        }
	for( int64_t idxy=0; idxy<this->npx_y; idxy++) {
		std::memcpy(&this->dvec[idxy*npx_x], &SRC.dvec[idxy*SRC.npx_x+startpos], npx_x*sizeof(TY));
	}
}

/**Region selection from 2D array**/
template <typename TY>
void Field2D<TY>::impsel(Field2D<TY>& SRC, int64_t startpos) {
//        int64_t idxmax = (startpos+(target.npx_x+target.npx_y*this->npx_x))-1;
//        if(  idxmax > SRC.n_pix ) {
//            perror(A_C_RED "ERROR: Region selection is out of range!" A_C_RESET);
//        }
	for( int64_t idxy=0; idxy<this->npx_y; idxy++) {
		std::memcpy(&this->dvec[idxy*npx_x], &SRC.dvec[idxy*SRC.npx_x+startpos], npx_x*sizeof(TY));
	}
}

/***********************************************************************************************************/
/**Assignment operators**/
template <typename TY>
void Field2D<TY>::operator=( const Field2D<TY>& SRC) {
	if( LIKELY( this->n_pix == SRC.n_pix && this->npx_x == SRC.npx_x && this->npx_y == SRC.npx_y ) ) {
		std::memcpy(this->dvec,SRC.dvec,this->n_pix*sizeof(TY));
	} else {
		this->init(SRC.npx_x,SRC.npx_y);
		std::memcpy(this->dvec,SRC.dvec,this->n_pix*sizeof(TY));
	}
};

template <typename TY>
void Field2D<TY>::operator=( Field2D<TY>& SRC) {
	if( LIKELY( this->n_pix == SRC.n_pix && this->npx_x == SRC.npx_x && this->npx_y == SRC.npx_y ) ) {
		std::memcpy(this->dvec,SRC.dvec,this->n_pix*sizeof(TY));
	} else {
		this->init(SRC.npx_x,npx_y);
		std::memcpy(this->dvec,SRC.dvec,this->n_pix*sizeof(TY));
	}
};

template <typename TY>
template <typename TP>
void Field2D<TY>::operator=( TP VAL) {
	for(int64_t idx=0; idx<n_pix; idx++) {
		this->dvec[idx] = VAL;
	}
};

/*****************************************************************************************/
/**Element-wise addition**/
template <typename TY>
template <typename TP>
void Field2D<TY>::operator+=( Field2D<TP>& VAL) {
	if( this->n_pix == VAL.n_pix) {
		for(int64_t idx=0; idx<n_pix; idx++) {
//                vptr[idx] = _mm256_add_pd( vptr[idx], VAL.vptr[idx] );
			dvec[idx] += VAL.dvec[idx];
		}
	} else {
		perror(A_C_RED "ERROR: Attempting to add fields of different size!" A_C_RESET);
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::operator+=( TP VAL) {
	for(int64_t idx=0; idx<n_pix; idx++) {
		dvec[idx] += VAL;
	}
}

/*****************************************************************************************/
/**Element-wise subtraction**/
template <typename TY>
template <typename TP>
void Field2D<TY>::operator-=( Field2D<TP>& VAL) {
	if( this->n_pix == VAL.n_pix) {
		for(int64_t idx=0; idx<n_pix; idx++) {
//                vptr[idx] = _mm256_sub_pd( vptr[idx], VAL.vptr[idx] );
			dvec[idx] -= VAL.dvec[idx];
		}
	} else {
		perror(A_C_RED "ERROR: Attempting to subtract fields of different size!" A_C_RESET);
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::operator-=( TP VAL) {
	for(int64_t idx=0; idx<n_pix; idx++) {
		dvec[idx] -= VAL;
	}
}

/*****************************************************************************************/
/**Elementwise multiplication**/
template <typename TY>
template <typename TP>
void Field2D<TY>::operator*=( Field2D<TP>& VAL) {
	if( this->n_pix == VAL.n_pix) {
		for(int64_t idx=0; idx<n_pix; idx++) {
//                vptr[idx] = _mm256_mul_pd( vptr[idx], VAL.vptr[idx] );
			dvec[idx] *= VAL.dvec[idx];
		}
	} else {
		perror(A_C_RED "ERROR: Attempting to add fields of different size!" A_C_RESET);
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::operator*=( TP VAL) {
	for(int64_t idx=0; idx<n_pix; idx++) {
		dvec[idx] *= VAL;
	}
}

/*****************************************************************************************/
/**Elementwise division**/
template <typename TY>
template <typename TP>
void Field2D<TY>::operator/=( Field2D<TP>& VAL) {
	if( this->n_pix == VAL.n_pix) {
		for(int64_t idx=0; idx<n_pix; idx++) {
//                vptr[idx] = _mm256_mul_pd( vptr[idx], VAL.vptr[idx] );
			dvec[idx] /= VAL.dvec[idx];
		}
	} else {
		perror(A_C_RED "ERROR: Attempting to add fields of different size!" A_C_RESET);
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::operator/=( TP VAL) {
	for(int64_t idx=0; idx<n_pix; idx++) {
		dvec[idx] /= VAL;
	}
}

/*****************************************************************************************/
/**Parallelized binary operators**/
template <typename TY>
template <typename TP>
void Field2D<TY>::par_add( Field2D<TP>& VAL) {
	if( this->n_pix == VAL.n_pix) {
		#pragma omp parallel for
		for(int64_t idx=0; idx<n_pix; idx++) {
//                vptr[idx] = _mm256_mul_pd( vptr[idx], VAL.vptr[idx] );
			dvec[idx] += VAL.dvec[idx];
		}
	} else {
		perror(A_C_RED "ERROR: Attempting to add fields of different size!" A_C_RESET);
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::par_add( TP VAL) {
	#pragma omp parallel for
	for(int64_t idx=0; idx<n_pix; idx++) {
		dvec[idx] += VAL;
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::par_sub( Field2D<TP>& VAL) {
	if( this->n_pix == VAL.n_pix) {
		#pragma omp parallel for
		for(int64_t idx=0; idx<n_pix; idx++) {
//                vptr[idx] = _mm256_mul_pd( vptr[idx], VAL.vptr[idx] );
			dvec[idx] -= VAL.dvec[idx];
		}
	} else {
		perror(A_C_RED "ERROR: Attempting to add fields of different size!" A_C_RESET);
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::par_sub( TP VAL) {
	#pragma omp parallel for
	for(int64_t idx=0; idx<n_pix; idx++) {
		dvec[idx] -= VAL;
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::par_mul( Field2D<TP>& VAL) {
	if( this->n_pix == VAL.n_pix) {
		#pragma omp parallel for
		for(int64_t idx=0; idx<n_pix; idx++) {
//                vptr[idx] = _mm256_mul_pd( vptr[idx], VAL.vptr[idx] );
			dvec[idx] *= VAL.dvec[idx];
		}
	} else {
		perror(A_C_RED "ERROR: Attempting to add fields of different size!" A_C_RESET);
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::par_mul( TP VAL) {
	#pragma omp parallel for
	for(int64_t idx=0; idx<n_pix; idx++) {
		dvec[idx] *= VAL;
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::par_div( Field2D<TP>& VAL) {
	if( this->n_pix == VAL.n_pix) {
		#pragma omp parallel for
		for(int64_t idx=0; idx<n_pix; idx++) {
//                vptr[idx] = _mm256_mul_pd( vptr[idx], VAL.vptr[idx] );
			dvec[idx] /= VAL.dvec[idx];
		}
	} else {
		perror(A_C_RED "ERROR: Attempting to add fields of different size!" A_C_RESET);
	}
}

template <typename TY>
template <typename TP>
void Field2D<TY>::par_div( TP VAL) {
	#pragma omp parallel for
	for(int64_t idx=0; idx<n_pix; idx++) {
		dvec[idx] /= VAL;
	}
}

template <typename TY>
template <typename TA, typename TB>
void Field2D<TY>::par_fma( TA rate, Field2D<TB>& aval ) {
	if( this->n_pix == aval.n_pix) {
		#pragma omp parallel for
		for(int64_t idx=0; idx<n_pix; idx++) {
			dvec[idx] += rate*aval.dvec[idx];
		}
	}
}


#endif
