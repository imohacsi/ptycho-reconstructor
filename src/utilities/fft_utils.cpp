#include "fft_utils.hpp"




template <>
void fftw_allprec<float>::plan_fft() {
    if( _intr_ptr==nullptr ){ exit(-1); }
	_prec=32;
//	f_fftplan_fw = fftwf_plan_dft_2d(_num_pix,_num_pix,reinterpret_cast<fftwf_complex*>(_intr_ptr),reinterpret_cast<fftwf_complex*>(_intr_ptr),FFTW_FORWARD,FFTW_MEASURE);
//	f_fftplan_bw = fftwf_plan_dft_2d(_num_pix,_num_pix,reinterpret_cast<fftwf_complex*>(_intr_ptr),reinterpret_cast<fftwf_complex*>(_intr_ptr),FFTW_BACKWARD,FFTW_MEASURE);
}

template <>
void fftw_allprec<double>::plan_fft() {
    if( _intr_ptr==nullptr ){ exit(-1); }
	_prec=64;
	d_fftplan_fw = fftw_plan_dft_2d(_num_pix,_num_pix,reinterpret_cast<fftw_complex*>(_intr_ptr),reinterpret_cast<fftw_complex*>(_intr_ptr),FFTW_FORWARD,FFTW_MEASURE);
	d_fftplan_bw = fftw_plan_dft_2d(_num_pix,_num_pix,reinterpret_cast<fftw_complex*>(_intr_ptr),reinterpret_cast<fftw_complex*>(_intr_ptr),FFTW_BACKWARD,FFTW_MEASURE);
}



template <>
void fftw_allprec<double>::execute_fw( std::complex<double> *dptr ) {
    if( _intr_ptr==nullptr ){ exit(-1); }

    //Copy the input to the internal buffer
    for( int64_t xy=0; xy<_num_pix*_num_pix; xy++){
        _intr_ptr[xy] = dptr[xy]; }

    if(_prec==64) fftw_execute( d_fftplan_fw );

    //Copy the data back to the input array
    for( int64_t xy=0; xy<_num_pix*_num_pix; xy++){
        dptr[xy] = _intr_ptr[xy]; }
}

template <>
void fftw_allprec<double>::execute_bw( std::complex<double> *dptr ) {
    if( _intr_ptr==nullptr ){ exit(-1); }

    //Copy the input to the internal buffer
    for( int64_t xy=0; xy<_num_pix*_num_pix; xy++){
        _intr_ptr[xy] = dptr[xy]; }

    if(_prec==64) fftw_execute( d_fftplan_bw );

    //Copy the data back to the input array
    for( int64_t xy=0; xy<_num_pix*_num_pix; xy++){
        dptr[xy] = _intr_ptr[xy]; }
};


template <>
void fftw_allprec<double>::execute_fw( ) {
    if( _intr_ptr==nullptr ){ exit(-1); }
    if(_prec==64) fftw_execute( d_fftplan_fw );
}

template <>
void fftw_allprec<double>::execute_bw( ) {
    if( _intr_ptr==nullptr ){ exit(-1); }
    if(_prec==64) fftw_execute( d_fftplan_bw );
};
