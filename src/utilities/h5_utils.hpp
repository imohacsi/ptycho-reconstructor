#ifndef PTYREC_UTILS_H5UTILS_HPP_
#define PTYREC_UTILS_H5UTILS_HPP_
#include <hdf5.h>
#include <string>
#include <complex>
#include <vector>


class h5py {
protected:
	hid_t file_id;
public:
	h5py( char* filename, std::string openmode ) {
		if(openmode=="w") {
			file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
		} else if(openmode=="r") {
			file_id=H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
		} else {
			exit(EXIT_FAILURE);
		}
	}
	~h5py() {
		H5Fclose(file_id);
	}

	void dset_write_cdouble(std::complex<double>*,std::vector<int64_t>,char*);
	void dset_write_double(double*,std::vector<int64_t>,char*);
	void dset_write_int(int*,std::vector<int64_t>,char*);
	void dset_write_double(double,char*);
	void dset_write_int(int,char*);

	std::vector<int64_t> dset_read_cdouble(std::complex<double>*,char*);


};

#endif // PTYREC_UTILS_H5UTILS_HPP_
