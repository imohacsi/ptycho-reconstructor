#ifndef PTYREC_UTILS_FILEIO_HPP_
#define PTYREC_UTILS_FILEIO_HPP_
#include <iostream>
#include <string>
#include <complex>
#include <sys/stat.h>
#include "colorconsole.hpp"

class file_not_exists: public std::exception {
	virtual const char* what() {
		return "file_not_exists";
	}
};


void checkFileExist(char* filename,int severity=0);

/**Remplate class for reading an array from a binary file**/
template <typename TY>
void LoadBinFile(TY *Probe, int64_t Ndim, const char *filename) {
	TY tmp;
	FILE *fp=nullptr;
	fp=fopen(filename,"rb");
	if(fp==nullptr) {
		printf("%sERROR: unable to open binary file: %s%s\n",A_C_RED,filename,A_C_RESET);
		fflush(stdout);
		throw file_not_exists();
	}

	for(int64_t i=0; i<Ndim; i++) {
		fread(&tmp, sizeof(TY), 1, fp);
		Probe[i]=tmp;
	}
	fclose(fp);
}

template <typename TY>
void WriteBinFile(TY *Data, int64_t Ndim, const char *filename) {
	FILE *fp=nullptr;
	fp=fopen(filename,"wb");
	if(fp==nullptr) {
		printf("%sERROR: unable to open binary file: %s%s\n",A_C_RED,filename,A_C_RESET);
		fflush(stdout);
	}

	for(int64_t i=0; i<Ndim; i++) {
		fwrite(&Data[i],sizeof(TY),1,fp);
	}
	fclose(fp);
}

#endif  //PTYREC_UTILS_FILEIO_HPP_
