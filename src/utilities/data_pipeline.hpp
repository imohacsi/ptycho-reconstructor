#ifndef __PTYREC_DATA_PIPELINE_HPP__
#define __PTYREC_DATA_PIPELINE_HPP__
#include <iostream>
#include <string>
#include <vector>
#include "../preproc_data/preproc_data.hpp"
#include "../read_data/read_data.hpp"

/**Loads raw input data from files into memory.
*   Separate modules for loading image data, position data and masks.
*/
class data_pipeline {
  protected:
    /**Configurations**/
    data_format     _dconf;
    pproc_format    _pconf;
    ptyconf         _wconf;
    /**Main data buffers**/
    raw_data _scandata;
    expdata  _exp_data;
    ptyconf  _rec_config;

    /**Read raw experimental data into memory**/
    void read_data_from_disk( ){
        read_data DREADER( _dconf, _wconf );
        DREADER.run_read_data();
        _scandata = DREADER.get_data();
    }

   /**Read data into memory*/
    void apply_data_preprocessor(){
        /**Initialize preprocessor**/
        preprocessor PREPROC( _pconf, _wconf, _scandata);
        /**Clean up memory*/
        _scandata.clear();

        /**Run data preprocessing*/
        PREPROC.run_data_preprocessor();

         /**Retrieving relevant data from preprocessor**/
        _rec_config  = PREPROC.get_ptyconf();
        _exp_data    = PREPROC.get_expdata();
        }


  public:
    /**Constructor**/
    data_pipeline( data_format dconf, pproc_format pconf, ptyconf wconf ): _dconf(dconf),_pconf(pconf),_wconf(wconf) {};
	/**Executor**/
    void run_data_pipeline(){
        read_data_from_disk();
        apply_data_preprocessor();
    };

    /**Retrieving data**/
    expdata get_expdata(){ return _exp_data; }
    ptyconf get_ptyconf(){ return _rec_config; }
};
#endif //__PTYREC_DATA_PIPELINE_HPP__








