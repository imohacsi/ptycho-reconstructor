#include "Field2D_ops.hpp"



fftw_plan fft_plan_2d( Field2D<complex<double>>& dmap2D, int64_t dir) {
	fftw_plan PlanFFT;
	int64_t Npix = dmap2D.npx_x;
	if(dir>0) {
		PlanFFT= fftw_plan_dft_2d(Npix,Npix,reinterpret_cast<fftw_complex*>(dmap2D.dvec),reinterpret_cast<fftw_complex*>(dmap2D.dvec),FFTW_FORWARD,FFTW_MEASURE);
	} else {
		PlanFFT= fftw_plan_dft_2d(Npix,Npix,reinterpret_cast<fftw_complex*>(dmap2D.dvec),reinterpret_cast<fftw_complex*>(dmap2D.dvec),FFTW_BACKWARD,FFTW_MEASURE);
	}
	return PlanFFT;
}
