#ifndef DESYPTYCHO_WORKER_CONFIG_H_
#define DESYPTYCHO_WORKER_CONFIG_H_

#include <iostream>
#include <string>
#include <vector>
#include <complex>
#include "../utilities/wavefield.hpp"
#include "../utilities/memutils.h"

class ptyconf {
public:
	int64_t _num_pixels;
	int64_t _batch_size;
	int64_t _num_frames;
	int64_t _num_omodes;
	int64_t _num_pmodes;
	int64_t _num_obj;
	int64_t _num_pro;
	int64_t _num_msk;
	int64_t _OX;
	int64_t _OY;
	int64_t _border;
	double _dPX;

	int _batch_id;
	int _scan_id;
	std::string _run_name;
	char _batch_name[512];

	/**Not necessary parameters, but help to pass things around*/
	int64_t _tot_nscans;
	int64_t _avg_counts;
	int64_t _num_THREADS;
	double _energy;
	double _det_distance;
	double _det_pixelsize;
	double _lambda;
	double _cutoff;

	int64_t _num_rec_iter;
	int64_t _num_ref_iter;
	int64_t _num_curr_iter;
};

/**Class to store experimental data**/
class expdata {
public:
	float **IMG;
	int **MSK;
	int *_plist;
	int *_olist;
	int *_msklist;
	int64_t *_pos1D;
	double **_pos2D;
	double *BGROUND;
};


template <typename TY>
class ptydata: public expdata {
public:
//    field<std::complex<TY>> **PSI=nullptr;
	trifield<std::complex<TY>> PSI;
	trifield<std::complex<TY>> PRO;
	trifield<std::complex<TY>> OBJ;

	ptydata() {};
	ptydata(expdata edata): expdata(edata) {};
	template <typename TP>
    ptydata(const ptydata<TP>& templ): expdata(templ) {};

	~ptydata() {};

//	template <typename TP>
//	ptydata<TY>& operator=(ptydata<TP>& A);
//	template <typename TP>
//	ptydata<TY> operator=(const ptydata<TP>& A);
};

//
//template <>
//template <>
//inline ptydata<float>& ptydata<float>::operator=(ptydata<double>& A){
//    this->~ptydata<float>();
//    new(this) ptydata<float>(A);
//
//    //Copy the probes
//    this->PRO.init( A.PRO._num_field, A.PRO._num_modes, A.PRO._pix_x, A.PRO._pix_y);
//    for( int64_t npr=0; npr<A.PRO._num_field; npr++){
//        for( int64_t pp=0; pp<A.PRO._num_modes; pp++){
//            for( int64_t xy=0; xy<A.OBJ._n_pix; xy++){
//                PRO[npr][pp][xy] = A.PRO[npr][pp][xy];
//            }}}
//
//    //Copy the views
//    this->PSI.init( A.PSI._num_field, A.PSI._num_modes, A.PSI._pix_x, A.PSI._pix_y);
//    for( int64_t ff=0; ff<A.PSI._num_field; ff++){
//        for( int64_t mm=0; mm<A.PSI._num_modes; mm++){
//            for( int64_t xy=0; xy<A.OBJ._n_pix; xy++){
//                PSI[ff][mm][xy] = A.PSI[ff][mm][xy];
//            }}}
//
//    //Copy the objects
//    this->OBJ.init( A.OBJ._num_field, A.OBJ._num_modes, A.OBJ._pix_x, A.OBJ._pix_y);
//    for( int64_t nob=0; nob<A.OBJ._num_field; nob++){
//        for( int64_t oo=0; oo<A.OBJ._num_modes; oo++){
//            for( int64_t xy=0; xy<A.OBJ._n_pix; xy++){
//                OBJ[nob][oo][xy] = A.OBJ[nob][oo][xy];
//            }}}
//    return *this;
//}
//
//template <>
//template <>
//inline ptydata<double>& ptydata<double>::operator=(ptydata<float>& A){
//    this->~ptydata<double>();
//    new(this) ptydata<double>(A);
//
//    //Copy the probes
//    this->PRO.init( A.PRO._num_field, A.PRO._num_modes, A.PRO._pix_x, A.PRO._pix_y);
//    for( int64_t npr=0; npr<A.PRO._num_field; npr++){
//        for( int64_t pp=0; pp<A.PRO._num_modes; pp++){
//            for( int64_t xy=0; xy<A.OBJ._n_pix; xy++){
//                PRO[npr][pp][xy] = A.PRO[npr][pp][xy];
//            }}}
//
//    //Copy the views
//    this->PSI.init( A.PSI._num_field, A.PSI._num_modes, A.PSI._pix_x, A.PSI._pix_y);
//    for( int64_t ff=0; ff<A.PSI._num_field; ff++){
//        for( int64_t mm=0; mm<A.PSI._num_modes; mm++){
//            for( int64_t xy=0; xy<A.OBJ._n_pix; xy++){
//                PSI[ff][mm][xy] = A.PSI[ff][mm][xy];
//            }}}
//
//    //Copy the objects
//    this->OBJ.init( A.OBJ._num_field, A.OBJ._num_modes, A.OBJ._pix_x, A.OBJ._pix_y);
//    for( int64_t nob=0; nob<A.OBJ._num_field; nob++){
//        for( int64_t oo=0; oo<A.OBJ._num_modes; oo++){
//            for( int64_t xy=0; xy<A.OBJ._n_pix; xy++){
//                OBJ[nob][oo][xy] = A.OBJ[nob][oo][xy];
//            }}}
//    return *this;
//}


#endif
