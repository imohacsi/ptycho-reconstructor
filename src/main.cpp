#include <iostream>
#include <omp.h>
#include "config/config.hpp"
#include "./utilities/data_pipeline.hpp"
#include "./pipelines/pipeline_double.hpp"
#include "./pipelines/pipeline_float.hpp"
using namespace std;

int main(int argc,char *argv[]) {
    /**Read config files**/
    printf("%sNumber of command line arguments parameters: %d%s\n",A_C_MAGENTA,argc,A_C_RESET);fflush(stdout);
    std::string inifile("./input/basicconfig_p11nov.ini");
    std::string scanfile("./input/ScanList.dat");
    configure CONFIG;
    CONFIG.run_reconfig(inifile,scanfile);
    if(argc>1) inifile = argv[1];
    if(argc>2) scanfile = argv[2];
    CONFIG.run_reconfig(inifile,scanfile);

    /**Setting number of threads for upcoming processing**/
    omp_set_num_threads(CONFIG.wconfig._num_THREADS);
    /******************************************************************/
    /******************************************************************/
    /******************************************************************/
    /******************************************************************/
    /******************************************************************/




    data_pipeline data_preprocessor( CONFIG.data_in, CONFIG.pproc_in, CONFIG.wconfig );
    data_preprocessor.run_data_pipeline();
    ptyconf   rec_config  = data_preprocessor.get_ptyconf();
    expdata   exp_data    = data_preprocessor.get_expdata();

//    printf("NI\n");fflush(stdout);

    /**Start the actual reconstruction**/
    if(CONFIG.engin_in.precision=="double") {
        pipeline_fp64 dp_pipeline(CONFIG,rec_config,exp_data);
        dp_pipeline.run_pipeline();
    }else if(CONFIG.engin_in.precision=="float" ){
        pipeline_fp32 sp_pipeline(CONFIG,rec_config,exp_data);
        sp_pipeline.run_pipeline();
    }
    return 0;
}
