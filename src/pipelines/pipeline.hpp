#ifndef PTYREC_PIPELINE_HPP_
#define PTYREC_PIPELINE_HPP_
#include "../config/config.hpp"
#include "../pipelines/engines/engines.hpp"
#include "../pipelines/engines/parallelconfig.hpp"
#include "./auxiliary_iterators/position_refinement/posref.hpp"


class pipeline {
    public:
        pipeline(configure conf, ptyconf wconf, expdata &edata): _config(conf),_wconf(wconf),_edata(edata) {};

    protected:
        configure       _config;
        ptyconf         _wconf;
        expdata         _edata;

        /**Configure run tag**/
        ptyconf conf_tag( ptyconf wconf_in, char* tag, int idx ) {
            ptyconf wconf_out(wconf_in);
            char newbname[512];
            sprintf(newbname,"%s_%s%d",wconf_out._batch_name,tag,idx);
            strcpy(wconf_out._batch_name, newbname);
            return wconf_out;
        }

        /**Set configuration options for the reconstruction engine**/
        void conf_receng( int rstatus) {
            if( rstatus==0 ) {
                _wconf._num_curr_iter = _wconf._num_rec_iter;
                _config.engin_in.algorithm_curr = _config.engin_in.algorithm_rec;
            } else {
                _wconf._num_curr_iter = _wconf._num_ref_iter;
                _config.engin_in.algorithm_curr = _config.engin_in.algorithm_ref;
            }
        }

        /**Set configuration options for the initial guess**/
        void conf_iguess(std::string alg, int rstatus ) {
            /**Select between batch and minibatch algorithms**/
            if( alg=="DM_CPU" || alg=="UNST_CPU") {
                _config.guess_in._ipsi_batchtype = "BATCH";
            } else {
                _config.guess_in._ipsi_batchtype = "MINIBATCH";
            }
        }



};


#endif // PTYREC_PIPELINE_HPP_
