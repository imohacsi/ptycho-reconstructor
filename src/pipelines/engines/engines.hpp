#ifndef DESYPTYCHO_ENGINE_FORMATS_H_
#define DESYPTYCHO_ENGINE_FORMATS_H_

#include "../engines/engine_format.hpp"
#include "./templates_fullbatch/parallel_templates.hpp"
#include "./templates_minibatch/minibatch_templates.hpp"
//#include "./templates_cl/cl_templates_switch.hpp"


#include "../../print_data/print_data.hpp"


template <typename TY>
class motor {
public:
	ptyconf   _Wconf;
	ptydata<TY>     _rdata;
	ptydata<TY>     _Rdata;
	engine_format   _econf;
	parformat       _rconf;

	void run_engine_dmap();
	void run_engine_newt();
	void run_engine_epie();
	void run_engine_maxl();
	void run_engine_gpu_epie();
	void run_engine_gpu_maxl();

	void make_reconstruction();
	void print_final_data();

	motor(ptyconf wconf, ptydata<TY> rdata, engine_format engf):  _Wconf(wconf), _rdata(rdata), _econf(engf),
		_rconf( wconf, engf.engcom, engf.auxcom) {};
	void run_reconstruction_worker();
};


/**Set up and run the batch difference map reconstruction**/
template <typename TY>
void motor<TY>::run_engine_dmap() {
	printf("%sStarting reconstruction with: double precision difference map algorithm...%s\n",A_C_GREEN,A_C_RESET);
	printf("%s\tFULL BATCH METHOD!%s\n",A_C_YELLOW,A_C_RESET);
	fflush(stdout);

	_rconf._viewsel_lvl=1; //DM view selection
	_rconf._do_weak_obj=0; //Phase-only object
	batch_engine_template<double> bmotor( _rconf, _rdata);
	/**Launching reconstruction**/
	bmotor.run_reconstruction();
	/**Retrieving reconstructed data**/
	this->_rdata=bmotor.getDataCurrent();
	this->_Rdata=bmotor.getDataRelaxed();
}

/**Set up and run the batch Newton's method reconstruction**/
template <typename TY>
void motor<TY>::run_engine_newt() {
	printf("%sStarting reconstruction with: double precision Newton's method algorithm...%s\n",A_C_GREEN,A_C_RESET);
	printf("%s\tFULL BATCH METHOD!%s\n",A_C_YELLOW,A_C_RESET);
	fflush(stdout);

	_rconf._viewsel_lvl=0; //DM view selection
	_rconf._do_weak_obj=0; //Phase-only object
	batch_engine_template<double> bmotor( _rconf, _rdata);
	bmotor.run_reconstruction();
	this->_rdata=bmotor.getDataCurrent();
	this->_Rdata=bmotor.getDataRelaxed();
}

/**Set up and run the minibatch ePIE reconstruction**/
template <typename TY>
void motor<TY>::run_engine_epie() {
	printf("%sStarting reconstruction with: double precision ePIE algorithm...%s\n",A_C_GREEN,A_C_RESET);
	printf("%s\tMINIBATCH METHOD!%s\n",A_C_YELLOW,A_C_RESET);
	fflush(stdout);

	_rconf._mbatch_alg=0;    //ePIE algorithm
	minibatch_engine_template<double> bmotor( _rconf, _rdata);
	bmotor.run_reconstruction();
	this->_rdata=bmotor.getDataCurrent();
	this->_Rdata=bmotor.getDataRelaxed();
}

/**Set up and run the minibatch Maximum likelihood reconstruction**/
template <typename TY>
void motor<TY>::run_engine_maxl() {
	printf("%sStarting reconstruction with: double precision Maximum likelihood algorithm...%s\n",A_C_GREEN,A_C_RESET);
	printf("%s\tMINIBATCH METHOD!%s\n",A_C_YELLOW,A_C_RESET);
	fflush(stdout);

	_rconf._mbatch_alg=1; //ML algorithm
	minibatch_engine_template<double> bmotor( _rconf, _rdata);
	bmotor.run_reconstruction();
	this->_rdata=bmotor.getDataCurrent();
	this->_Rdata=bmotor.getDataRelaxed();
}

/**Set up and run the minibatch Maximum likelihood reconstruction
template <typename TY>
void motor<TY>::run_engine_gpu_epie() {
	printf("%sStarting reconstruction with: single precision ePIE algorithm (w OpenCL)...%s\n",A_C_GREEN,A_C_RESET);
	printf("%s\tMINIBATCH METHOD!%s\n",A_C_YELLOW,A_C_RESET);
	fflush(stdout);

	_rconf._mbatch_alg=0; //ePIE algorithm
	cl_engine_template bmotor( _rconf, _rdata);
	bmotor.run_reconstruction();
	this->_rdata=bmotor.getDataCurrent();
	this->_Rdata=bmotor.getDataRelaxed();
}
**/

/**Set up and run the minibatch Maximum likelihood reconstruction
template <typename TY>
void motor<TY>::run_engine_gpu_maxl() {
	printf("%sStarting reconstruction with: single precision ePIE algorithm (w OpenCL)...%s\n",A_C_GREEN,A_C_RESET);
	printf("%s\tMINIBATCH METHOD!%s\n",A_C_YELLOW,A_C_RESET);
	fflush(stdout);

	_rconf._mbatch_alg=1; //Maximum likelihood algorithm
	cl_engine_template bmotor( _rconf, _rdata);
	bmotor.run_reconstruction();
	this->_rdata=bmotor.getDataCurrent();
	this->_Rdata=bmotor.getDataRelaxed();
}
**/

/**Do the actual reconstruction*/
template <typename TY>
void motor<TY>::make_reconstruction() {
	if(_econf.algorithm_curr=="DM_CPU") { this->run_engine_dmap(); }
	else if(_econf.algorithm_curr=="UNST_CPU") { this->run_engine_newt(); }
	else if(_econf.algorithm_curr=="EPIE_CPU") { this->run_engine_epie(); }
	else if(_econf.algorithm_curr=="MAXLIKE_CPU") { this->run_engine_maxl(); }
	//else if(_econf.algorithm_curr=="EPIE_GPU") { this->run_engine_gpu_epie(); }
	//else if(_econf.algorithm_curr=="MAXLIKE_GPU") { this->run_engine_gpu_maxl(); }
	else{ printf("\nERROR: Unknown method for reconstruction: %s\n",_econf.algorithm_curr.c_str() ); }
	printf("Fnished with reconstruction!\n");
	fflush(stdout);

}

/**Run da batch!**/
template <typename TY>
void motor<TY>::run_reconstruction_worker() {
	make_reconstruction();
	printf("Done with iterations\n");
	fflush(stdout);
}


#endif
