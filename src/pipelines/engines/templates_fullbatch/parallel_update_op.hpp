#ifndef PTYREC_ENGINE_PARALLEL_UPDATE_OP_H_
#define PTYREC_ENGINE_PARALLEL_UPDATE_OP_H_

#include <cstring>
#include <omp.h>
#include <time.h>

#include "../parallelconfig.hpp"
#include "../../../utilities/colorconsole.hpp"
#include "../../../utilities/Field2D.hpp"
#include "../../../utilities/Field2D_ops.hpp"
#include "../../../utilities/wavefield.hpp"

#define Npx _rconf._num_pixels
#define xyobj (xx+yy*_rdata.OBJ[_rdata._olist[ff]][oo].npx_x+_rdata._pos1D[ff])
#define xypro (xx+yy*_rconf._num_pixels)


template <typename TY>
class batch_probj_update {
public:
	parformat           _rconf;
	ptydata<TY>         _rdata;



	/**Compact constructor and destructor**/
	batch_probj_update(parformat settings, ptydata<TY> data): _rconf(settings),_rdata(data) {
		init();
	}

	~batch_probj_update() {
		printf("DESTROYING BATCH_PROBJ_UPDATE\n");
		fflush(stdout);
		finish();
		printf("\tDONE\n");
		fflush(stdout);
	}

	void UpdateObjectProbe(int);

protected:
	trifield<double> normOBJ;
	trifield<double> normPRO;

	void init();
	void finish();

	/**Object and probe update functions**/
	void Update_PRO();
	void mapUpdPRO();
	void Update_OBJ();
	void mapUpdOBJ();
	void mapUpdOBJ_weak();

	/**Additional support constraints**/
	void supProbeRSupport();
	void supProbeQSupport();

};

/**Initializer outside of class declaration**/
template <typename TY>
void batch_probj_update<TY>::init() {
	normOBJ.init( _rconf._num_obj, _rconf._num_omodes, _rdata.OBJ[0][0].npx_x, _rdata.OBJ[0][0].npx_y );
	normPRO.init( _rconf._num_pro, _rconf._num_pmodes, _rdata.PRO[0][0].npx_x, _rdata.PRO[0][0].npx_y );
}


/**Destructor outside of class declaration**/
template <typename TY>
void batch_probj_update<TY>::finish() {
	normOBJ.free();
	normPRO.free();
}


/**Performs the object update including resetting temporary buffers
    and capping transmittance at 100%.**/
template <typename TY>
void batch_probj_update<TY>::Update_OBJ() {
	int64_t nob,oo;

	/**Initialize dummy values to avoid div0**/
	for(nob=0; nob<_rconf._num_obj; nob++) {
		for(oo=0; oo<_rconf._num_omodes; oo++) {
			_rdata.OBJ[nob][oo]=1e-6;
			normOBJ[nob][oo]=1e-6;
		}
	}

	/**Do the actual update process*/
	if(_rconf._do_weak_obj) {
		mapUpdOBJ_weak();
	} else {
		mapUpdOBJ();
	}

	/**Cap object values */
	for(nob=0; nob<_rconf._num_obj; nob++) {
		for(oo=0; oo<_rconf._num_omodes; oo++) {
			_rdata.OBJ[nob][oo].par_div( normOBJ[nob][oo] );
			par_cap( _rdata.OBJ[nob][oo] );
		}
	}
}

/**Calculates the intensity-weighted updated object map and its normalization**/
template <typename TY>
void batch_probj_update<TY>::mapUpdOBJ() {
	int64_t nob,npr,oo,pp,ff,xx,yy;

	for(oo=0; oo<_rconf._num_omodes; oo++) {
		for(pp=0; pp<_rconf._num_pmodes; pp++) {
			for(ff=0; ff<_rconf._num_frames; ff++) {
				nob = _rdata._olist[ff];
				npr = _rdata._plist[ff];
				#pragma omp parallel for private(xx)
				for(yy=0; yy<Npx; yy++) {
					for(xx=0; xx<Npx; xx++) {
						_rdata.OBJ[nob][oo][xyobj]+=conj(_rdata.PRO[npr][pp][xx+yy*Npx])*_rdata.PSI[pp+oo*_rconf._num_pmodes][ff][xx+yy*Npx];
						normOBJ[nob][oo][xyobj]+=norm(_rdata.PRO[npr][pp][xx+yy*Npx]);
					}
				}
			}
		}
	}
}

/**Calculates the intensity-weighted updated object map and its normalization in weak object approximation**/
template <typename TY>
void batch_probj_update<TY>::mapUpdOBJ_weak() {
	int64_t oo,pp,ff,xx,yy;
	complex<TY> nrmobj;

	for(oo=0; oo<_rconf._num_omodes; oo++) {
		for(pp=0; pp<_rconf._num_pmodes; pp++) {
			for(ff=0; ff<_rconf._num_frames; ff++) {
				#pragma omp parallel for private(xx,nrmobj) schedule(dynamic, 8)
				for(yy=0; yy<Npx; yy++) {
					for(xx=0; xx<Npx; xx++) {
						nrmobj=_rdata.PSI[pp+oo*_rconf._num_pmodes][ff][xx+yy*Npx]/(_rdata.PRO[_rdata._plist[ff]][pp][xx+yy*Npx]);
						nrmobj*=1.0/abs(nrmobj);
						_rdata.OBJ[_rdata._olist[ff]][oo][xyobj]+=norm(_rdata.PRO[_rdata._plist[ff]][pp][xx+yy*Npx])*nrmobj;
						normOBJ[_rdata._olist[ff]][oo][xyobj]+=norm(_rdata.PRO[_rdata._plist[ff]][pp][xx+yy*Npx]);
					}
				}
			}
		}
	}
}


/**Performs the probe update including the allocation of
*   temporary buffers but does no fancy stabilization algorithms. **/
template <typename TY>
void batch_probj_update<TY>::Update_PRO() {
	int64_t npr,pp;
	TY weight=_rconf._opupd_regpro*double(_rconf._num_frames);

	/**Leave some memory in the system from the previous iteration*/
	for(npr=0; npr<_rconf._num_pro; npr++) {
		for(pp=0; pp<_rconf._num_pmodes; pp++) {
			_rdata.PRO[npr][pp].par_mul(weight);
			normPRO[npr][pp]=weight;
		}
	}

	/**Do the actual update process*/
	mapUpdPRO();

	/**Normalize the updated probe*/
	for(npr=0; npr<_rconf._num_pro; npr++) {
		for(pp=0; pp<_rconf._num_pmodes; pp++) {
			_rdata.PRO[npr][pp].par_div( normPRO[npr][pp] );
		}
	}

	/**Calculate the necessary supports for the probe**/
	if(_rconf._do_prsup_r) {
		supProbeRSupport( );
	}
	if(_rconf._do_prsup_q) {
		supProbeQSupport( );
	}

}


/**Calculates the intensity-weighted updated object map and its normalization**/
template <typename TY>
void batch_probj_update<TY>::mapUpdPRO() {
	int64_t oo,pp,ff,xx,yy;

	for(oo=0; oo<_rconf._num_omodes; oo++) {
		for(pp=0; pp<_rconf._num_pmodes; pp++) {
			for(ff=0; ff<_rconf._num_frames; ff++) {
				#pragma omp parallel for private(xx)
				for(yy=0; yy<Npx; yy++) {
					for(xx=0; xx<Npx; xx++) {
						_rdata.PRO[_rdata._plist[ff]][pp][xx+yy*Npx]+=conj(_rdata.OBJ[_rdata._olist[ff]][oo][xyobj])*_rdata.PSI[pp+oo*_rconf._num_pmodes][ff][xx+yy*Npx];
						normPRO[_rdata._plist[ff]][pp][xx+yy*Npx]+=norm(_rdata.OBJ[_rdata._olist[ff]][oo][xyobj]);

					}
//				    #pragma omp simd
//					for(xx=0; xx<Npx; xx++) {
//					}
				}
			}
		}
	}
}



/**Update the object and probe estimates using a few iterations of weighted averaging**/
template <typename TY>
void batch_probj_update<TY>::UpdateObjectProbe( int niter ) {
	for(int64_t it=0; it<_rconf._opupd_niter; it++) {
		if(niter>=_rconf._start_objupd) {
			Update_OBJ();
		}
		if(niter>=_rconf._start_proupd) {
			Update_PRO();
		}
	}
}



/**Apply a real space constraint to the probe (cut the corners)**/
template <typename TY>
void batch_probj_update<TY>::supProbeRSupport() {
	double position;
	int64_t npr,pp,xx,yy;

	/**Do the actual update process*/
	for(npr=0; npr<_rconf._num_pro; npr++) {
		for(pp=0; pp<_rconf._num_pmodes; pp++) {
			#pragma omp parallel for private(xx,position)
			for(yy=0; yy<Npx; yy++) {
				for(xx=0; xx<Npx; xx++) {
					position = sqrt( (xx-Npx/2)*(xx-Npx/2)+(yy-Npx/2)*(yy-Npx/2) );
					if( position>_rconf._prsup_r) {
						_rdata.PRO[npr][pp][xx+yy*Npx]*=1e-3;
					}
				}
			}
		}
	}
}

template <typename TY>
void batch_probj_update<TY>::supProbeQSupport() { }





#endif

