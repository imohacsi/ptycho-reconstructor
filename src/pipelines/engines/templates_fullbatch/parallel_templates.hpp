#ifndef DESYPTYCHO_ENGINE_PAR_TEMPLATE_H_
#define DESYPTYCHO_ENGINE_PAR_TEMPLATE_H_

#include <cstring>
#include <fftw3.h>
#include <omp.h>
#include <time.h>

#include "../parallelconfig.hpp"
#include "./parallel_update_psi.hpp"
#include "./parallel_update_op.hpp"

#define Npx _rconf._num_pixels

template <typename TY>
class batch_engine_template {
public:
	void run_reconstruction();
	ptydata<TY> getDataRelaxed() {
		return _anndata;
	}
	ptydata<TY> getDataCurrent() {
		return _rdata;
	}

	batch_engine_template(parformat rconf, ptydata<TY> rdata): _rconf(rconf), _rdata(rdata),view_update_loop(_rconf,_rdata),opro_update_loop(_rconf,_rdata) {
		init();
	};

	/**Keep the API minimal**/
protected:
	parformat           _rconf;
	ptydata<TY>         _rdata;
	ptydata<TY>         _anndata;

	batch_view_update<TY> view_update_loop;
	batch_probj_update<TY> opro_update_loop;
	void init();

	/**Make a single iteration**/
	int64_t niter;
	void make_iteration();

	/**Simulated annealing**/
	int64_t num_relaxed;
	void relaxAddCurrent();
	void relaxNormalize();

	/**Error diagnostics**/
	double *_prmodeint;
	void diag_print_info();
	void diagRSpace(int64_t,int64_t);
	void diag_prmode_int();

	/**Real time plotting*/
	void print_current_iteration();
};

/**Initializer outside of class declaration**/
template <typename TY>
void batch_engine_template<TY>::init() {
	num_relaxed = 0;

	_anndata.OBJ.init(_rconf._num_obj, _rconf._num_omodes, _rdata.OBJ[0][0].npx_x, _rdata.OBJ[0][0].npx_y );
	_anndata.PRO.init(_rconf._num_pro, _rconf._num_pmodes, _rdata.PRO[0][0].npx_x, _rdata.PRO[0][0].npx_y );
}




/**Single command to run the reconstruction**/
template <typename TY>
void batch_engine_template<TY>::run_reconstruction() {
	num_relaxed=0;

	for(niter=0; niter<_rconf._num_curr_iter; niter++) {
		make_iteration();

		/**Add to simulated annealing over last iterations for noise reduction*/
		if(niter>(_rconf._num_curr_iter*3)/4) {
			relaxAddCurrent();
		}

		if(niter%_rconf._WriteEveryNthIter==0) {
			view_update_loop.diag_print_info();
			print_current_iteration();
		}
	}
	printf("Done with iterations!\n");
	fflush(stdout);

	relaxNormalize();

}



template <typename TY>
void batch_engine_template<TY>::make_iteration() {
	/**Re-set iteration parameters**/
	view_update_loop.diag_reset_error();
	clock_t Tstart,Tend;
	Tstart=clock();

	/**Update views**/
	#pragma omp parallel for
	for(ssize_t ff=0; ff<_rconf._num_frames; ff++) {
		view_update_loop.image_pipeline(ff);
	}

	/**Update the object and probe estimates**/
	opro_update_loop.UpdateObjectProbe(niter);

	/**Print relative intensity of multiple probe modes**/
	if(_rconf._do_calc_print && _rconf._num_pmodes>1) {
		diag_prmode_int();
	}

//	if( niter>10 && niter%20==1){
//        printf("\nWARNING: TEMPORARY ORTHOGONALIZATION!!!\n");
//        _rdata.PRO.linalg_orth();
//	}

	/**Printing benchmark and status*/
	Tend=clock();
	double Titer=(double)(Tend-Tstart)/CLOCKS_PER_SEC;
	printf("%sFinished %d th of %d iter. (%g s)%s\n",A_C_GREEN, (int)niter,(int)_rconf._num_curr_iter,Titer, A_C_RESET);
	fflush(stdout);

}

/**Diagnostics to compare probe mode intensities**/
template <typename TY>
void batch_engine_template<TY>::diag_prmode_int() {
	double MODEINT[_rconf._num_pro][_rconf._num_pmodes];
	double TOTINT[_rconf._num_pro];

	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			MODEINT[npr][pp]= sum( _rdata.PRO[npr][pp] );
			TOTINT[npr]+=MODEINT[npr][pp];
		}
	}

	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		printf("\t%sIntensity in the %dth probe's modes: ", A_C_YELLOW, (int)npr);
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			printf( "%0.4g",MODEINT[npr][pp]/TOTINT[npr]);
		}
		printf("%s\n",A_C_RESET);
		fflush(stdout);
	}
}

/**Add current object and probe to relaxation**/
template <typename TY>
void batch_engine_template<TY>::relaxAddCurrent() {
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			_anndata.PRO[npr][pp]+=_rdata.PRO[npr][pp];
		}
	}

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			_anndata.OBJ[nob][oo]+=_rdata.OBJ[nob][oo];
		}
	}

	num_relaxed++;
}

/**Normalize relaxation**/
template <typename TY>
void batch_engine_template<TY>::relaxNormalize() {
	TY norm = 1.0/(double)num_relaxed;

	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			_anndata.PRO[npr][pp]*=norm;
		}
	}

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			_anndata.OBJ[nob][oo]*=norm;
		}
	}
}

/**Write out current progress as bitmap files**/
template <typename TY>
void batch_engine_template<TY>::print_current_iteration() {
	char filename[512];
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			sprintf(filename,"./output/Citer_Probe_P%d_M%d_I.bmp",(int)npr,(int)pp);
			ImWrite(_rdata.PRO[npr][pp],"lin","int","BMP",filename);
			sprintf(filename,"./output/Citer_Probe_P%d_M%d_P.bmp",(int)npr,(int)pp);
			ImWrite(_rdata.PRO[npr][pp],"lin","arg","BMP",filename);
		}
	}

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			sprintf(filename,"./output/Citer_Object_O%d_M%d_I.bmp",(int)nob,(int)oo);
			ImWrite(_rdata.OBJ[nob][oo],"lin","int","BMP",filename);
			sprintf(filename,"./output/Citer_Object_O%d_M%d_P.bmp",(int)nob,(int)oo);
			ImWrite(_rdata.OBJ[nob][oo],"lin","arg","BMP",filename);
		}
	}
}

#endif


