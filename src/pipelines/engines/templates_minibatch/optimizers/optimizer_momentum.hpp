#ifndef PTYREC_ENGINE_OPTIMIZER_MOMENTUM_H_
#define PTYREC_ENGINE_OPTIMIZER_MOMENTUM_H_

#include <cstring>
#include <omp.h>
#include <time.h>

#include "../../parallelconfig.hpp"
#include "../../../../utilities/colorconsole.hpp"
#include "../../../../utilities/Field2D.hpp"
#include "../../../../utilities/Field2D_ops.hpp"
#include "../../../../utilities/wavefield.hpp"

#define Npx _rconf._num_pixels
#define xyobj (xx+yy*_rdata.OBJ[_rdata._olist[ff]][oo].npx_x+_rdata._pos1D[ff])
#define xypro (xx+yy*_rconf._num_pixels)


template <typename TY>
class optimizer_momentum: public optimizer<TY> {
public:
	TY _momentum = 0.9;

	/**Compact constructor and destructor**/
	optimizer_momentum(parformat settings, ptydata<TY> &data): optimizer<TY>::optimizer(settings,data) { };
	optimizer_momentum(parformat settings): optimizer<TY>::optimizer(settings) { };

protected:
	void set_gradients_obj( trifield<complex<TY>>& );
	void set_gradients_pro( trifield<complex<TY>>& );
};



/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer_momentum<TY>::set_gradients_obj(trifield<complex<TY>>& grad_o ) {
	int64_t nob,oo,xy;
//    printf("Setting momentum optimizer gradients\n");

	//Updating the momentum
	for(nob=0; nob<this->_rconf._num_obj; nob++) {
		for(oo=0; oo<this->_rconf._num_omodes; oo++) {
            #pragma omp parallel for
			for(xy=0; xy<this->_rdata.OBJ[nob][oo].n_pix; xy++) {
                this->gradientsOBJ[nob][oo][xy] = _momentum*this->gradientsOBJ[nob][oo][xy]+((TY)1.0-_momentum)*grad_o[nob][oo][xy]; } } }
}

/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer_momentum<TY>::set_gradients_pro(trifield<complex<TY>>& grad_p ) {
	int64_t npr,pp,xy;

	//Updating the momentum
	for(npr=0; npr<this->_rconf._num_pro; npr++) {
		for(pp=0; pp<this->_rconf._num_pmodes; pp++) {
            #pragma omp parallel for
			for(xy=0; xy<this->_rdata.PRO[npr][pp].n_pix; xy++) {
            this->gradientsPRO[npr][pp][xy] = _momentum*this->gradientsPRO[npr][pp][xy] + ((TY)1.0-_momentum)*grad_p[npr][pp][xy]; } } }
}

#undef Npx
#undef xyobj
#undef xypro
#endif

