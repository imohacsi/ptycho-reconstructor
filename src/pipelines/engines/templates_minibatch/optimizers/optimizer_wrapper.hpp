#ifndef PTYREC_ENGINE_OPTIMIZER_WRAPPER_H_
#define PTYREC_ENGINE_OPTIMIZER_WRAPPER_H_

#include "./optimizer_gradient.hpp"
#include "./optimizer_momentum.hpp"
#include "./optimizer_rmsprop.hpp"
#include "./optimizer_adadelta.hpp"

template <typename TY>
class optimizer_wrapper {
    protected:
    int _otype = 0;
    optimizer_gradient<TY> _o_grad;
    optimizer_momentum<TY> _o_mom;
    optimizer_rmsprop<TY>  _o_rms;
    optimizer_adadelta<TY>  _o_ada;



    public:
    /**Compact constructor and destructor**/
	optimizer_wrapper( std::string type, parformat settings ): _o_grad(settings),_o_mom(settings),_o_rms(settings),_o_ada(settings) {
        if( type=="GRADIENT"){ _otype=0; }
        else if( type=="MOMENTUM" ){ _otype=1; }
        else if( type=="RMSPROP"){ _otype=2; }
        else if( type=="ADADELTA"){ _otype=3; }
        else{ printf("%s\tERROR: Unknown optimizer: %s%s\n",A_C_RED,type.c_str(),A_C_RESET); exit(-1); }
	};

	void setParameters(parformat settings, ptydata<TY> &data) {
	    if( _otype==0 ){ _o_grad.setParameters( settings, data); }
	    else if( _otype==1 ){ _o_mom.setParameters( settings, data); }
	    else if( _otype==2 ){ _o_rms.setParameters( settings, data); }
	    else if( _otype==3 ){ _o_ada.setParameters( settings, data); }
	};

	void UpdateOBJ( trifield<complex<TY>>&  _gr_obj){
	    if( _otype==0 ){ _o_grad.UpdateOBJ(_gr_obj); }
	    else if( _otype==1 ){ _o_mom.UpdateOBJ(_gr_obj); }
	    else if( _otype==2 ){ _o_rms.UpdateOBJ(_gr_obj); }
	    else if( _otype==3 ){ _o_ada.UpdateOBJ(_gr_obj); }
	};

	void UpdatePRO( trifield<complex<TY>>&  _gr_pro){
	    if( _otype==0 ){ _o_grad.UpdatePRO(_gr_pro); }
	    else if( _otype==1 ){ _o_mom.UpdatePRO(_gr_pro); }
	    else if( _otype==2 ){ _o_rms.UpdatePRO(_gr_pro); }
	    else if( _otype==3 ){ _o_ada.UpdatePRO(_gr_pro); }
	};

};


#endif // PTYREC_ENGINE_OPTIMIZER_WRAPPER_H_


