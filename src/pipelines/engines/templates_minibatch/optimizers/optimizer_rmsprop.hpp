#ifndef PTYREC_ENGINE_OPTIMIZER_RMSPROP_H_
#define PTYREC_ENGINE_OPTIMIZER_RMSPROP_H_

#include <cstring>
#include <omp.h>
#include <time.h>

#include "../../parallelconfig.hpp"
#include "../../../../utilities/colorconsole.hpp"
#include "../../../../utilities/Field2D.hpp"
#include "../../../../utilities/Field2D_ops.hpp"
#include "../../../../utilities/wavefield.hpp"

#define Npx _rconf._num_pixels
#define xyobj (xx+yy*_rdata.OBJ[_rdata._olist[ff]][oo].npx_x+_rdata._pos1D[ff])
#define xypro (xx+yy*_rconf._num_pixels)


template <typename TY>
class optimizer_rmsprop: public optimizer<TY> {
public:
	TY _gamma = 0.9;

	trifield<complex<TY>> Eavg_o;
	trifield<complex<TY>> Eavg_p;

	/**Compact constructor and destructor**/
	optimizer_rmsprop(parformat settings, ptydata<TY> &data): optimizer<TY>::optimizer(settings,data) { };
	optimizer_rmsprop(parformat settings): optimizer<TY>::optimizer(settings) { };

protected:
	void set_gradients_obj( trifield<complex<TY>>& );
	void set_gradients_pro( trifield<complex<TY>>& );

	void init();
    void finish();

};

/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer_rmsprop<TY>::init( ) {
    optimizer<TY>::init();
    printf("Called RMSPROP optimizer initializer!");fflush(stdout);
    Eavg_o.init(this->_rconf._num_obj, this->_rconf._num_omodes, this->_rdata.OBJ._pix_x, this->_rdata.OBJ._pix_y);
    Eavg_p.init(this->_rconf._num_pro, this->_rconf._num_pmodes, this->_rdata.PRO._pix_x, this->_rdata.PRO._pix_y);
}

/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer_rmsprop<TY>::finish( ) {
    optimizer<TY>::finish();
    Eavg_o.free();
    Eavg_p.free();
}


/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer_rmsprop<TY>::set_gradients_obj(trifield<complex<TY>>& grad_o ) {
	int64_t nob,oo,xy;

    for(nob=0; nob<this->_rconf._num_obj; nob++) {
    for(oo=0; oo<this->_rconf._num_omodes; oo++) {
        #pragma omp parallel for
        for(xy=0; xy<this->_rdata.OBJ[nob][oo].n_pix; xy++) {
            Eavg_o[nob][oo][xy] = _gamma*Eavg_o[nob][oo][xy] + ((TY)1.0-_gamma)*norm(grad_o[nob][oo][xy]); } } }

    for(nob=0; nob<this->_rconf._num_obj; nob++) {
		for(oo=0; oo<this->_rconf._num_omodes; oo++) {
            #pragma omp parallel for
			for(xy=0; xy<this->_rdata.OBJ[nob][oo].n_pix; xy++) {
                this->gradientsOBJ[nob][oo][xy] = grad_o[nob][oo][xy]/sqrt(abs(Eavg_o[nob][oo][xy])+(TY)1e-8); } } }
}


/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer_rmsprop<TY>::set_gradients_pro(trifield<complex<TY>>& grad_p ) {
	int64_t npr,pp,xy;

	//Updating the momentum
	for(npr=0; npr<this->_rconf._num_pro; npr++) {
		for(pp=0; pp<this->_rconf._num_pmodes; pp++) {
            #pragma omp parallel for
			for(xy=0; xy<this->_rdata.PRO[npr][pp].n_pix; xy++) {
                Eavg_p[npr][pp][xy] = _gamma*Eavg_p[npr][pp][xy] + ((TY)1.0-_gamma)*norm(grad_p[npr][pp][xy]); } } }

	//Updating the momentum
	for(npr=0; npr<this->_rconf._num_pro; npr++) {
		for(pp=0; pp<this->_rconf._num_pmodes; pp++) {
            #pragma omp parallel for
			for(xy=0; xy<this->_rdata.PRO[npr][pp].n_pix; xy++) {
            this->gradientsPRO[npr][pp][xy] = grad_p[npr][pp][xy]/sqrt(abs(Eavg_p[npr][pp][xy])+(TY)1e-3); } } }
//            this->gradientsPRO[npr][pp][xy] = _gamma*this->gradientsPRO[npr][pp][xy] + (1.0-_gamma)*grad_p[npr][pp][xy]; }     } }
}

#undef Npx
#undef xyobj
#undef xypro
#endif // PTYREC_ENGINE_OPTIMIZER_RMSPROP_H_

