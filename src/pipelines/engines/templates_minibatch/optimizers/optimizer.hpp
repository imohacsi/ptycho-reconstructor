#ifndef PTYREC_ENGINE_OPTIMIZERS_HPP_
#define PTYREC_ENGINE_OPTIMIZERS_HPP_


#include <cstring>
#include <omp.h>
#include <time.h>

#include "../../parallelconfig.hpp"
#include "../../../../utilities/colorconsole.hpp"
#include "../../../../utilities/Field2D.hpp"
#include "../../../../utilities/Field2D_ops.hpp"
#include "../../../../utilities/wavefield.hpp"

template <typename TY>
class optimizer {
public:
	parformat           _rconf;
	ptydata<TY>         _rdata;

	/**Compact constructor and destructor**/
	optimizer(parformat settings, ptydata<TY> &data): _rconf(settings),_rdata(data) { init(); };
	optimizer(parformat settings): _rconf(settings) { };
	~optimizer() { finish(); }

	void setParameters(parformat settings, ptydata<TY> &data) {
		_rconf=settings;
		_rdata=data;
		init();
	};

	virtual void UpdateOBJ( trifield<complex<TY>>& );
	virtual void UpdatePRO( trifield<complex<TY>>& );

protected:
	trifield<complex<TY>> gradientsOBJ;
	trifield<complex<TY>> gradientsPRO;

	virtual void set_gradients_obj( trifield<complex<TY>>& );
	virtual void set_gradients_pro( trifield<complex<TY>>& );
	void apply_gradients_obj( );
	void apply_gradients_pro( );
	virtual void init();
	void finish();

	/**Additional support constraints**/
	void supProbeRSupport();
	void supProbeQSupport();

};


template <typename TY>
void optimizer<TY>::init(){
    gradientsOBJ.init(_rconf._num_obj, _rconf._num_omodes, _rdata.OBJ._pix_x, _rdata.OBJ._pix_y);
    gradientsPRO.init(_rconf._num_pro, _rconf._num_pmodes, _rdata.PRO._pix_x, _rdata.PRO._pix_y);
}

template <typename TY>
void optimizer<TY>::finish(){
    gradientsOBJ.free();
    gradientsPRO.free();
}





/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer<TY>::set_gradients_obj( trifield<complex<TY>>& grad_o ) {
	int64_t nob,oo,xy;

	/**Update gradients*/
	for(nob=0; nob<_rconf._num_obj; nob++) {
		for(oo=0; oo<_rconf._num_omodes; oo++) {
			#pragma omp parallel for
			for(xy=0; xy<_rdata.OBJ[nob][oo].n_pix; xy++) {
				gradientsOBJ[nob][oo][xy] = grad_o[nob][oo][xy];
			}
		}
	}
}

/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer<TY>::set_gradients_pro( trifield<complex<TY>>& grad_p ) {
	int64_t npr,pp,xy;

	/**Update gradients*/
	for(npr=0; npr<_rconf._num_pro; npr++) {
		for(pp=0; pp<_rconf._num_pmodes; pp++) {
			#pragma omp parallel for
			for(xy=0; xy<_rdata.PRO[npr][pp].n_pix; xy++) {
				gradientsPRO[npr][pp][xy] = grad_p[npr][pp][xy];
			}
		}
	}
}


/**Apply the calculated object gradients **/
template <typename TY>
void optimizer<TY>::apply_gradients_obj( ) {
	int64_t nob,oo,xy;
	const TY lrate = _rconf._lrate_obj;

	/**Do the actual update process*/
	for(nob=0; nob<_rconf._num_obj; nob++) {
		for(oo=0; oo<_rconf._num_omodes; oo++) {
			#pragma omp parallel for
			for(xy=0; xy<_rdata.OBJ[nob][oo].n_pix; xy++) {
				_rdata.OBJ[nob][oo][xy]+= lrate * gradientsOBJ[nob][oo][xy];
			}
		}
	}
}

/**Apply the calculated probe gradients **/
template <typename TY>
void optimizer<TY>::apply_gradients_pro( ) {
	int64_t npr,pp,xy;
	const TY lrate = _rconf._lrate_pro;

	/**Do the actual update process*/
	for(npr=0; npr<_rconf._num_pro; npr++) {
		for(pp=0; pp<_rconf._num_pmodes; pp++) {
			#pragma omp parallel for
			for(xy=0; xy<_rdata.PRO[npr][pp].n_pix; xy++) {
				_rdata.PRO[npr][pp][xy]+= lrate * gradientsPRO[npr][pp][xy];
			}
		}
	}
}

/**Wrapper functions**/
template <typename TY>
void optimizer<TY>::UpdateOBJ( trifield<complex<TY>>& grad_o ){
    this->set_gradients_obj( grad_o );
    this->apply_gradients_obj();
}

template <typename TY>
void optimizer<TY>::UpdatePRO( trifield<complex<TY>>& grad_p ){
    this->set_gradients_pro( grad_p );
    this->apply_gradients_pro();
}


#endif // PTYREC_ENGINE_OPTIMIZERS_HPP_
