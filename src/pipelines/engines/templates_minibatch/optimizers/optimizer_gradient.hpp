#ifndef PTYREC_ENGINE_OPTIMIZER_GRADDESC_H_
#define PTYREC_ENGINE_OPTIMIZER_GRADDESC_H_

#include "./optimizer.hpp"
#include "../../parallelconfig.hpp"
#include "../../../../utilities/colorconsole.hpp"
#include "../../../../utilities/Field2D.hpp"
#include "../../../../utilities/Field2D_ops.hpp"
#include "../../../../utilities/wavefield.hpp"

#define Npx _rconf._num_pixels
#define xyobj (xx+yy*_rdata.OBJ[_rdata._olist[ff]][oo].npx_x+_rdata._pos1D[ff])
#define xypro (xx+yy*_rconf._num_pixels)
#define VERBOSITY_LEVEL 0


template <typename TY>
class optimizer_gradient: public optimizer<TY> {
    public:
	/**Compact constructor and destructor**/
	optimizer_gradient(parformat settings, ptydata<TY> &data): optimizer<TY>::optimizer(settings,data) { };
	optimizer_gradient(parformat settings): optimizer<TY>::optimizer(settings) { };
};

#endif

