#ifndef PTYREC_ENGINE_OPTIMIZER_ADADELTA_H_
#define PTYREC_ENGINE_OPTIMIZER_ADADELTA_H_

#include <cstring>
#include <omp.h>
#include <time.h>

#include "../../parallelconfig.hpp"
#include "../../../../utilities/colorconsole.hpp"
#include "../../../../utilities/Field2D.hpp"
#include "../../../../utilities/Field2D_ops.hpp"
#include "../../../../utilities/wavefield.hpp"

#define Npx _rconf._num_pixels
#define xyobj (xx+yy*_rdata.OBJ[_rdata._olist[ff]][oo].npx_x+_rdata._pos1D[ff])
#define xypro (xx+yy*_rconf._num_pixels)


template <typename TY>
class optimizer_adadelta: public optimizer<TY> {
public:
	TY _gamma = 0.9;

	trifield<complex<TY>> Eg_avg_o;
	trifield<complex<TY>> Eg_avg_p;
	trifield<complex<TY>> Et_avg_o;
	trifield<complex<TY>> Et_avg_p;

	/**Compact constructor and destructor**/
	optimizer_adadelta(parformat settings, ptydata<TY> &data): optimizer<TY>::optimizer(settings,data) { };
	optimizer_adadelta(parformat settings): optimizer<TY>::optimizer(settings) { };

protected:
	void set_gradients_obj( trifield<complex<TY>>& );
	void set_gradients_pro( trifield<complex<TY>>& );

	void init();
    void finish();

};

/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer_adadelta<TY>::init( ) {
    optimizer<TY>::init();
    printf("Called ADADELTA optimizer initializer!");fflush(stdout);
    Eg_avg_o.init(this->_rconf._num_obj, this->_rconf._num_omodes, this->_rdata.OBJ._pix_x, this->_rdata.OBJ._pix_y);
    Eg_avg_p.init(this->_rconf._num_pro, this->_rconf._num_pmodes, this->_rdata.PRO._pix_x, this->_rdata.PRO._pix_y);
    Et_avg_o.init(this->_rconf._num_obj, this->_rconf._num_omodes, this->_rdata.OBJ._pix_x, this->_rdata.OBJ._pix_y);
    Et_avg_p.init(this->_rconf._num_pro, this->_rconf._num_pmodes, this->_rdata.PRO._pix_x, this->_rdata.PRO._pix_y);
}

/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer_adadelta<TY>::finish( ) {
    optimizer<TY>::finish();
    Eg_avg_o.free();
    Eg_avg_p.free();
    Et_avg_o.free();
    Et_avg_p.free();
}


/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer_adadelta<TY>::set_gradients_obj(trifield<complex<TY>>& grad_o ) {
	int64_t nob,oo,xy;
    TY lrate_o = this->_rconf._lrate_obj;

    for(nob=0; nob<this->_rconf._num_obj; nob++) {
    for(oo=0; oo<this->_rconf._num_omodes; oo++) {
        #pragma omp parallel for
        for(xy=0; xy<this->_rdata.OBJ[nob][oo].n_pix; xy++) {
            Eg_avg_o[nob][oo][xy] = _gamma*Eg_avg_o[nob][oo][xy] + ((TY)1.0-_gamma)*norm(grad_o[nob][oo][xy]); } } }


    TY rms_o;
    for(nob=0; nob<this->_rconf._num_obj; nob++) {
		for(oo=0; oo<this->_rconf._num_omodes; oo++) {
            #pragma omp parallel for private(rms_o)
			for(xy=0; xy<this->_rdata.OBJ[nob][oo].n_pix; xy++) {
                rms_o = sqrt(abs(Et_avg_o[nob][oo][xy])+(TY)1e-8)/sqrt(abs(Eg_avg_o[nob][oo][xy])+(TY)1e-8);
                this->gradientsOBJ[nob][oo][xy] = rms_o*(grad_o[nob][oo][xy]); } } }

    for(nob=0; nob<this->_rconf._num_obj; nob++) {
    for(oo=0; oo<this->_rconf._num_omodes; oo++) {
        #pragma omp parallel for
        for(xy=0; xy<this->_rdata.OBJ[nob][oo].n_pix; xy++) {
            Et_avg_o[nob][oo][xy] = _gamma*Et_avg_o[nob][oo][xy] + ((TY)1.0-_gamma)*norm( (TY)1.0/sqrt(abs(Eg_avg_o[nob][oo][xy])+(TY)1e-8)*grad_o[nob][oo][xy]*lrate_o); } } }

}


/**Calculate new object and probe gradients **/
template <typename TY>
void optimizer_adadelta<TY>::set_gradients_pro(trifield<complex<TY>>& grad_p ) {
	int64_t npr,pp,xy;
    TY lrate_p = this->_rconf._lrate_pro;

	//Updating the momentum
	for(npr=0; npr<this->_rconf._num_pro; npr++) {
		for(pp=0; pp<this->_rconf._num_pmodes; pp++) {
            #pragma omp parallel for
			for(xy=0; xy<this->_rdata.PRO[npr][pp].n_pix; xy++) {
                Eg_avg_p[npr][pp][xy] = _gamma*Eg_avg_p[npr][pp][xy] + ((TY)1.0-_gamma)*norm(grad_p[npr][pp][xy]); } } }


	//Updating the momentum
	TY rms_p;
	for(npr=0; npr<this->_rconf._num_pro; npr++) {
		for(pp=0; pp<this->_rconf._num_pmodes; pp++) {
            #pragma omp parallel for private(rms_p)
			for(xy=0; xy<this->_rdata.PRO[npr][pp].n_pix; xy++) {
            rms_p = sqrt(abs(Et_avg_p[npr][pp][xy])+(TY)1e-8)/sqrt(abs(Eg_avg_p[npr][pp][xy])+(TY)1e-8);
            this->gradientsPRO[npr][pp][xy] = rms_p*(grad_p[npr][pp][xy]); } } }

	for(npr=0; npr<this->_rconf._num_pro; npr++) {
		for(pp=0; pp<this->_rconf._num_pmodes; pp++) {
            #pragma omp parallel for
			for(xy=0; xy<this->_rdata.PRO[npr][pp].n_pix; xy++) {
                Et_avg_p[npr][pp][xy] = _gamma*Et_avg_p[npr][pp][xy] + ((TY)1.0-_gamma)*norm( (TY)1.0/sqrt(abs(Eg_avg_p[npr][pp][xy])+(TY)1e-8)*grad_p[npr][pp][xy]*lrate_p); } } }

}

#undef Npx
#undef xyobj
#undef xypro
#endif // PTYREC_ENGINE_OPTIMIZER_ADADELTA_H_


