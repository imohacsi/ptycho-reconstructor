#ifndef PTYREC_ENGINE_MINIBATCH_UPDATE_PSI_H_
#define PTYREC_ENGINE_MINIBATCH_UPDATE_PSI_H_

#include <cstring>
#include <fftw3.h>
#include <omp.h>
#include <time.h>

#include "../parallelconfig.hpp"
#include "../../../utilities/colorconsole.hpp"
#include "../../../utilities/Field2D.hpp"
#include "../../../utilities/Field2D_ops.hpp"
#include "../../../utilities/wavefield.hpp"

#define Npx _rconf._num_pixels
#define VERBOSITY_LEVEL 0


template <typename TY>
class minibatch_update_psi {
public:
	parformat           _rconf;
	ptydata<TY>         _rdata;

	/**Compact constructor**/
	minibatch_update_psi(parformat settings): _rconf(settings) {};
	minibatch_update_psi(parformat settings, ptydata<TY> &data): _rconf(settings),_rdata(data) {
		init();
	};

	void setParameters(parformat settings, ptydata<TY> &data) {
		_rconf = settings;
		_rdata = data;
		init();
	};

	/**Compact destructor**/
	~minibatch_update_psi() {
		printf("Destroying MINIBATCH_UPDATE_PSI...\n");
		fflush(stdout);
		finish();
		printf("Done!\n");
		fflush(stdout);
	};

	void image_pipeline(int64_t);
	void diag_print_info();
	void diag_reset_error();

protected:
	int niter;
	trifield<complex<TY>>   OBxPR;
	trifield<complex<TY>>   BUFER;
	trifield<complex<TY>>   FIELD;
	trifield<TY>            SUMINT;
	trifield<TY>            RESIDUE;
	double *extBGROUNDI;
	double *extSCALEINT;

	void init();
	void finish();

	/**Fourier transform related members**/
	fftw_allprec<TY> *FFTplans;
	void normFFT(int thID) {
		const TY norm=1.0/(double)Npx;
		FIELD[0][thID]*=norm;
	};

	/**Forward pass pipeline functions*/
	void passFORWARD(int64_t,int64_t);
	void setView_OxP(int64_t,int64_t,int64_t,int64_t,int64_t,int64_t,int64_t);
	void Sum_Modes(int64_t thID);

	/**Magnitude update functions for ePIE and ML**/
	void Update_Magnitudes(int64_t,int64_t,int64_t);
	void Update_Magnitudes_int(int64_t,int64_t,int64_t);
	void Update_Magnitudes_like(int64_t,int64_t,int64_t);
	void Update_Magnitudes_basic(int64_t,int64_t,int64_t);
	void Update_Magnitudes_normal(int64_t,int64_t,int64_t);
	void Update_Magnitudes_fancy(int64_t,int64_t,int64_t);
	void Update_Likelihood_basic(int64_t,int64_t,int64_t);
	void Update_Likelihood_normal(int64_t,int64_t,int64_t);

	/**Backward pass pipeline functions**/
	void passBACKWARD(int64_t,int64_t);
	void Update_PSI(int64_t,int64_t,int64_t);
	void Update_PSI_EPIE(int64_t,int64_t,int64_t);
	void Update_PSI_LIKE(int64_t,int64_t,int64_t);

	/**Error and leftover calculation for diagnostics**/
	double _msqerr;
	double _lolerr;
	double **_errmap;
	void diagQSpaceError(int64_t,int64_t);
	void diag_error_msqerr(int64_t,int64_t);
	void diag_error_lolerr(int64_t,int64_t);
	void diag_error_errmap(int64_t,int64_t);

};/**minibatch_update_psi**/

/**Initializer outside of class declaration**/
template <typename TY>
void minibatch_update_psi<TY>::init() {

	_errmap = calloc2D<double>(_rconf._num_pro, _rconf._num_pixels*_rconf._num_pixels );
	OBxPR.init( _rconf._num_THREADS, _rconf._num_modes, _rconf._num_pixels, _rconf._num_pixels);
	BUFER.init( _rconf._num_THREADS, _rconf._num_modes, _rconf._num_pixels, _rconf._num_pixels);
	FIELD.init( 1, _rconf._num_THREADS, _rconf._num_pixels, _rconf._num_pixels);
	SUMINT.init(1, _rconf._num_THREADS, _rconf._num_pixels, _rconf._num_pixels);

	FFTplans = new fftw_allprec<TY>[_rconf._num_THREADS];
	for( int64_t tt=0; tt< _rconf._num_THREADS; tt++) {
		FFTplans[tt].init( FIELD[0][tt].dvec, FIELD[0][tt].npx_x );
	}
}



/**Destructor outside of class declaration**/
template <typename TY>
void minibatch_update_psi<TY>::finish() {
	OBxPR.free();
	BUFER.free();
	FIELD.free();
	SUMINT.free();
	free2D(_errmap, _rconf._num_pro );
}



/**This function runs for each view (scan point) separately.
    It performs the area selection, the projection to the
    reciprocal space, the update of magnitudes and finally
    returning to the sample plane. It also has various
    diagnostic functions included.*/
template <typename TY>
void minibatch_update_psi<TY>::image_pipeline(int64_t ff) {
	int64_t thID=omp_get_thread_num();
	/**Forward pass: from sample plane to reciprocal space**/
	passFORWARD(thID,ff);
	/**Update magnitudes with measured values**/
	Update_Magnitudes(thID,ff,_rdata._msklist[ff]);
	/**Diagnostics: assess the fit quality between measured and calculated patterns**/
	diagQSpaceError(thID,ff);
	/**Backward pass: from detector plane to sample plane**/
	passBACKWARD(thID,ff);
}



/**Propagate from the sample to the detector plane**/
template <typename TY>
void minibatch_update_psi<TY>::passFORWARD(int64_t thID, int64_t ff) {
	for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			setView_OxP(thID,_rdata._pos1D[ff],pp,oo,ff,_rdata._plist[ff],_rdata._olist[ff]);
			FFTplans[thID].execute_fw();
			normFFT(thID);
			BUFER[thID][pp+oo*_rconf._num_pmodes].imp(FIELD[0][thID]);
		}
	}
	Sum_Modes(thID);
}

/**Generate the illuminated region on the object and the actual view**/
template <typename TY>
void minibatch_update_psi<TY>::setView_OxP(int64_t thID, int64_t spos, int64_t pp, int64_t oo, int64_t ff, int64_t npr, int64_t nob) {
	OBxPR[thID][pp+oo*_rconf._num_pmodes].impsel( _rdata.OBJ[nob][oo], spos );
	OBxPR[thID][pp+oo*_rconf._num_pmodes]*=_rdata.PRO[npr][pp];
	FIELD[0][thID]=OBxPR[thID][pp+oo*_rconf._num_pmodes];
}

/**Sum up all object and probe modes. This generates the
     calculated amplitudes on the detector.**/
template <typename TY>
void minibatch_update_psi<TY>::Sum_Modes(int64_t thID) {
	SUMINT[0][thID].zero();

	for(int64_t oopp=0; oopp<_rconf._num_modes; oopp++) {
		for(int64_t xy=0; xy<Npx*Npx; xy++) {
			SUMINT[0][thID][xy]+=norm(BUFER[thID][oopp][xy]);
		}
	}
	sqrt( SUMINT[0][thID] );
}

/**Propagate back from the detector into the sample plane**/
template <typename TY>
void minibatch_update_psi<TY>::passBACKWARD(int64_t thID, int64_t ff) {
	for(int64_t oopp=0; oopp<_rconf._num_modes; oopp++) {

		FIELD[0][thID].imp(BUFER[thID][oopp]);
		FFTplans[thID].execute_bw();
		normFFT(thID);
		Update_PSI(thID,oopp,ff);
	}
}


template <typename TY>
void minibatch_update_psi<TY>::Update_PSI(int64_t thID, int64_t oopp, int64_t ff) {
	if( _rconf._mbatch_alg==0) {
		Update_PSI_EPIE(thID,oopp,ff);
	}
	if( _rconf._mbatch_alg==1) {
		Update_PSI_LIKE(thID,oopp,ff);
	}
}


/**Update the view gradient estimates**/
template <typename TY>
void minibatch_update_psi<TY>::Update_PSI_EPIE(int64_t thID, int64_t oopp, int64_t ff) {
	_rdata.PSI[oopp][ff]=FIELD[0][thID];
	_rdata.PSI[oopp][ff]-=OBxPR[thID][oopp];
}

/**Update the view gradient estimates**/
template <typename TY>
void minibatch_update_psi<TY>::Update_PSI_LIKE(int64_t thID, int64_t oopp, int64_t ff) {
	_rdata.PSI[oopp][ff]=FIELD[0][thID];
}


/**Do the actual magnitude updates with masking, range check, scale and background correction.
    The exact algorithm needs to be set externally to tune reconstruction time.**/
template <typename TY>
void minibatch_update_psi<TY>::Update_Magnitudes(int64_t thID, int64_t ff, int64_t midx) {
	/**Choosing the right algorithm**/
	if( _rconf._mbatch_alg==0) {
		Update_Magnitudes_int(thID,ff,midx);
	}
	if( _rconf._mbatch_alg==1) {
		Update_Magnitudes_like(thID,ff,midx);
	}
}


/**Do the actual magnitude updates with masking, range check, scale and background correction.
    The exact algorithm needs to be set externally to tune reconstruction time.**/
template <typename TY>
void minibatch_update_psi<TY>::Update_Magnitudes_like(int64_t thID, int64_t ff, int64_t midx) {
	/**Choosing the right algorithm**/
	if( _rconf._magupdate_lvl==0) {
		Update_Likelihood_basic(thID,ff,midx);
	}
	if( _rconf._magupdate_lvl==1) {
		Update_Likelihood_normal(thID,ff,midx);
	}
}

/**Calculate the likelihood gradients updates with masking.
    Speed is the primary objective, so keep in minimal.**/
template <typename TY>
void minibatch_update_psi<TY>::Update_Likelihood_basic(int64_t thID, int64_t ff, int64_t midx) {
	const TY eps = 1e-6;
	for(int64_t oopp=0; oopp<_rconf._num_modes; oopp++) {
		for(int64_t xy=0; xy<Npx*Npx; xy++) {
			if(_rdata.MSK[midx][xy]==1) {
				BUFER[thID][oopp][xy] = (TY)( norm(_rdata.IMG[ff][xy])/(norm(SUMINT[0][thID][xy])+eps) -1.0) * BUFER[thID][oopp][xy];
			} else {
				BUFER[thID][oopp][xy] = (TY)( 0.0 );
			}
		}
	}
}

/**Calculate the likelihood gradients updates with masking.
    Speed still matters, so no aggressive corrections.**/
template <typename TY>
void minibatch_update_psi<TY>::Update_Likelihood_normal(int64_t thID, int64_t ff, int64_t midx) {
	const TY eps = 1e-6;
	for(int64_t oopp=0; oopp<_rconf._num_modes; oopp++) {
		for(int64_t xy=0; xy<Npx*Npx; xy++) {
			if(_rdata.MSK[midx][xy]==1) {
				if(_rdata.IMG[ff][xy]<_rconf._cutoff) {
					BUFER[thID][oopp][xy] = (TY)( norm(_rdata.IMG[ff][xy])/(norm(SUMINT[0][thID][xy])+eps) -1.0 ) * BUFER[thID][oopp][xy];
				} else {
					BUFER[thID][oopp][xy] = (TY)( 0.0 );
				}
			}
		}
	}
}


/**Do the actual magnitude updates with masking, range check, scale and background correction.
    The exact algorithm needs to be set externally to tune reconstruction time.**/
template <typename TY>
void minibatch_update_psi<TY>::Update_Magnitudes_int(int64_t thID, int64_t ff, int64_t midx) {
	/**Choosing the right algorithm**/
	if( _rconf._magupdate_lvl==0) {
		Update_Magnitudes_basic(thID,ff,midx);
	}
	if( _rconf._magupdate_lvl==1) {
		Update_Magnitudes_normal(thID,ff,midx);
	}
	if( _rconf._magupdate_lvl==2) {
		Update_Magnitudes_fancy(thID,ff,midx);
	}
}

/**Do the actual magnitude updates with masking.
    Speed is the primary objective, so keep in minimal.**/
template <typename TY>
void minibatch_update_psi<TY>::Update_Magnitudes_basic(int64_t thID, int64_t ff, int64_t midx) {
	const TY eps = 1e-16;
	for(int64_t oopp=0; oopp<_rconf._num_modes; oopp++) {
		for(int64_t xy=0; xy<Npx*Npx; xy++) {
			if(_rdata.MSK[midx][xy]==1) {
				BUFER[thID][oopp][xy] = (TY)_rdata.IMG[ff][xy]*BUFER[thID][oopp][xy]/(SUMINT[0][thID][xy]+eps) ;
			}
		}
	}
}

/**Do the actual magnitude updates with masking and valid range check
    Speed still matters, so no complex, all-inclusive statements.**/
template <typename TY>
void minibatch_update_psi<TY>::Update_Magnitudes_normal(int64_t thID, int64_t ff, int64_t midx) {
	const TY eps = 1e-16;
	for(int64_t oopp=0; oopp<_rconf._num_modes; oopp++) {
		for(int64_t xy=0; xy<Npx*Npx; xy++) {
			if(_rdata.MSK[midx][xy]==1) {
				if(_rdata.IMG[ff][xy]<_rconf._cutoff) {
					BUFER[thID][oopp][xy]=(TY)_rdata.IMG[ff][xy]*BUFER[thID][oopp][xy]/(SUMINT[0][thID][xy]+eps);
				}
			}
		}
	}
}

/**Do the actual magnitude updates with masking, range check, scale and background correction.
    Speed does not matter at all so use a lot of refinements.**/
template <typename TY>
void minibatch_update_psi<TY>::Update_Magnitudes_fancy(int64_t thID, int64_t ff, int64_t midx) {
	TY Imeas, Ameas;
	const TY eps = 1e-16;
	for(int64_t oopp=0; oopp<_rconf._num_modes; oopp++) {
		for(int64_t xy=0; xy<Npx*Npx; xy++) {
			if(_rdata.MSK[midx][xy]==1) {
				if(_rdata.IMG[ff][xy]<_rconf._cutoff) {
					Imeas=extSCALEINT[ff]*(norm( _rdata.IMG[ff][xy])-extBGROUNDI[xy] );
					if( Imeas<TY(0) ) {
						Imeas=TY(0);
					}
					Ameas=sqrt(Imeas);
					BUFER[thID][oopp][xy]=Ameas*BUFER[thID][oopp][xy]/(SUMINT[0][thID][xy]+eps);
				}
			}
		}
	}
}

/**Calculates the model's mismatch from the data.**/
template <typename TY>
void minibatch_update_psi<TY>::diagQSpaceError(int64_t thID, int64_t ff) {
	if(_rconf._do_calc_error) {
		diag_error_msqerr(thID,ff);
		diag_error_lolerr(thID,ff);
	}
	if(_rconf._do_calc_lover) {
		diag_error_errmap(thID,ff);
	}
}

/**Calculates the mismatch between the measured
*   and calculated data	(mse error metric).**/
template <typename TY>
void minibatch_update_psi<TY>::diag_error_msqerr(int64_t thID, int64_t ff) {
	double frerror=0.0;
	for(int64_t xy=0; xy<Npx*Npx; xy++) {
		if(_rdata.MSK[_rdata._msklist[ff]][xy]==1) {
//            frerror+=norm((norm(_rdata.IMG[ff][xy])-extBGROUNDI[xy])-norm(SUMINT[thID].data[xy]));
			frerror+=norm((norm(_rdata.IMG[ff][xy])-norm(SUMINT[0][thID][xy])));
		}
	}
	#pragma omp atomic
	_msqerr+=frerror;
}


/**Calculates the mismatch between the measured
*   and calculated data	(loli error metric).**/
template <typename TY>
void minibatch_update_psi<TY>::diag_error_lolerr(int64_t thID, int64_t ff) {
	double frerror=0.0;
	for(int64_t xy=0; xy<Npx*Npx; xy++) {
		if(_rdata.MSK[_rdata._msklist[ff]][xy]==1) {
			frerror += norm(_rdata.IMG[ff][xy])*log( norm(SUMINT[0][thID][xy])+1e-16 ) - norm(SUMINT[0][thID][xy]) ;
		}
	}
	#pragma omp atomic
	_lolerr+=frerror;
}


/**Saves the pattern mismatch between the
*   measured and calculated intensities.**/
template <typename TY>
void minibatch_update_psi<TY>::diag_error_errmap(int64_t thID, int64_t ff) {
	double tmp;
//	int64_t npr=_rdata._plist[ff];

	for(int64_t xy=0; xy<Npx*Npx; xy++) {
		if(_rdata.MSK[_rdata._msklist[ff]][xy]==1) {
			if(_rdata.IMG[ff][xy]<_rconf._cutoff) {
//                tmp=((norm(_Wdata.IMG[ff][xy])-extBGROUNDI[xy])-norm(SUMINT[thID].data[xy]));
				tmp=norm(norm(_rdata.IMG[ff][xy])-norm(SUMINT[0][thID][xy]));
				#pragma omp atomic
				_errmap[0][xy]+=tmp;
//				_errmap[npr][xy]+=tmp;
			}
		}
	}
}

template <typename TY>
void minibatch_update_psi<TY>::diag_print_info() {
	if(_rconf._do_calc_error) {
		printf("%sMSE loss: %0.4g, LOLI loss %0.4g%s\n", A_C_CYAN,_msqerr,_lolerr,A_C_RESET);
	}
	if(_rconf._do_calc_lover) {
		char filename[512];
		sprintf(filename,"./output/LeftOver_P%d_lin.tif",0);
		WriteImOut(_errmap[0], Npx, Npx, "lin","int","TIFF",filename);
		sprintf(filename,"./output/LeftOver_P%d_log.tif",0);
		WriteImOut(_errmap[0], Npx, Npx, "log","int","TIFF",filename);
	}

}

/**Resets the error counters when starting a new iteration**/
template <typename TY>
void minibatch_update_psi<TY>::diag_reset_error() {
	_msqerr=0.0;
	_lolerr=0.0;
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		memset(_errmap[npr],0,Npx*Npx*sizeof(double));
	}
}




#endif




