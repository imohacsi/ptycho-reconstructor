#ifndef PTYREC_ENGINE_MINIBATCH_TEMPLATE_H_
#define PTYREC_ENGINE_MINIBATCH_TEMPLATE_H_

#include <cstdlib>
#include <algorithm>
#include <random>
#include <vector>
#include <cstring>
#include <fftw3.h>
#include <omp.h>
#include <time.h>

#include "../parallelconfig.hpp"
#include "./minibatch_update_psi.hpp"
#include "./minibatch_update_op.hpp"

//#include "../../imageprocessing/imageprocessing.h"
//#include "../../utilities/fft_utils.hpp"
//#include "../../utilities/wavefield.hpp"
//#include "../../utilities/memutils.h"

#define Npx _rconf._num_pixels
#define VERBOSITY_LEVEL 0

template <typename TY>
class minibatch_engine_template {
public:
	void run_reconstruction();
	ptydata<TY> getDataRelaxed() {
		return _anndata;
	}
	ptydata<TY> getDataCurrent() {
		return _rdata;
	}

	minibatch_engine_template(parformat iconf, ptydata<TY> rdata): _rconf(iconf), _rdata(rdata),view_update_loop(_rconf),opro_update_loop(_rconf) {
		init();
		frameidx.resize(_rconf._num_frames);
		std::iota( std::begin(frameidx),std::end(frameidx),0);
		std::shuffle(frameidx.begin(),frameidx.end(),  std::mt19937{std::random_device{}()} );
		initMBatchData();
		rsetMBatchData();

		view_update_loop.setParameters(_rconf,_mbdata);
		opro_update_loop.setParameters(_rconf,_mbdata);
	};


	/**Keep the API minimal**/
protected:
	parformat           _rconf;
	ptydata<TY>         _rdata;
	ptydata<TY>         _mbdata;
	ptydata<TY>         _anndata;

	std::vector<int64_t> frameidx;
	minibatch_update_psi<TY> view_update_loop;
	minibatch_update_op<TY> opro_update_loop;
//    batch_aux_update<TY> auxiliary_update_loop;
	void init();
	int64_t bstart;
	void initMBatchData();
	void rsetMBatchData();

	/**Make a single iteration**/
	int64_t niter;
	void make_iteration();

	/**Simulated annealing**/
	int64_t num_relaxed;
	void relaxAddCurrent();
	void relaxNormalize();

	/**Error diagnostics**/
	double *_prmodeint;
	void diag_print_info();
	void diagRSpace(int64_t,int64_t);
	void diag_prmode_int();

	/**Real time plotting*/
	void print_current_iteration();
};

/**Initializer outside of class declaration**/
template <typename TY>
void minibatch_engine_template<TY>::init() {
	num_relaxed = 0;
	if( _rconf._batch_size>_rconf._num_frames) {
		_rconf._batch_size=_rconf._num_frames;
	}

	_anndata.OBJ.init(_rconf._num_obj, _rconf._num_omodes, _rdata.OBJ[0][0].npx_x, _rdata.OBJ[0][0].npx_y);
	_anndata.PRO.init(_rconf._num_pro, _rconf._num_pmodes, _rdata.PRO[0][0].npx_x, _rdata.PRO[0][0].npx_y);

}

template <typename TY>
void minibatch_engine_template<TY>::initMBatchData() {
	bstart=0;
	_mbdata.OBJ = _rdata.OBJ;
	_mbdata.PRO = _rdata.PRO;
	_mbdata.PSI = _rdata.PSI;
	_mbdata.MSK = _rdata.MSK;
	_mbdata.BGROUND = _rdata.BGROUND;


	_mbdata.IMG = (float**)calloc(_rconf._num_frames,sizeof(float*));
	_mbdata._plist = (int*)calloc(_rconf._num_frames,sizeof(int));
	_mbdata._olist = (int*)calloc(_rconf._num_frames,sizeof(int));
	_mbdata._msklist = (int*)calloc(_rconf._num_frames,sizeof(int));
	_mbdata._pos1D = (int64_t*)calloc(_rconf._num_frames,sizeof(int64_t));
	_mbdata._pos2D = (double**)calloc(_rconf._num_frames,sizeof(double));
}

template <typename TY>
void minibatch_engine_template<TY>::rsetMBatchData() {
	if( (bstart+_rconf._batch_size)> _rconf._num_frames ) {
		printf("Reshufling...\n");
		std::shuffle( frameidx.begin(), frameidx.end(), std::mt19937{std::random_device{}()} );
		bstart=0;
	}

	for( int64_t ff=bstart; ff<bstart+_rconf._batch_size; ff++) {
		_mbdata.IMG[ff-bstart] = _rdata.IMG[frameidx[ff]];
		_mbdata._plist[ff-bstart] = _rdata._plist[frameidx[ff]];
		_mbdata._olist[ff-bstart] = _rdata._olist[frameidx[ff]];
		_mbdata._msklist[ff-bstart] = _rdata._msklist[frameidx[ff]];
		_mbdata._pos1D[ff-bstart] = _rdata._pos1D[frameidx[ff]];
//        _mbdata._pos2D[ff-bstart] = _rdata._pos2D[frameidx[ff]];
	}
	bstart+=_rconf._batch_size;
}



/**Single command to run the reconstruction**/
template <typename TY>
void minibatch_engine_template<TY>::run_reconstruction() {

	num_relaxed=0;
	for(niter=0; niter<_rconf._num_curr_iter; niter++) {
		make_iteration();

		/**Add to simulated annealing over last iterations for noise reduction*/
		if(niter>(_rconf._num_curr_iter*3)/4) {
			relaxAddCurrent();
		}

		if(niter%_rconf._WriteEveryNthIter==0) {
            if(_rconf._do_calc_print && _rconf._num_pmodes>1) { diag_prmode_int(); }
			view_update_loop.diag_print_info();
			print_current_iteration();
		}
	}

	relaxNormalize();
}



template <typename TY>
void minibatch_engine_template<TY>::make_iteration() {

	/**Re-set iteration parameters**/
	rsetMBatchData();
	view_update_loop.diag_reset_error();
	clock_t Tstart,Tend;
	Tstart=clock();

	if(VERBOSITY_LEVEL) {
		printf("Starting image pipeline!\n");
		fflush(stdout);
	}
	/**Update views**/
	#pragma omp parallel for
	for(int64_t ff=0; ff<_rconf._batch_size; ff++) {
		view_update_loop.image_pipeline(ff);
	}

	/**Update the object and probe estimates**/
	opro_update_loop.UpdateObjectProbe(niter);

	/**Printing benchmark and status*/
	Tend=clock();
	double Titer=(double)(Tend-Tstart)/CLOCKS_PER_SEC;
	printf("%sFinished %d th of %d iter. (%g s)%s\n",A_C_GREEN, (int)niter,(int)_rconf._num_curr_iter,Titer, A_C_RESET);
	fflush(stdout);

}

/**Diagnostics to compare probe mode intensities**/
template <typename TY>
void minibatch_engine_template<TY>::diag_prmode_int() {
	double MODEINT[_rconf._num_pro][_rconf._num_pmodes];
	double TOTINT[_rconf._num_pro];

	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			MODEINT[npr][pp]= sum( _rdata.PRO[npr][pp] );
			TOTINT[npr]+=MODEINT[npr][pp];
		}
	}

	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		printf("\t%sIntensity in the %dth probe's modes: ",A_C_YELLOW, (int)npr);
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			printf( "%0.3g",MODEINT[npr][pp]/TOTINT[npr]);
		}
		printf("%s\n",A_C_RESET);
		fflush(stdout);
	}
}

/**Add current object and probe to relaxation**/
template <typename TY>
void minibatch_engine_template<TY>::relaxAddCurrent() {
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			_anndata.PRO[npr][pp]+=_rdata.PRO[npr][pp];
		}
	}

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			_anndata.OBJ[nob][oo]+=_rdata.OBJ[nob][oo];
		}
	}

	num_relaxed++;
}

/**Normalize relaxation**/
template <typename TY>
void minibatch_engine_template<TY>::relaxNormalize() {
	TY norm = 1.0/(double)num_relaxed;

	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			_anndata.PRO[npr][pp]*=norm;
		}
	}

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			_anndata.OBJ[nob][oo]*=norm;
		}
	}
}

/**Write out current progress as bitmap files**/
template <typename TY>
void minibatch_engine_template<TY>::print_current_iteration() {
	char filename[512];
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			sprintf(filename,"./output/Citer_Probe_P%d_M%d_I.bmp",(int)npr,(int)pp);
			ImWrite(_rdata.PRO[npr][pp],"lin","int","BMP",filename);
			sprintf(filename,"./output/Citer_Probe_P%d_M%d_P.bmp",(int)npr,(int)pp);
			ImWrite(_rdata.PRO[npr][pp],"lin","arg","BMP",filename);
		}
	}

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			sprintf(filename,"./output/Citer_Object_O%d_M%d_I.bmp",(int)nob,(int)oo);
			ImWrite(_rdata.OBJ[nob][oo],"lin","int","BMP",filename);
			sprintf(filename,"./output/Citer_Object_O%d_M%d_P.bmp",(int)nob,(int)oo);
			ImWrite(_rdata.OBJ[nob][oo],"lin","arg","BMP",filename);
		}
	}
}

#endif


