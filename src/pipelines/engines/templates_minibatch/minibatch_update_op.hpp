#ifndef PTYREC_ENGINE_MINIBATCH_UPDATE_OP_H_
#define PTYREC_ENGINE_MINIBATCH_UPDATE_OP_H_

#include <cstring>
#include <omp.h>
#include <time.h>

#include "../parallelconfig.hpp"
#include "./optimizers/optimizer_wrapper.hpp"


#define Npx _rconf._num_pixels
#define xyobj (xx+yy*_mbdata.OBJ[_mbdata._olist[ff]][oo].npx_x+_mbdata._pos1D[ff])
#define xypro (xx+yy*_rconf._num_pixels)
#define VERBOSITY_LEVEL 0

template <typename TY>
class minibatch_update_op {
public:
	parformat           _rconf;
	ptydata<TY>         _mbdata;

	/**Compact constructor and destructor**/
	minibatch_update_op(parformat settings, ptydata<TY> &data): _rconf(settings),_mbdata(data) {
		init();
	};

	minibatch_update_op(parformat settings): _rconf(settings),_optimizer(settings._optim_alg,_rconf) { };

	void setParameters(parformat settings, ptydata<TY> &data) {
		_rconf=settings;
		_mbdata=data;
		_optimizer.setParameters(_rconf,_mbdata);
		init();
	};

	~minibatch_update_op() {
		finish();
	}

	void UpdateObjectProbe(int);

protected:

	void init();
	void finish();

	/**Optimizer and gradients**/
	optimizer_wrapper<TY> _optimizer;
	trifield<complex<TY>>   gradOBJ;
	trifield<TY>            normOBJ;
	trifield<complex<TY>>   gradPRO;
	trifield<TY>            normPRO;

	/**Object and probe update functions**/
	void Upd_opGRAD();
	double _mag_l2reg_objgradients;
	void L2reg_ObjectGradients();
	void L2reg_ProbeGradients();

	/**Additional support constraints**/
	void supProbeRSupport();
	void supProbeQSupport();

};

///**Initializer outside of class declaration**/
template <typename TY>
void minibatch_update_op<TY>::init() {
	_mag_l2reg_objgradients = _rconf._l2reg_objgrad;

	gradOBJ.init( _rconf._num_obj, _rconf._num_omodes, _mbdata.OBJ[0][0].npx_x,_mbdata.OBJ[0][0].npx_y );
	normOBJ.init( _rconf._num_obj, _rconf._num_omodes, _mbdata.OBJ[0][0].npx_x,_mbdata.OBJ[0][0].npx_y );
	gradPRO.init( _rconf._num_pro, _rconf._num_pmodes, _mbdata.PRO[0][0].npx_x,_mbdata.PRO[0][0].npx_y );
	normPRO.init( _rconf._num_pro, _rconf._num_pmodes, _mbdata.PRO[0][0].npx_x,_mbdata.PRO[0][0].npx_y );
}


/**Destructor outside of class declaration**/
template <typename TY>
void minibatch_update_op<TY>::finish() {
	gradOBJ.free();
	normOBJ.free();
	gradPRO.free();
	normPRO.free();
}



/**Calculate new object and probe gradients **/
template <typename TY>
void minibatch_update_op<TY>::Upd_opGRAD() {
	if(VERBOSITY_LEVEL) {
		printf("Updating gradients from view...\n");
		fflush(stdout);
	}

	int64_t nob,npr,oo,pp,ff,xx,yy;
	TY eps = 1e-12;
	TY pmax =0.0;
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			for(int64_t xy=0; xy<_mbdata.PRO[npr][pp].n_pix; xy++) {
				pmax = max( pmax, norm(_mbdata.PRO[npr][pp][xy]) );
			}
		}
	}

	for(nob=0; nob<_rconf._num_obj; nob++) {
		for(oo=0; oo<_rconf._num_omodes; oo++) {
			normOBJ[nob][oo]=(eps);
		}
	}

	/**Do the actual update process*/
	for(oo=0; oo<_rconf._num_omodes; oo++) {
		for(pp=0; pp<_rconf._num_pmodes; pp++) {
			for(ff=0; ff<_rconf._batch_size; ff++) {
				nob = _mbdata._olist[ff];
				npr = _mbdata._plist[ff];
				#pragma omp parallel for private(xx)
				for(yy=0; yy<Npx; yy++) {
					for(xx=0; xx<Npx; xx++) {
						gradPRO[npr][pp][xx+yy*Npx] += conj(_mbdata.OBJ[nob][oo][xyobj])*_mbdata.PSI[pp+oo*_rconf._num_pmodes][ff][xx+yy*Npx];
						gradOBJ[nob][oo][xyobj] += conj(_mbdata.PRO[npr][pp][xx+yy*Npx])*_mbdata.PSI[pp+oo*_rconf._num_pmodes][ff][xx+yy*Npx];
						normOBJ[nob][oo][xyobj] += pmax;
					}}
        }}}

	for(nob=0; nob<_rconf._num_obj; nob++) {
		for(oo=0; oo<_rconf._num_omodes; oo++) {
			gradOBJ[nob][oo].par_div(normOBJ[nob][oo]);
		}}

	for(npr=0; npr<_rconf._num_pro; npr++) {
		for(pp=0; pp<_rconf._num_pmodes; pp++) {
			gradPRO[npr][pp].par_mul( 1.0/(_rconf._batch_size) );
		}}

}



/**Calculate object gradient regularizer to smoothen the object**/
template <typename TY>
void minibatch_update_op<TY>::L2reg_ObjectGradients() {
	if(VERBOSITY_LEVEL) {
		printf("Regularizing with object gradients...\n");
		fflush(stdout);
	}

	int64_t OX,OY;
	complex<TY> laplO;
	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			OX = _mbdata.OBJ[nob][oo].npx_x;
			OY = _mbdata.OBJ[nob][oo].npx_y;
			for(int64_t yy=1; yy<OY-1; yy++) {
				for(int64_t xx=1; xx<OX-1; xx++) {
					laplO = 4.0*_mbdata.OBJ[nob][oo][xx+yy*OX]-_mbdata.OBJ[nob][oo][xx+yy*OX-1]-_mbdata.OBJ[nob][oo][xx+yy*OX+1]
					        -_mbdata.OBJ[nob][oo][xx+yy*OX-OX]-_mbdata.OBJ[nob][oo][xx+yy*OX+OX];
					gradOBJ[nob][oo][xx+yy*OX] -= _mag_l2reg_objgradients*(laplO);
				}
			}
		}
	}
}

/**Calculate object gradient regularizer to smoothen the object**/
template <typename TY>
void minibatch_update_op<TY>::L2reg_ProbeGradients() {
	if(VERBOSITY_LEVEL) {
		printf("Regularizing with object gradients...\n");
		fflush(stdout);
	}

	TY divP;
	complex<TY> laplP;
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			for(int64_t yy=1; yy<Npx-1; yy++) {
				for(int64_t xx=1; xx<Npx-1; xx++) {
					laplP = 4.0*_mbdata.PRO[npr][pp][xx+yy*Npx]-_mbdata.PRO[npr][pp][xx+yy*Npx-1]-_mbdata.PRO[npr][pp][xx+yy*Npx+1]
					        -_mbdata.PRO[npr][pp][xx+yy*Npx-Npx]-_mbdata.PRO[npr][pp][xx+yy*Npx+Npx];
					gradPRO[npr][pp][xx+yy*Npx] -= _mag_l2reg_objgradients*(laplP/(abs(_mbdata.PRO[npr][pp][xx+yy*Npx])+1e-8));
				}
			}
		}
	}
}


/**Do one update cycle**/
template <typename TY>
void minibatch_update_op<TY>::UpdateObjectProbe(int niter) {
	if(VERBOSITY_LEVEL) {
		printf("Starting minibatch updates..\n");
		fflush(stdout);
	}

	/**Update the gradients...**/
	Upd_opGRAD();
	L2reg_ObjectGradients();
//    L2reg_ProbeGradients();
//
//WriteImOut(gradOBJ[0][0].dvec, gradOBJ[0][0].npx_x,gradOBJ[0][0].npx_y, "lin","int", "TIFF","./output/last_gradobj.tif");
//WriteImOut(gradPRO[0][0].dvec, Npx,Npx, "lin","int", "TIFF","./output/last_gradpro.tif");

    if( niter>=_rconf._start_objupd){
        _optimizer.UpdateOBJ(gradOBJ);}

    if( niter>=_rconf._start_proupd){
        _optimizer.UpdatePRO(gradPRO); }

//WriteImOut(_mbdata.OBJ[0][0].dvec, gradOBJ[0][0].npx_x,gradOBJ[0][0].npx_y, "lin","arg", "TIFF","./output/last_updobj.tif");
//WriteImOut(_mbdata.PRO[0][0].dvec, Npx,Npx, "lin","int", "TIFF","./output/last_updpro.tif");

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			par_cap( _mbdata.OBJ[nob][oo] );
		}
	}
	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			gradOBJ[nob][oo]=1e-16;
		}
	}
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			gradPRO[npr][pp]=1e-16;
		}
	}
}


#undef Npx
#undef xyobj
#undef xypro
#undef VERBOSITY_LEVEL
#endif

