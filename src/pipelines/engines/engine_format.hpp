#ifndef PTYREC_ENGINE_FORMAT_H_
#define PTYREC_ENGINE_FORMAT_H_

#include "../../reconstruction/worker_config.hpp"
#include "../../imageprocessing/imageprocessing.h"
#include <cstring>
#include <fftw3.h>
#include <omp.h>
#include "../../utilities/fft_utils.hpp"
#include "../../reconstruction/worker_config.hpp"
#include "../../utilities/wavefield.hpp"


class engine_common {
public:

	/**Object/Probe update parameters**/
	int     _opupd_niter=10;
	int     _start_proupd=2;
	int     _start_objupd=0;
	double  _opupd_regpro=0.1;
	double  _lrate_obj=0.5;
	double  _lrate_pro=0.5;
	/**Magnitude update parameters**/
	double  _magupdate_reg;
	int     _magupdate_lvl;
	int     _viewsel_lvl;
	bool    _do_calc_error;
	bool    _do_calc_lover;
	int     _WriteEveryNthIter;

	/**Object support parameters**/
	bool    _do_weak_obj;

	/**Probe support parameters**/
	bool    _do_prsup_r;
	double  _prsup_r;
	bool    _do_prsup_q;
	double  _prsup_q;

	/**Minibatch update parameters**/
	double  _l2reg_objgrad=0.00001;
	double  _mbatch_alg=0;
	double _optim_decay=500.0;
	std::string _optim_alg="MOMENTUM";



	int _bgref_q;
	std::string _bgref_alg;
	int _bgref_start;
	int _bgref_every;
	int _bgref_stop;
	bool _do_bgref_q;
	bool _do_patt_scale;
	bool _do_calc_print;
};


class auxil_common {
public:
	int _do_posref;
	int _posref_nstep;
	int _posref_niter;
	int _posref_maxjump;
	double _lrate_posref;

};


class engine_format {
public:
	std::string         algorithm_rec;
	std::string         algorithm_ref;
	std::string         algorithm_curr;
	std::string         precision;
	engine_common       engcom;
	auxil_common        auxcom;
};

#endif
