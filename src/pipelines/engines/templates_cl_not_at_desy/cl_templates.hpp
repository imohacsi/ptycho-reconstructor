#ifndef PTYREC_ENGINE_OCL_ENGINE_TEMPLATE_HPP_
#define PTYREC_ENGINE_OCL_ENGINE_TEMPLATE_HPP_

#include <cstdlib>
#include <algorithm>
#include <random>
#include <vector>
#include <cstring>
#include <fftw3.h>
#include <omp.h>
#include <time.h>

#include "../parallelconfig.hpp"
#include "../templates_minibatch/minibatch_update_psi.hpp"
#include "../templates_minibatch/minibatch_update_op.hpp"
#include "./cl_update_psi.hpp"

#define Npx _rconf._num_pixels

class cl_engine_template {
public:
	void run_reconstruction();
	ptydata<double> getDataRelaxed(){ return _anndata; };
	ptydata<double> getDataCurrent(){ return _rdata;   };

	cl_engine_template(parformat iconf, ptydata<double> rdata): _rconf(iconf), _rdata(rdata),view_update_loop(_rconf) {
		init();
		initMBatchData();
		setMBatchData();

		view_update_loop.setParameters(_rconf,_mbdata);
	};


	/**Keep the API minimal**/
protected:
	parformat              _rconf;
	ptydata<double>         _rdata;
	ptydata<double>         _mbdata;
	ptydata<double>         _anndata;

	cl_update_psi         view_update_loop;

	void init();

	std::vector<int64_t> _ring_idx;
	int64_t _ring_start=0;

	void initMBatchData();
	void setMBatchData();

	/**Make a single iteration**/
	int64_t niter;
	void make_iteration();

	/**Simulated annealing**/
	int64_t num_relaxed=0;
	void relaxAddCurrent();
	void relaxNormalize();

	/**Error diagnostics**/
	void diag_print_info();
	void diag_prmode_int();

	/**Real time plotting*/
	void print_current_iteration();
};

#endif // PTYREC_ENGINE_OCL_ENGINE_TEMPLATE_HPP_



