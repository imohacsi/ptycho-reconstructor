#include "./cl_update_psi.hpp"
#define Npx _rconf._num_pixels



void cl_update_psi::init(){
    _mag_l2reg_objgradients = _rconf._l2reg_objgrad;

    _mbdata.PSI.init( _rconf._batch_size, _rconf._num_modes, _rconf._num_pixels, _rconf._num_pixels );
    _mbdata.OBJ.init( _rconf._num_obj, _rconf._num_omodes, _rconf._OX, _rconf._OY );
    _mbdata.PRO.init( _rconf._num_pro, _rconf._num_pmodes, _rconf._num_pixels, _rconf._num_pixels );

	gradOBJ.init( _rconf._num_obj, _rconf._num_omodes, _mbdata.OBJ[0][0].npx_x,_mbdata.OBJ[0][0].npx_y );
	gradPRO.init( _rconf._num_pro, _rconf._num_pmodes, _mbdata.PRO[0][0].npx_x,_mbdata.PRO[0][0].npx_y );

//    OBxPR.init( _rconf._batch_size, _rconf._num_modes, _rconf._num_pixels, _rconf._num_pixels);
//    FIELD.init( _rconf._batch_size, _rconf._num_modes, _rconf._num_pixels, _rconf._num_pixels);
    /**Allocating device memory and copy static members**/
    _clenv.dev_alloc( _rconf._batch_size, _rconf._num_omodes, _rconf._num_pmodes, _rconf._num_pixels, _rconf._OX, _rconf._OY, _rconf._num_msk, _rconf._num_obj, _rconf._num_pro );
    for( int mm=0; mm<1; mm++){
        clEnqueueWriteBuffer(_clenv.commands,_clenv._dev_MSK[mm],CL_TRUE,0,sizeof(int)*Npx*Npx,_rdata.MSK[mm],0,NULL,NULL);
    }
}

/**Destructor outside of class declaration**/
void cl_update_psi::finish() {
	gradOBJ.free();
	gradPRO.free();
}

void cl_update_psi::export_batch(){
    for( int nob=0; nob<_rconf._num_obj; nob++){
        for( int oo=0; oo<_rconf._num_omodes; oo++){
            for( int xy=0; xy<_rdata.OBJ._n_pix; xy++){
                _rdata.OBJ[nob][oo][xy] = _mbdata.OBJ[nob][oo][xy]; }}}
    for( int npr=0; npr<_rconf._num_pro; npr++){
        for( int pp=0; pp<_rconf._num_pmodes; pp++){
            for( int xy=0; xy<_rdata.PRO._n_pix; xy++){
                _rdata.PRO[npr][pp][xy] = _mbdata.PRO[npr][pp][xy]; }}}
}

void cl_update_psi::fill_batch(){

    for( int nob=0; nob<_rconf._num_obj; nob++){
        for( int oo=0; oo<_rconf._num_omodes; oo++){
            for( int xy=0; xy<_rdata.OBJ._n_pix; xy++){
                _mbdata.OBJ[nob][oo][xy] = _rdata.OBJ[nob][oo][xy]; }}}
    for( int npr=0; npr<_rconf._num_pro; npr++){
        for( int pp=0; pp<_rconf._num_pmodes; pp++){
            for( int xy=0; xy<_rdata.PRO._n_pix; xy++){
                _mbdata.PRO[npr][pp][xy] = _rdata.PRO[npr][pp][xy]; }}}

    for( int bb=0; bb<_rconf._batch_size; bb++){
        clEnqueueWriteBuffer(_clenv.commands,_clenv._dev_IMG[bb],CL_TRUE,0,sizeof(float)*_rconf._num_pixels*_rconf._num_pixels,_rdata.IMG[bb],0,NULL,NULL);
     }

//    printf("\tWriting probe!\n");fflush(stdout);
    /**Probe needs to be refreshed after every update**/
    for( int npr=0; npr<_rconf._num_pro; npr++){
    for( int pp=0; pp<_rconf._num_pmodes; pp++){
        clEnqueueWriteBuffer(_clenv.commands,_clenv._dev_PRO[npr][pp],CL_TRUE,0,sizeof(float)*2*_rconf._num_pixels*_rconf._num_pixels,_mbdata.PRO[npr][pp].dvec,0,NULL,NULL);
    }}

//    printf("\tWriting obj!\n");fflush(stdout);
    /**Probe needs to be refreshed after every update**/
    for( int nob=0; nob<_rconf._num_obj; nob++){
    for( int oo=0; oo<_rconf._num_omodes; oo++){
        clEnqueueWriteBuffer(_clenv.commands,_clenv._dev_OBJ[nob][oo],CL_TRUE,0,sizeof(float)*2*_rconf._OX*_rconf._OY,_mbdata.OBJ[nob][oo].dvec,0,NULL,NULL);
    }}
    clFinish(_clenv.commands);
}

//
//void cl_update_psi::read_fields(){
//    /**Filling the wavefields with the new selection**/
//    for( int bb=0; bb<_rconf._batch_size; bb++){
//        for( int op=0; op<_rconf._num_modes; op++){
//            clEnqueueReadBuffer(_clenv.commands,_clenv._dev_FIELD[bb][op],CL_TRUE,0,sizeof(float)*2*_rconf._num_pixels*_rconf._num_pixels,FIELD[bb][op].dvec,0,NULL,NULL);
//        }
//    }
//}

void cl_update_psi::read_gradients(){
    /**Filling the wavefields with the new selection**/
    for( int nob=0; nob<_rconf._num_obj; nob++){
    for( int oo=0; oo<_rconf._num_omodes; oo++){
        clEnqueueReadBuffer(_clenv.commands,_clenv._dev_grOBJ[nob][oo],CL_TRUE,0,sizeof(float)*2*_rconf._OX*_rconf._OY,gradOBJ[nob][oo].dvec,0,NULL,NULL);
    }}
    for( int npr=0; npr<_rconf._num_pro; npr++){
    for( int pp=0; pp<_rconf._num_pmodes; pp++){
        clEnqueueReadBuffer(_clenv.commands,_clenv._dev_grPRO[npr][pp],CL_TRUE,0,sizeof(float)*2*Npx*Npx,gradPRO[npr][pp].dvec,0,NULL,NULL);
    }}
}

void cl_update_psi::image_pipeline(){
    size_t map_size = _rconf._num_pixels*_rconf._num_pixels;
//    printf("Start GPU gradient update...\n");fflush(stdout);

    /**Filling batch with new data**/
    fill_batch();

    /**Multiply with probe**/
    for( int bb=0; bb<_rconf._batch_size; bb++){
        for( int oo=0; oo<_rconf._num_omodes; oo++){
            for( int pp=0; pp<_rconf._num_pmodes; pp++){
                int oopp = pp+oo*_rconf._num_pmodes;
            _clenv.k_calc_view(_clenv._dev_OBxPR[bb][oopp], _clenv._dev_FIELD[bb][oopp], _clenv._dev_OBJ[_rdata._olist[bb]][oo], _clenv._dev_PRO[_rdata._plist[bb]][pp], _rdata._pos1D[bb], _rconf._OX, _rconf._num_pixels, Npx  );
        }}}
    clFinish(_clenv.commands);

    /**Forward FFT**/
    for( int bb=0; bb<_rconf._batch_size; bb++){
        for( int op=0; op<_rconf._num_modes; op++){
            clfftEnqueueTransform( _clenv._fftPlanHandle, CLFFT_FORWARD, 1, &_clenv.commands,0,NULL,NULL,&_clenv._dev_FIELD[bb][op], NULL,NULL);
        }}
    clFinish(_clenv.commands);

    /**Sum intensity from all modes**/
    for( int bb=0; bb<_rconf._batch_size; bb++){
        _clenv.k_const_fill( _clenv._dev_SUMINT[bb], 0.0, map_size);   }
    clFinish(_clenv.commands);
    for( int op=0; op<_rconf._num_modes; op++){
        for( int bb=0; bb<_rconf._batch_size; bb++){
            _clenv.k_add_mode_intensity(_clenv._dev_SUMINT[bb], _clenv._dev_FIELD[bb][op], map_size );
        }
        clFinish(_clenv.commands);
    }

    if( _rconf._mbatch_alg==0 ){
        /**Magnitude update**/
        for( int bb=0; bb<_rconf._batch_size; bb++){
            for( int op=0; op<_rconf._num_modes; op++){
                _clenv.k_magnitude_update(_clenv._dev_FIELD[bb][op], _clenv._dev_IMG[bb], _clenv._dev_MSK[_rdata._msklist[bb]], _clenv._dev_SUMINT[bb], (float)(Npx), map_size );
            }}
         clFinish(_clenv.commands);
    }else if( _rconf._mbatch_alg==1 ){
        /**Likelihood update**/
        for( int bb=0; bb<_rconf._batch_size; bb++){
            for( int op=0; op<_rconf._num_modes; op++){
                _clenv.k_likelihood_update(_clenv._dev_FIELD[bb][op], _clenv._dev_IMG[bb], _clenv._dev_MSK[_rdata._msklist[bb]], _clenv._dev_SUMINT[bb], (float)(Npx), map_size );
            }}
         clFinish(_clenv.commands);
    }

    /**Backwards FFT**/
    for( int bb=0; bb<_rconf._batch_size; bb++){
        for( int op=0; op<_rconf._num_modes; op++){
            clfftEnqueueTransform( _clenv._fftPlanHandle, CLFFT_BACKWARD, 1, &_clenv.commands,0,NULL,NULL,&_clenv._dev_FIELD[bb][op], NULL,NULL);
        }}
    clFinish(_clenv.commands);

    /**Re-set gradients**/
    for( int nob=0; nob<_rconf._num_obj; nob++){
    for( int oo=0; oo<_rconf._num_omodes; oo++){
        _clenv.k_const_fill( _clenv._dev_grNRM[nob][oo], 1.0e-0f, _rconf._OX*_rconf._OY );
        _clenv.k_const_fill( _clenv._dev_grOBJ[nob][oo], 1.0e-10f, 2*_rconf._OX*_rconf._OY );
        }}
    for( int npr=0; npr<_rconf._num_pro; npr++){
    for( int pp=0; pp<_rconf._num_pmodes; pp++){
        _clenv.k_const_fill( _clenv._dev_grPRO[npr][pp], 0.0, 2*map_size ); } }
    clFinish(_clenv.commands);

    if( _rconf._mbatch_alg==0 ){
        /**Update view gradients if using magnitude delta**/
        for( int bb=0; bb<_rconf._batch_size; bb++){
            for( int oopp=0; oopp<_rconf._num_modes; oopp++){
                _clenv.k_update_view_mag(_clenv._dev_OBxPR[bb][oopp], _clenv._dev_FIELD[bb][oopp], map_size );
            }}
        clFinish(_clenv.commands);
    }

    /**Update object and probe gradients**/
    for( int bb=0; bb<_rconf._batch_size; bb++){
        for( int oo=0; oo<_rconf._num_omodes; oo++){
        for( int pp=0; pp<_rconf._num_pmodes; pp++){
            _clenv.k_gradupd_opn( _clenv._dev_grOBJ[_rdata._olist[bb]][oo], _clenv._dev_grPRO[_rdata._plist[bb]][pp], _clenv._dev_grNRM[_rdata._olist[bb]][oo],
                                 _clenv._dev_OBJ[_rdata._olist[bb]][oo], _clenv._dev_PRO[_rdata._plist[bb]][pp],_clenv._dev_FIELD[bb][pp+oo*_rconf._num_pmodes],
                                 _rdata._pos1D[bb], _rconf._OX, _rconf._num_pixels, Npx );
            }}
        clFinish(_clenv.commands);
        }


	float pmax =0.0;
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			for(int64_t xy=0; xy<_mbdata.PRO[npr][pp].n_pix; xy++) {
				pmax = max( pmax, norm(_mbdata.PRO[npr][pp][xy]) );
			}
		}
	}

    /**Update object and probe gradients**/
    for( int nob=0; nob<_rconf._num_obj; nob++){
    for( int oo=0; oo<_rconf._num_omodes; oo++){
        _clenv.k_grad_norm_scale_o( _clenv._dev_grOBJ[nob][oo], _clenv._dev_grNRM[nob][oo], 1.0/pmax, _rconf._OX*_rconf._OY ); }}
    for( int npr=0; npr<_rconf._num_pro; npr++){
    for( int pp=0; pp<_rconf._num_pmodes; pp++){
        _clenv.k_grad_norm_scale_p( _clenv._dev_grPRO[npr][pp], 1.0f/(float)_rconf._batch_size, map_size ); } }
    clFinish(_clenv.commands);


    /**Reading gradients back from CL device**/
    read_gradients();
    clFinish(_clenv.commands);

//    printf("Done\n");fflush(stdout);
}
