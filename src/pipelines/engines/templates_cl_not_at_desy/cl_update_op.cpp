#include "./cl_update_psi.hpp"
#define Npx _rconf._num_pixels
#include <unistd.h>


/**Do one update cycle**/
void cl_update_psi::UpdateObjectProbe(int niter) {
	/**Smooth the gradients...**/
	L2reg_ObjectGradients();
//    L2reg_ProbeGradients();

    if( niter>=_rconf._start_objupd){
        _optimizer.UpdateOBJ(gradOBJ);}

    if( niter>=_rconf._start_proupd){
        _optimizer.UpdatePRO(gradPRO); }

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			par_cap( _mbdata.OBJ[nob][oo] );
		}
	}

	export_batch();

//	std::string buf;
//	std::cin >> buf;
//	usleep(1000);
}

/**Calculate object gradient regularizer to smoothen the object**/
void cl_update_psi::L2reg_ObjectGradients() {
//    printf("Regularizing with object gradients...\n"); fflush(stdout);

	int64_t OX,OY;
	complex<float> laplO;
	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			OX = _mbdata.OBJ[nob][oo].npx_x;
			OY = _mbdata.OBJ[nob][oo].npx_y;
			for(int64_t yy=1; yy<OY-1; yy++) {
				for(int64_t xx=1; xx<OX-1; xx++) {
					laplO = 4.0f*_mbdata.OBJ[nob][oo][xx+yy*OX]-_mbdata.OBJ[nob][oo][xx+yy*OX-1]-_mbdata.OBJ[nob][oo][xx+yy*OX+1]
					        -_mbdata.OBJ[nob][oo][xx+yy*OX-OX]-_mbdata.OBJ[nob][oo][xx+yy*OX+OX];
					gradOBJ[nob][oo][xx+yy*OX] -= (float)_mag_l2reg_objgradients*(laplO);
				}
			}
		}
	}
}

/**Calculate object gradient regularizer to smoothen the object**/
void cl_update_psi::L2reg_ProbeGradients() {
    printf("Regularizing with object gradients...\n"); fflush(stdout);

	complex<float> laplP;
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			for(int64_t yy=1; yy<Npx-1; yy++) {
				for(int64_t xx=1; xx<Npx-1; xx++) {
					laplP = 4.0f*_mbdata.PRO[npr][pp][xx+yy*Npx]-_mbdata.PRO[npr][pp][xx+yy*Npx-1]-_mbdata.PRO[npr][pp][xx+yy*Npx+1]
					        -_mbdata.PRO[npr][pp][xx+yy*Npx-Npx]-_mbdata.PRO[npr][pp][xx+yy*Npx+Npx];
					gradPRO[npr][pp][xx+yy*Npx] -= (float)_mag_l2reg_objgradients*(laplP/(abs(_mbdata.PRO[npr][pp][xx+yy*Npx])+1e-16f));
				}
			}
		}
	}
}
