#include "cl_env.hpp"
#include <CL/cl.h>

const char *__kernel_field_divide = "\n" \
"__kernel void field_divide( __global float2 *FIELD, __global float *val ){     \n" \
"       const uint xy=get_global_id(0);                                         \n" \
"       FIELD[xy]= FIELD[xy]/val[xy];                                           \n" \
"       }                                                                       \n";

const char *__kernel_const_multiply = "\n" \
"__kernel void const_multiply(__global float2 *FIELD, __private float val){     \n" \
"       const uint xy=get_global_id(0);                                         \n" \
"       FIELD[xy] = FIELD[xy]*val;                                              \n" \
"       }                                                                       \n";

const char *__kernel_const_fill = "\n" \
"__kernel void const_fill(__global float *FIELD, __private float val){          \n" \
"       const uint xy=get_global_id(0);                                         \n" \
"       FIELD[xy]= val;                                                         \n" \
"       }                                                                       \n";

const char *__kernel_add_mode_intensity = "\n" \
"inline float  norm(float2 a){ return a.x*a.x+a.y*a.y; }                                  \n" \
"                                                                                         \n" \
"__kernel void add_mode_intensity(__global float *SUMINT, __global const float2 *FIELD){        \n" \
"       const uint xy=get_global_id(0);                                                   \n" \
"       SUMINT[xy] += norm(FIELD[xy]);                                                    \n" \
"       }                                                                                 \n";


const char *__kernel_calc_view = "\n" \
"inline float2 mult(float2 a, float2 b){ return (float2)(a.x*b.x-a.y*b.y,a.x*b.y+a.y*b.x); }    \n" \
"                                                                                               \n" \
"__kernel void calc_view(__global float2 *OXP, __global float2 *PSI, __global const float2 *OBJ, __global const float2 *PRO, __private int offset, __private int NxO, __private int NxP ){    \n" \
"       const uint xx=get_global_id(0);                                         \n" \
"       const uint yy=get_global_id(1);                                         \n" \
"                                                                               \n" \
"       OXP[xx+yy*NxP] = mult( OBJ[offset+xx+yy*NxO] , PRO[xx+yy*NxP] );        \n" \
"       PSI[xx+yy*NxP] = OXP[xx+yy*NxP];                                        \n" \
"       }                                                                       \n";


const char *__kernel_update_view_mag = "\n" \
"__kernel void update_view_mag(__global float2 *OXP, __global float2 *PSI ){                      \n" \
"       const uint xy=get_global_id(0);                                                           \n" \
"       PSI[xy] -= OXP[xy];                                                                       \n" \
"       }                                                                                         \n";


const char *__kernel_likelihood_update = "\n" \
"inline float  norm(float2 a){ return a.x*a.x+a.y*a.y; }                                          \n" \
"inline float  fnorm(float  a){ return a*a; }                                                     \n" \
"                                                                                                 \n" \
"__kernel void likelihood_update(__global float2 *FIELD, __global const float *IMG, __global const int *MASK, __global const float *SUMINT, __private float scale){          \n" \
"       const uint xy=get_global_id(0);                                                           \n" \
"       if(MASK[xy]==1){                                                                          \n" \
"           FIELD[xy] = FIELD[xy]*( fnorm(scale*IMG[xy]) / (SUMINT[xy]+1e-6f) -1.0f );              \n" \
"           }                                                                                     \n" \
"       }                                                                                         \n";

const char *__kernel_magnitude_update = "\n" \
"__kernel void magnitude_update(__global float2 *FIELD, __global const float *IMG, __global const int *MASK, __global const float *SUMINT, __private float scale){          \n" \
"       const uint xy=get_global_id(0);                                                           \n" \
"       if(MASK[xy]==1){                                                                          \n" \
"           FIELD[xy] = FIELD[xy]*( scale*IMG[xy] / (sqrt(SUMINT[xy])+1e-8f) );                     \n" \
"           }                                                                                     \n" \
"       }                                                                                         \n";


const char *__kernel_gradupd_opn = "\n" \
"inline float  norm(float2 a){ return a.x*a.x+a.y*a.y; }                                        \n" \
"inline float2 conj(float2 a){ return (float2)(a.x,-a.y); }                                     \n" \
"inline float2 mult(float2 a, float2 b){ return (float2)(a.x*b.x-a.y*b.y,a.x*b.y+a.y*b.x); }    \n" \
"                                                                               \n" \
"__kernel void gradupd_opn( __global float2 *grOBJ, __global float2 *grPRO, __global float *grNORM, __global float2 *OBJ, __global float2 *PRO, __global float2 *PSI, __private int offset, __private int NxO, __private int NxP ){   \n" \
"       const uint xx=get_global_id(0);                                         \n" \
"       const uint yy=get_global_id(1);                                         \n" \
"       const uint xy_o = offset+xx+yy*NxO;                                     \n" \
"       float2 pTmp = mult( conj(OBJ[xy_o]),      PSI[xx+yy*NxP] );             \n" \
"       float2 oTmp = mult( conj(PRO[xx+yy*NxP]), PSI[xx+yy*NxP] );             \n" \
"                                                                               \n" \
"       grOBJ[offset+xx+yy*NxO] += oTmp;                                        \n" \
"       grNORM[offset+xx+yy*NxO] += 1.0;                                       \n" \
"       grPRO[xx+yy*NxP]   += pTmp;                                             \n" \
"       }                                                                       \n";


const char *__kernel_grad_norm_scale = "\n" \
"__kernel void grad_norm_scale(__global float2 *grOBJ, __global float *grNORM, __private float SCALE){          \n" \
"       const uint xy=get_global_id(0);                                      \n" \
"       grOBJ[xy] = grOBJ[xy]*(SCALE/grNORM[xy]);                                     \n" \
"       }                                                                    \n";




const char *clOptimization="-cl-fast-relaxed-math";
//const char *clOptimization="";

ocl_env::ocl_env(){
    /**Set up openCL environment**/
    clGetPlatformIDs(1,&clPlatform,NULL);
    clGetDeviceIDs(clPlatform,CL_DEVICE_TYPE_GPU,1,&device_id,NULL);
    context=clCreateContext(0,1,&device_id,NULL,NULL,&err);
    commands=clCreateCommandQueue(context,device_id,0,&err);

    /**Compile the kernels on this maschine**/
    __const_multiply = ocl_compile(__kernel_const_multiply,"const_multiply");
    __const_fill = ocl_compile(__kernel_const_fill,"const_fill");
    __calc_view = ocl_compile(__kernel_calc_view,"calc_view");
    __update_view_mag = ocl_compile(__kernel_update_view_mag,"update_view_mag");
    __magnitude_update = ocl_compile(__kernel_magnitude_update,"magnitude_update");
    __likelihood_update = ocl_compile(__kernel_likelihood_update,"likelihood_update");
    __add_mode_intensity = ocl_compile(__kernel_add_mode_intensity,"add_mode_intensity");
    __gradupd_opn = ocl_compile(__kernel_gradupd_opn,"gradupd_opn");
    __gradnorm_ob = ocl_compile(__kernel_grad_norm_scale,"grad_norm_scale");
}

/**Compile the program on this maschine**/
cl_kernel ocl_env::ocl_compile(const char *kernel_string, const char* kernel_name){
     cl_int err;
     program=clCreateProgramWithSource(context,1,(const char **)&kernel_string,NULL,&err);
     if(err!=CL_SUCCESS){printf("CreateProgram ERROR: %s\n",kernel_name); }
     err=clBuildProgram(program,1,&device_id,clOptimization,NULL,NULL);
     if(err!=CL_SUCCESS){printf("BuildProgram ERROR: %s",kernel_name); }
     cl_kernel compiled = clCreateKernel(program,kernel_name,&err);
     if(err!=CL_SUCCESS){printf("CreateKernel ERROR: %s",kernel_name); }
    return compiled;
}

void ocl_env::gpu_alloc_3D(std::vector<std::vector<cl_mem>>& vec, int fields, int modes, size_t map_size){
    vec.resize(fields);
    for( int ff=0; ff<fields; ff++){
        vec[ff].resize(modes);
        for( int mm=0; mm<modes; mm++){
            vec[ff][mm] = clCreateBuffer(context,CL_MEM_READ_WRITE,map_size,NULL,NULL);
        }}
}

void ocl_env::gpu_free_3D(std::vector<std::vector<cl_mem>>& vec){
    for( int ff=0; ff<vec.size(); ff++){
        for( int mm=0; mm<vec[ff].size(); mm++){
            clReleaseMemObject(vec[ff][mm]);
        }}
}

void ocl_env::gpu_alloc_2D(std::vector<cl_mem>& vec, int fields, size_t map_size){
    vec.resize(fields);
    for( int ff=0; ff<fields; ff++){
        vec[ff] = clCreateBuffer(context,CL_MEM_READ_WRITE,map_size,NULL,NULL);
        }
}

void ocl_env::gpu_free_2D(std::vector<cl_mem>& vec){
    for( int ff=0; ff<vec.size(); ff++){
        clReleaseMemObject(vec[ff]);
        }
}

/**Allocating buffers on OCL device**/
void ocl_env::dev_alloc(int batch_size, int omode, int pmode, int n_pix, int o_x, int o_y, int n_msk, int n_ob, int n_pro){
    cl_ulong maxbuffersize;
    clGetDeviceInfo(device_id, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(cl_ulong),&maxbuffersize, NULL);
    printf("\nMax buffer size on this device: %li MB\n",(long int)maxbuffersize/(1024*1024)); fflush(stdout);

    /**Main wavefields**/
    gpu_alloc_3D(_dev_FIELD,batch_size,omode*pmode, sizeof(float)*2*n_pix*n_pix );
    gpu_alloc_3D(_dev_OBxPR,batch_size,omode*pmode, sizeof(float)*2*n_pix*n_pix );
    gpu_alloc_2D(_dev_SUMINT,batch_size, sizeof(float)*n_pix*n_pix );
    gpu_alloc_2D(_dev_IMG,batch_size, sizeof(float)*n_pix*n_pix );
    gpu_alloc_2D(_dev_MSK,n_msk, sizeof(int)*n_pix*n_pix );
    /**Object and its gradients**/
    gpu_alloc_3D(_dev_OBJ,n_ob,omode,   sizeof(float)*2*o_x*o_y );
    gpu_alloc_3D(_dev_grOBJ,n_ob,omode, sizeof(float)*2*o_x*o_y );
    gpu_alloc_3D(_dev_grNRM,n_ob,omode, sizeof(float)*o_x*o_y );
    /**Probe and its gradients**/
    gpu_alloc_3D(_dev_PRO,n_pro,pmode,   sizeof(float)*2*n_pix*n_pix );
    gpu_alloc_3D(_dev_grPRO,n_pro,pmode, sizeof(float)*2*n_pix*n_pix );

    /**Setup clFFT. **/
    cl_int err;
	clfftSetupData fftSetup;
	err = clfftInitSetupData(&fftSetup);
	err = clfftSetup(&fftSetup);

	clfftDim fft_dim = CLFFT_2D;
	size_t fft_shapes[2] = {(size_t)n_pix,(size_t)n_pix};
    err = clfftCreateDefaultPlan(&_fftPlanHandle,context,fft_dim,fft_shapes);
    err = clfftSetPlanPrecision(_fftPlanHandle, CLFFT_SINGLE);
	err = clfftSetResultLocation(_fftPlanHandle, CLFFT_INPLACE);
    err = clfftBakePlan(_fftPlanHandle, 0, &commands, NULL, NULL);
}

void ocl_env::k_const_multiply(cl_mem data, float val, size_t global_size){
    clSetKernelArg(__const_multiply,0,sizeof(cl_mem),&data);
    clSetKernelArg(__const_multiply,1,sizeof(float),&val);
    clEnqueueNDRangeKernel(commands,__const_multiply,1,NULL,&global_size,&__local_size,0,NULL,NULL);
}

void ocl_env::k_const_fill(cl_mem data, float val, size_t global_size){
    clSetKernelArg(__const_fill,0,sizeof(cl_mem),&data);
    clSetKernelArg(__const_fill,1,sizeof(float),&val);
    clEnqueueNDRangeKernel(commands,__const_fill,1,NULL,&global_size,&__local_size,0,NULL,NULL);
}

void ocl_env::k_calc_view(cl_mem oxp, cl_mem psi, cl_mem obj, cl_mem pro, int offset, int n_ox, int n_px, size_t global_size){
    size_t global_size_2D[2] = {global_size,global_size};
    size_t local_size_2D[2] = {16,1};

    clSetKernelArg(__calc_view,0,sizeof(cl_mem),&oxp);
    clSetKernelArg(__calc_view,1,sizeof(cl_mem),&psi);
    clSetKernelArg(__calc_view,2,sizeof(cl_mem),&obj);
    clSetKernelArg(__calc_view,3,sizeof(cl_mem),&pro);
    clSetKernelArg(__calc_view,4,sizeof(int),&offset);
    clSetKernelArg(__calc_view,5,sizeof(int),&n_ox);
    clSetKernelArg(__calc_view,6,sizeof(int),&n_px);
    clEnqueueNDRangeKernel(commands,__calc_view,2,NULL,global_size_2D,local_size_2D,0,NULL,NULL);
}

void ocl_env::k_update_view_mag(cl_mem oxp, cl_mem psi, size_t global_size){
    clSetKernelArg(__update_view_mag,0,sizeof(cl_mem),&oxp);
    clSetKernelArg(__update_view_mag,1,sizeof(cl_mem),&psi);
    clEnqueueNDRangeKernel(commands,__update_view_mag,1,NULL,&global_size,&__local_size,0,NULL,NULL);
}

void ocl_env::k_add_mode_intensity(cl_mem sumint, cl_mem psi, size_t global_size){
    clSetKernelArg(__add_mode_intensity,0,sizeof(cl_mem),&sumint);
    clSetKernelArg(__add_mode_intensity,1,sizeof(cl_mem),&psi);
    clEnqueueNDRangeKernel(commands,__add_mode_intensity,1,NULL,&global_size,&__local_size,0,NULL,NULL);
}


void ocl_env::k_magnitude_update(cl_mem psi, cl_mem img, cl_mem mask, cl_mem sumint, float scale, size_t global_size){
    clSetKernelArg(__magnitude_update,0,sizeof(cl_mem),&psi);
    clSetKernelArg(__magnitude_update,1,sizeof(cl_mem),&img);
    clSetKernelArg(__magnitude_update,2,sizeof(cl_mem),&mask);
    clSetKernelArg(__magnitude_update,3,sizeof(cl_mem),&sumint);
    clSetKernelArg(__magnitude_update,4,sizeof(float),&scale);
    clEnqueueNDRangeKernel(commands,__magnitude_update,1,NULL,&global_size,&__local_size,0,NULL,NULL);
}

void ocl_env::k_likelihood_update(cl_mem psi, cl_mem img, cl_mem mask, cl_mem sumint, float scale, size_t global_size){
    clSetKernelArg(__likelihood_update,0,sizeof(cl_mem),&psi);
    clSetKernelArg(__likelihood_update,1,sizeof(cl_mem),&img);
    clSetKernelArg(__likelihood_update,2,sizeof(cl_mem),&mask);
    clSetKernelArg(__likelihood_update,3,sizeof(float),&sumint);
    clSetKernelArg(__likelihood_update,4,sizeof(float),&scale);
    clEnqueueNDRangeKernel(commands,__likelihood_update,1,NULL,&global_size,&__local_size,0,NULL,NULL);
}

void ocl_env::k_gradupd_opn(cl_mem grobj, cl_mem grpro, cl_mem grnrm, cl_mem obj, cl_mem pro, cl_mem field, int offset, int n_ox, int n_px, size_t global_size){
    size_t global_size_2D[2] = {global_size,global_size};
    size_t local_size_2D[2] = {16,1};

    clSetKernelArg(__gradupd_opn,0,sizeof(cl_mem),&grobj);
    clSetKernelArg(__gradupd_opn,1,sizeof(cl_mem),&grpro);
    clSetKernelArg(__gradupd_opn,2,sizeof(cl_mem),&grnrm);
    clSetKernelArg(__gradupd_opn,3,sizeof(cl_mem),&obj);
    clSetKernelArg(__gradupd_opn,4,sizeof(cl_mem),&pro);
    clSetKernelArg(__gradupd_opn,5,sizeof(cl_mem),&field);
    clSetKernelArg(__gradupd_opn,6,sizeof(int),&offset);
    clSetKernelArg(__gradupd_opn,7,sizeof(int),&n_ox);
    clSetKernelArg(__gradupd_opn,8,sizeof(int),&n_px);
    clEnqueueNDRangeKernel(commands,__gradupd_opn,2,NULL,global_size_2D,local_size_2D,0,NULL,NULL);
}

void ocl_env::k_grad_norm_scale_o(cl_mem grobj, cl_mem grnrm, float scale, size_t global_size){
    clSetKernelArg(__gradnorm_ob,0,sizeof(cl_mem),&grobj);
    clSetKernelArg(__gradnorm_ob,1,sizeof(cl_mem),&grnrm);
    clSetKernelArg(__gradnorm_ob,2,sizeof(float),&scale);
    clEnqueueNDRangeKernel(commands,__gradnorm_ob,1,NULL,&global_size,&__local_size,0,NULL,NULL);
}

void ocl_env::k_grad_norm_scale_p(cl_mem grpro, float scale, size_t global_size){
    clSetKernelArg(__const_multiply,0,sizeof(cl_mem),&grpro);
    clSetKernelArg(__const_multiply,1,sizeof(float),&scale);
    clEnqueueNDRangeKernel(commands,__const_multiply,1,NULL,&global_size,&__local_size,0,NULL,NULL);
}


ocl_env::~ocl_env(){
    cl_int err;
    err = clfftDestroyPlan( &_fftPlanHandle );
    clfftTeardown();

    /**Main wavefield**/
    gpu_free_2D(_dev_SUMINT);
    gpu_free_2D(_dev_IMG);
    gpu_free_2D(_dev_MSK);
    gpu_free_3D(_dev_FIELD);
    gpu_free_3D(_dev_OBxPR);
    gpu_free_3D(_dev_OBJ);
    gpu_free_3D(_dev_grOBJ);
    gpu_free_3D(_dev_grNRM);
    gpu_free_3D(_dev_PRO);
    gpu_free_3D(_dev_grPRO);
}

