#ifndef PTYREC_ENGINE_CLENV_HPP_
#define PTYREC_ENGINE_CLENV_HPP_
#include <CL/cl.h>
#include <clFFT.h>
#include <vector>
#include <iostream>


class ocl_env{
    public:
        ocl_env();
        virtual ~ocl_env();
        cl_int              err,num_devices;
        cl_platform_id      clPlatform;
        cl_device_id        device_id;
        cl_context          context;
        cl_command_queue    commands;
        cl_program          program;

        clfftPlanHandle     _fftPlanHandle;

        //Create device memory objects
        std::vector<std::vector<cl_mem>> _dev_FIELD;
        std::vector<std::vector<cl_mem>> _dev_OBxPR;
        std::vector<cl_mem> _dev_SUMINT;
        std::vector<cl_mem> _dev_IMG;
        std::vector<cl_mem> _dev_MSK;
        std::vector<std::vector<cl_mem>> _dev_OBJ;
        std::vector<std::vector<cl_mem>> _dev_grOBJ;
        std::vector<std::vector<cl_mem>> _dev_grNRM;
        std::vector<std::vector<cl_mem>> _dev_PRO;
        std::vector<std::vector<cl_mem>> _dev_grPRO;

        void k_field_multiply(cl_mem,cl_mem,size_t);
        void k_const_multiply(cl_mem,float,size_t);
        void k_const_fill(cl_mem,float,size_t);
        void k_calc_view(cl_mem,cl_mem,cl_mem,cl_mem,int,int,int,size_t);
        void k_update_view_mag(cl_mem,cl_mem,size_t);
        void k_add_mode_intensity(cl_mem,cl_mem,size_t);
        void k_magnitude_update(cl_mem,cl_mem,cl_mem,cl_mem,float,size_t);
        void k_likelihood_update(cl_mem,cl_mem,cl_mem,cl_mem,float,size_t);
        void k_gradupd_opn(cl_mem,cl_mem,cl_mem,cl_mem,cl_mem,cl_mem,int,int,int,size_t);
        void k_grad_norm_scale_o(cl_mem,cl_mem,float,size_t);
        void k_grad_norm_scale_p(cl_mem,float,size_t);

        void dev_alloc( int,int, int, int, int, int, int, int, int );

    protected:
        size_t              __local_size = 16;
        cl_kernel           __field_multiply;
        cl_kernel           __const_multiply;
        cl_kernel           __const_fill;
        cl_kernel           __calc_view;
        cl_kernel           __update_view_mag;
        cl_kernel           __add_mode_intensity;
        cl_kernel           __magnitude_update;
        cl_kernel           __likelihood_update;
        cl_kernel           __gradupd_opn;
        cl_kernel           __gradnorm_ob;
        /**Helper functions**/
        cl_kernel           ocl_compile(const char*,const char*);
        void gpu_alloc_3D(std::vector<std::vector<cl_mem>>&,int,int,size_t);
        void gpu_alloc_2D(std::vector<cl_mem>&,int,size_t);
        void gpu_free_3D(std::vector<std::vector<cl_mem>>&);
        void gpu_free_2D(std::vector<cl_mem>&);
};

#endif // PTYREC_ENGINE_CLENV_HPP_
