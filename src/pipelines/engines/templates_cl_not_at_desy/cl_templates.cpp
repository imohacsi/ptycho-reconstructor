#include "./cl_templates.hpp"

#define Npx _rconf._num_pixels


/**Initializer outside of class declaration**/
void cl_engine_template::init() {
	_rconf._batch_size = std::min(_rconf._batch_size,_rconf._num_frames);

	double memory_gpu = 0.0;
	memory_gpu += 2*(_rconf._batch_size*_rconf._num_modes*_rconf._num_pixels*_rconf._num_pixels*8.0);
	memory_gpu += 2*(_rconf._batch_size*_rconf._num_pixels*_rconf._num_pixels*4.0);  //IMG and SUMINT
	memory_gpu += 1*(_rconf._num_msk*_rconf._num_pixels*_rconf._num_pixels*4.0);     //MSK
	memory_gpu += 2*(_rconf._num_pro*_rconf._num_pmodes*_rconf._num_pixels*_rconf._num_pixels*8.0);  //PRO and grPRO
	memory_gpu += 2*(_rconf._num_obj*_rconf._num_omodes*_rconf._OX*_rconf._OY*8.0);  //OBJ and grOBJ
	memory_gpu += 1*(_rconf._num_obj*_rconf._num_omodes*_rconf._OX*_rconf._OY*4.0);  //grNORM
    printf("%s\tReserved GPU memory: %g MB%s\n",A_C_CYAN,memory_gpu/(1024*1024),A_C_RESET); fflush(stdout);

	_anndata.OBJ.init(_rconf._num_obj, _rconf._num_omodes, _rdata.OBJ[0][0].npx_x, _rdata.OBJ[0][0].npx_y);
	_anndata.PRO.init(_rconf._num_pro, _rconf._num_pmodes, _rdata.PRO[0][0].npx_x, _rdata.PRO[0][0].npx_y);
}

/**Initialize buffers for random traversal of the batch**/
void cl_engine_template::initMBatchData() {
    /**Establishing connection between global and minibatch indexing**/
    _ring_idx.resize(_rconf._num_frames);
    std::iota( std::begin(_ring_idx),std::end(_ring_idx),0);
    std::shuffle(_ring_idx.begin(),_ring_idx.end(),  std::mt19937{std::random_device{}()} );

    /**Setting the pointers */
	_mbdata.OBJ = _rdata.OBJ;
	_mbdata.PRO = _rdata.PRO;
	_mbdata.PSI = _rdata.PSI;
//	_mbdata.PSI.init(_rconf._batch_size,_rconf._num_modes,_rconf._num_pixels,_rconf._num_pixels);
	_mbdata.MSK = _rdata.MSK;

	_mbdata.IMG = (float**)calloc(_rconf._num_frames,sizeof(float*));
	_mbdata._plist = (int*)calloc(_rconf._num_frames,sizeof(int));
	_mbdata._olist = (int*)calloc(_rconf._num_frames,sizeof(int));
	_mbdata._msklist = (int*)calloc(_rconf._num_frames,sizeof(int));
	_mbdata._pos1D = (int64_t*)calloc(_rconf._num_frames,sizeof(int64_t));
	_mbdata._pos2D = (double**)calloc(_rconf._num_frames,sizeof(double));
}

void cl_engine_template::setMBatchData() {
	if( (_ring_start+_rconf._batch_size)> _rconf._num_frames ) {
//		printf("Reshufling...\n");
		std::shuffle( _ring_idx.begin(), _ring_idx.end(), std::mt19937{std::random_device{}()} );
		_ring_start=0;
	}

	for( int64_t ff=_ring_start; ff<_ring_start+_rconf._batch_size; ff++) {
		_mbdata.IMG[ff-_ring_start] = _rdata.IMG[_ring_idx[ff]];
		_mbdata._plist[ff-_ring_start] = _rdata._plist[_ring_idx[ff]];
		_mbdata._olist[ff-_ring_start] = _rdata._olist[_ring_idx[ff]];
		_mbdata._msklist[ff-_ring_start] = _rdata._msklist[_ring_idx[ff]];
		_mbdata._pos1D[ff-_ring_start] = _rdata._pos1D[_ring_idx[ff]];
	}
	_ring_start+=_rconf._batch_size;
}



/**Single API command to run the reconstruction**/
void cl_engine_template::run_reconstruction() {

	for(niter=0; niter<_rconf._num_curr_iter; niter++) {
		make_iteration();

		/**Add to simulated annealing over last iterations for noise reduction*/
		if(niter>(_rconf._num_curr_iter*3)/4) {
			relaxAddCurrent();
		}

		if(niter%_rconf._WriteEveryNthIter==0) {
            if(_rconf._do_calc_print && _rconf._num_pmodes>1) { diag_prmode_int(); }
			print_current_iteration();
		}
	}

	relaxNormalize();
}

/**Make a single iteration by offloading the heavy duty to the GPU**/
void cl_engine_template::make_iteration() {

	/**Re-set iteration parameters**/
	setMBatchData();
	clock_t Tstart,Tend;
	Tstart=clock();

	/**Update views from measured data**/
    view_update_loop.image_pipeline();

	/**Update the object and probe estimates**/
	view_update_loop.UpdateObjectProbe(niter);

	/**Printing benchmark and status*/
	Tend=clock();
	double Titer=(double)(Tend-Tstart)/CLOCKS_PER_SEC;
	printf("%sFinished %d th of %d iter. (%g s)%s\n",A_C_GREEN, (int)niter, (int)_rconf._num_curr_iter,Titer, A_C_RESET);
	fflush(stdout);
}

/**Diagnostics to compare probe mode intensities**/
void cl_engine_template::diag_prmode_int() {
	double MODEINT[_rconf._num_pro][_rconf._num_pmodes];
	double TOTINT[_rconf._num_pro];

	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			MODEINT[npr][pp]= sum( _rdata.PRO[npr][pp] );
			TOTINT[npr]+=MODEINT[npr][pp];
		}
	}

	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		printf("\t%sIntensity in the %dth probe's modes: ",A_C_YELLOW, (int)npr);
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			printf( "%0.3g",MODEINT[npr][pp]/TOTINT[npr]);
		}
		printf("%s\n",A_C_RESET);
		fflush(stdout);
	}
}

/**Add current object and probe to relaxation**/
void cl_engine_template::relaxAddCurrent() {
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			_anndata.PRO[npr][pp]+=_rdata.PRO[npr][pp];
		}
	}

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			_anndata.OBJ[nob][oo]+=_rdata.OBJ[nob][oo];
		}
	}

	num_relaxed++;
}

/**Normalize relaxation**/
void cl_engine_template::relaxNormalize() {
	double norm = 1.0/(double)num_relaxed;

	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			_anndata.PRO[npr][pp]*=norm;
		}
	}

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			_anndata.OBJ[nob][oo]*=norm;
		}
	}
}

/**Write out current progress as bitmap files**/
void cl_engine_template::print_current_iteration() {
	char filename[512];
	for(int64_t npr=0; npr<_rconf._num_pro; npr++) {
		for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
			sprintf(filename,"./output/Citer_Probe_P%d_M%d_I.bmp",(int)npr,(int)pp);
			ImWrite(_rdata.PRO[npr][pp],"lin","int","BMP",filename);
			sprintf(filename,"./output/Citer_Probe_P%d_M%d_P.bmp",(int)npr,(int)pp);
			ImWrite(_rdata.PRO[npr][pp],"lin","arg","BMP",filename);
		}
	}

	for(int64_t nob=0; nob<_rconf._num_obj; nob++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			sprintf(filename,"./output/Citer_Object_O%d_M%d_I.bmp",(int)nob,(int)oo);
			ImWrite(_rdata.OBJ[nob][oo],"lin","int","BMP",filename);
			sprintf(filename,"./output/Citer_Object_O%d_M%d_P.bmp",(int)nob,(int)oo);
			ImWrite(_rdata.OBJ[nob][oo],"lin","arg","BMP",filename);
		}
	}
}




