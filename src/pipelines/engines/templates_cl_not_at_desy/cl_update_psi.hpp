#ifndef PTYREC_ENGINE_MINIBATCH_CUDA_HPP_
#define PTYREC_ENGINE_MINIBATCH_CUDA_HPP_

#include <cstring>
#include <omp.h>
#include <time.h>
#include <CL/cl.h>
#include <clFFT.h>

#include "../parallelconfig.hpp"
#include "cl_env.hpp"
#include "../templates_minibatch/optimizers/optimizer_wrapper.hpp"

class cl_update_psi {
public:
	parformat            _rconf;
	ptydata<float>       _mbdata;
	ptydata<double>      _rdata;
    ocl_env              _clenv;


	/**Compact constructor**/
	cl_update_psi(parformat settings): _rconf(settings),_optimizer(settings._optim_alg,settings) {};
//	cl_update_psi(parformat settings, ptydata<double> &data): _rconf(settings),_rdata(data),_optimizer(settings._optim_alg,settings) { init(); };

	void setParameters(parformat settings, ptydata<double> &idata) {
		_rconf = settings;
		_rdata = idata;
		init();
		_optimizer.setParameters(_rconf,_mbdata);
	};

	/**Main API call**/
	void image_pipeline();
	void UpdateObjectProbe(int);

protected:
	int niter;
	/**Oxp and propagation field**/
	trifield<complex<float>>   OBxPR;
	trifield<complex<float>>   FIELD;
    /**Object and probe gradients**/
	trifield<complex<float>>   gradOBJ;
	trifield<complex<float>>   gradPRO;

    /**Optimizer wrapper**/
	optimizer_wrapper<float> _optimizer;

	void read_fields();
	void read_gradients();
	void fill_batch();
	void export_batch();

	void init();
	void finish();

	/**Object and probe update functions**/
	void Upd_opGRAD();
	double _mag_l2reg_objgradients;
	void L2reg_ObjectGradients();
	void L2reg_ProbeGradients();


	/**Additional support constraints**/
	void supProbeRSupport();
	void supProbeQSupport();

};/**minibatch_update_psi**/



//#endif // __CUDACC__


#endif // PTYREC_ENGINE_MINIBATCH_CUDA_HPP_
