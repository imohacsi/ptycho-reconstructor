#ifndef PTYREC_ENGINE_OCL_SWITCH_HPP_
#define PTYREC_ENGINE_OCL_SWITCH_HPP_

#define USE_OPENCL TRUE

#if USE_OPENCL
    #include "./cl_update_psi.hpp"
#else
    #include "../templates_minibatch/minibatch_templates.hpp"
    class cl_engine_template: public minibatch_engine_template<double> {
        public:
        cl_engine_template(parformat iconf, ptydata<double> rdata): minibatch_engine_template<double>(iconf,rdata) {};


    };
#endif // USE_OPENCL

#endif // PTYREC_ENGINE_OCL_SWITCH_HPP_



