#ifndef DESYPTYCHO_ENGINE_PAR_TEMP_CONFIG_H_
#define DESYPTYCHO_ENGINE_PAR_TEMP_CONFIG_H_

#include <cstring>
#include <fftw3.h>
#include <omp.h>
#include <time.h>

#include "./engine_format.hpp"

#ifndef Npx
#define Npx _rconf._num_pixels
#endif

class parformat: public ptyconf, public engine_common, public auxil_common {
public:
	int64_t _num_modes;

	parformat( ptyconf wconf, engine_common econf, auxil_common aconf ):
		ptyconf(wconf), engine_common(econf), auxil_common(aconf) {
		_num_modes = _num_pmodes*_num_omodes;
	};
};

#endif // DESYPTYCHO_ENGINE_PAR_TEMP_CONFIG_H_
