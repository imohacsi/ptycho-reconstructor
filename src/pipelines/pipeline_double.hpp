#ifndef PTYREC_PIPELINE_DOUBLE_HPP_
#define PTYREC_PIPELINE_DOUBLE_HPP_
#include "./pipeline.hpp"

class pipeline_fp64: public pipeline {
    public:
        pipeline_fp64(configure conf, ptyconf wconf, expdata &edata): pipeline(conf,wconf,edata) {};
        void run_pipeline();

    protected:
        ptydata<double> _rdata;
        ptydata<double> _refdata;

        void make_reconstruction();
        void make_receng(int);
        void make_posref(int, int);
        void make_iguess(int);
};

#endif // PTYREC_PIPELINE_DOUBLE_HPP_
