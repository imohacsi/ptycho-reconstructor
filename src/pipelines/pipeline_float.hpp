#ifndef PTYREC_PIPELINE_FLOAT_HPP_
#define PTYREC_PIPELINE_FLOAT_HPP_
#include "./pipeline.hpp"

class pipeline_fp32: public pipeline {
    public:
        pipeline_fp32(configure conf, ptyconf wconf, expdata &edata): pipeline(conf,wconf,edata) {};
        void run_pipeline();

    protected:
        ptydata<float> _rdata;
        ptydata<float> _refdata;

        void make_reconstruction();
        void make_receng(int);
        void make_posref(int, int);
        void make_iguess(int);
};

#endif // PTYREC_PIPELINE_FLOAT_HPP_

