////#include "./engine_DM.hpp"
//#include "./engine_parallel_projections.hpp"
//
//
//
//void engine_pproj::Scale_Patterns() {
//    int Npx=_Wconf._num_pixels;
//    int Nfr=_Wconf._num_frames;
//    int thID=0;
//
//    int n_iref_iter =a10;
//    double lrate = 0.05;
//    double nominator, denominator, alpha;
//
//    for( int iref=0; iref<n_iref_iter; iref++ ) {
//        for(ssize_t ff=0; ff<Nfr; ff++) {
//            nominator=0.0;
//            denominator=0.0;
//            alpha=ScalePatterns[ff];
//            for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
//                for(int oo=0; oo<_Wconf._num_omodes; oo++) {
//                    /**Going to detector plane*/
//                    OxP(thID,_Wdata._pos1D[ff],pp,oo,ff,_Wdata._plist[ff],_Wdata._olist[ff]);
//                    fftw_execute(PlanFW[thID]);
//                    FFTnorm(thID);
//                    Copy_to_Mode_Buffer(thID,pp+oo*_Wconf._num_pmodes);
//                }
//            }
//            Sum_Modes(thID);
//
//            for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                if(_Wdata.MSK[_Wdata._msklist[ff]][xy]==1) {
//                    nominator+=alpha*norm(norm(_Wdata.IMG[ff][xy]))-norm(SUMINT[thID][xy])*norm(_Wdata.IMG[ff][xy]);
//                    denominator+=norm(norm(_Wdata.IMG[ff][xy]));
//                }
//            }
//            ScalePatterns[ff]=alpha-lrate*(nominator/(denominator+1.0));
//
//            if(ff<10) {
//                std::cout<<"WARNING: amplitude scaling active.\n nom.: "<<  nominator << " denom.: " << denominator << " total: " << ScalePatterns[ff] << std::endl;
//            }
//        }
//    }
//
//    double avgscale = 0.0;
//    for(ssize_t ff=0; ff<Nfr; ff++) {
//        avgscale += ScalePatterns[ff];
//    }
//    avgscale = avgscale/double(Nfr);
//    /**Normalizing average scale to 1 **/
//    for(ssize_t ff=0; ff<Nfr; ff++) {
//        ScalePatterns[ff]*=1.0/avgscale;
//    }
//    std::cout << "Average scale was " << avgscale << std::endl;
//
//}
//
//
