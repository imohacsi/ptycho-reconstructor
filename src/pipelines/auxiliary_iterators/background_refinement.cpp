////#include "./engine_DM.hpp"
//#include "./engine_parallel_projections.hpp"
//
//
//
//void engine_pproj::Update_BGR() {
//    double tmp;
//    int Npx=_Wconf._num_pixels;
//    int Nfr=_Wconf._num_frames;
//    int thID,oopp;
//    double terror=0.0;
//
//    double *DHAM=(double*)calloc(Npx*Npx,sizeof(double));
//    double *DDHAM=(double*)calloc(Npx*Npx,sizeof(double));
//    double *PsiSQ=(double*)calloc(Npx*Npx,sizeof(double));
//    double *ETA=DDHAM;
//    double *nrmETA=DHAM;
//
//    int Nit=10;
//    if(_comconf._bgref_alg=="paper") {
//        Nit=1;
//    }
//
//
//    for(int brit=0; brit<Nit; brit++) {
//        terror=0.0;
//        memset(PsiSQ,0,Npx*Npx*sizeof(double));
//        memset(ETA,0,Npx*Npx*sizeof(double));
//        memset(nrmETA,0,Npx*Npx*sizeof(double));
//
//        #pragma omp parallel for private(thID,oopp,tmp)
//        for(ssize_t ff=0; ff<Nfr; ff++) {
//            thID=omp_get_thread_num();
//            for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
//                for(int oo=0; oo<_Wconf._num_omodes; oo++) {
//                    /**Going to detector plane*/
//                    oopp=pp+oo*_Wconf._num_pmodes;
//                    OxP(thID,_Wdata._pos1D[ff],pp,oo,ff,_Wdata._plist[ff],_Wdata._olist[ff]);
//                    fftw_execute(PlanFW[thID]);
//                    FFTnorm(thID);
//                    Copy_to_Mode_Buffer(thID,oopp);
//                }
//            }
//            Sum_Modes(thID);
//
//
//
//            if(_comconf._bgref_alg=="errgrad") {
//                /**First calculate gradients...*/
//                for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                    if(_Wdata.MSK[_Wdata._msklist[ff]][xy]) {
//                        tmp=2.0*(norm(SUMINT[thID][xy])-(norm(_Wdata.IMG[ff][xy])-BGROUND[xy]));
//                        #pragma omp atomic
//                        DHAM[xy]+=tmp;
//                        tmp=1.0;
//                        #pragma omp atomic
//                        DDHAM[xy]+=tmp;
//                        tmp=norm(norm(SUMINT[thID][xy])-(norm(_Wdata.IMG[ff][xy])-BGROUND[xy]));
//                        #pragma omp atomic
//                        terror+=tmp;
//                    }
//                }
//            }
//
//            if(_comconf._bgref_alg=="likegrad") {
//                /**First calculate gradients...*/
//                for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                    if(_Wdata.MSK[_Wdata._msklist[ff]][xy]) {
//                        tmp=1.0-(norm(_Wdata.IMG[ff][xy])-BGROUND[xy])/norm(SUMINT[thID][xy]);
//                        #pragma omp atomic
//                        DHAM[xy]+=tmp;
//                        tmp=1.0/norm(SUMINT[thID][xy]);
//                        #pragma omp atomic
//                        DDHAM[xy]+=tmp;
//                        tmp=0.5/norm(SUMINT[thID][xy])*norm( norm(SUMINT[thID][xy])- (norm(_Wdata.IMG[ff][xy])-BGROUND[xy]) );
//                        #pragma omp atomic
//                        terror+=tmp;
//                    }
//                }
//            }
//
//            if(_comconf._bgref_alg=="paper") {
//                for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                    if(_Wdata.MSK[_Wdata._msklist[ff]][xy]) {
//                        tmp=(norm(_Wdata.IMG[ff][xy])-BGROUND[xy])*norm(SUMINT[thID][xy]);
//                        #pragma omp atomic
//                        ETA[xy]+=tmp;
//                        tmp=norm(norm(_Wdata.IMG[ff][xy])-BGROUND[xy]);
//                        #pragma omp atomic
//                        nrmETA[xy]+=tmp;
//                        tmp=norm(SUMINT[thID][xy]);
//                        #pragma omp atomic
//                        PsiSQ[xy]+=tmp;
//                    }
//                }
//            }
//        }
//
//
//
//        /**From the fancy augmented projections paper (Marchesini et al.,
//            Inverse Problems 29, 11 (2013))...*/
//        if(_comconf._bgref_alg=="paper") {
//            for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                ETA[xy]=ETA[xy]/(nrmETA[xy]+1e-32);
//            }
//            for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                if( ETA[xy]<3.8) {
//                    ETA[xy]=3.8;
//                }
//            }
//            for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                PsiSQ[xy]*=-1.0/ETA[xy];
//            }
//
//            #pragma omp parallel for private(tmp)
//            for(ssize_t ff=0; ff<Nfr; ff++) {
//                for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                    tmp=(norm(_Wdata.IMG[ff][xy])-BGROUND[xy]);
//                    #pragma omp atomic
//                    PsiSQ[xy]+=tmp;
//                }
//            }
//            /**Do the actual background update*/
//            for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                BGROUND[xy]+=PsiSQ[xy]/double(Nfr);
//            }
//        }
//
//        /**Self developed method using gradient descent with the
//            Newton-Rhapson method on conventional error metric.*/
//        if(_comconf._bgref_alg=="errgrad") {
//            /**Do the actual background update*/
//            for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                BGROUND[xy]-=0.75*DHAM[xy]/(DDHAM[xy]+1e-32);
//            }
//        }
//
//        /**Self developed method using gradient descent with the
//            Newton-Rhapson method on Gaussian log-likelihood metric.*/
//        if(_comconf._bgref_alg=="likegrad") {
//            /**Do the actual background update*/
//            for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                BGROUND[xy]-=0.75*DHAM[xy]/(DDHAM[xy]+1e-32);
//            }
//        }
//
//
//        /**And correct non physical values*/
//        #pragma omp parallel for
//        for(ssize_t ff=0; ff<Nfr; ff++) {
//            for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//                if(!_Wdata.MSK[_Wdata._msklist[ff]][xy]) {
//                    BGROUND[xy]=0.0;
//                }
//            }
//        }
//        for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//            if(BGROUND[xy]<0.0) {
//                BGROUND[xy]=0.0;
//            }
//        }
//
//        /**Preventing probe from entering into background*/
//        fftshift(BGROUND,Npx,Npx);
//        for(ssize_t xx=-Npx/2; xx<Npx/2; xx++) {
//            for(ssize_t yy=-Npx/2; yy<Npx/2; yy++) {
//                if(xx*xx+yy*yy < _comconf._bgref_q*_comconf._bgref_q) {
//                    BGROUND[xx+Npx/2+(yy+Npx/2)*Npx]=0.0;
//                }
//            }
//        }
//        fftshift(BGROUND,Npx,Npx);
//        std::cout << "Total error metric is: " << terror << std::endl;
//    }
//
//
//
//    /**End of function diagnostics*/
//    WriteImOut(BGROUND,Npx,Npx,"lin","int","TIFF","./output/BGROUND.tif");
//    double sum=0;
//    for(ssize_t xy=0; xy<Npx*Npx; xy++) {
//        sum+=BGROUND[xy];
//    }
//    std::cout << "Total BG is: " << sum << std::endl;
//    std::cout << "Total ERR is: " << terror << std::endl;
//    free(DHAM);
//    free(DDHAM);
//    free(PsiSQ);
//}
//
//
