#include "./posref.hpp"

#define Npx _rconf._num_pixels


/**Initializer outside of class declaration**/
void posref_data::init() {
	BUFER.init(_rconf._num_THREADS,_rconf._num_modes,_rconf._num_pixels,_rconf._num_pixels);
	FIELD.init(1,_rconf._num_THREADS,_rconf._num_pixels,_rconf._num_pixels);
	SUMINT.init(1,_rconf._num_THREADS,_rconf._num_pixels,_rconf._num_pixels);

	FFTplans = new fftw_allprec<double>[_rconf._num_THREADS];
	for( int64_t tt=0; tt< _rconf._num_THREADS; tt++) {
		FFTplans[tt].init( FIELD[0][tt].dvec, FIELD[0][tt].npx_x );
	}
}

/**Destructor outside of class declaration**/
void posref_data::finish() {
	printf("DESTROYING POSITION REFINEMENT TEMPORARIES\n");
	fflush(stdout);


	delete[] FFTplans;
	BUFER.free();
	FIELD.free();
	SUMINT.free();
	printf("DONE\n");
	fflush(stdout);
}



/**Propagate from the sample to the detector plane**/
void posref_data::passFORWARD(int64_t thID, int64_t ff, int64_t xypos) {
	for(int64_t pp=0; pp<_rconf._num_pmodes; pp++) {
		for(int64_t oo=0; oo<_rconf._num_omodes; oo++) {
			setView_OxP(thID,xypos,pp,oo,ff,_rdata._plist[ff],_rdata._olist[ff]);
			FFTplans[thID].execute_fw();
			normFFT(thID);
			BUFER[thID][pp+oo*_rconf._num_pmodes].imp( FIELD[0][thID] );
		}
	}
	Sum_Modes(thID);
}

void posref_data::setView_OxP(int64_t thID, int64_t spos, int64_t pp, int64_t oo, int64_t ff, int64_t npr, int64_t nob) {
	FIELD[0][thID].impsel( _rdata.OBJ[nob][oo], spos );
	FIELD[0][thID]*=_rdata.PRO[npr][pp];
}

/**Sum up all object and probe modes. This generates the
     calculated amplitudes on the detector.**/
void posref_data::Sum_Modes(int64_t thID) {
	/**Zero buffer**/
	SUMINT[0][thID].zero();

	/**Sum up modes**/
	for(int64_t oopp=0; oopp<_rconf._num_modes; oopp++) {
		for(int64_t xy=0; xy<Npx*Npx; xy++) {
			SUMINT[0][thID].dvec[xy]+=norm(BUFER[thID][oopp].dvec[xy]);
		}
	}

	/**Convert to amplitude**/
	sqrt( SUMINT[0][thID] );
}

/**Calculates the mismatch between the measured
*   and calculated data	(mse error metric).**/
double posref_data::diag_error_msqerr(int64_t thID, int64_t ff) {
	double frerror=0.0;
	for(int64_t xy=0; xy<Npx*Npx; xy++) {
		if(_rdata.MSK[_rdata._msklist[ff]][xy]==1) {
//            frerror+=norm((norm(_rdata.IMG[ff][xy])-extBGROUNDI[xy])-norm(SUMINT[thID].data[xy]));
			frerror+=norm( norm(_rdata.IMG[ff][xy])-norm(SUMINT[0][thID].dvec[xy]) );
		}
	}
	return frerror;
}


/**Refine probe positions using the moderated Newton-Rhapson method**/
double posref_data::run_probeposition_refinement() {
	double dF_x,dF_y;
	double ddF_x,ddF_y;
	double r_jump;
	std::valarray<double> dpos_x(_rconf._num_frames);
	std::valarray<double> dpos_y(_rconf._num_frames);

	#pragma omp parallel for private(dF_x,dF_y,ddF_x,ddF_y,r_jump)
	for(int64_t ff=0; ff<_rconf._num_frames; ff++) {
//      //Second order approximates...
//      dF_x = image_pipeline(ff,1,0) - image_pipeline(ff,-1,0);
//      dF_y = image_pipeline(ff,0,1) - image_pipeline(ff,0,-1);
//      ddF_x = image_pipeline(ff,1,0) + image_pipeline(ff,-1,0) - 2*image_pipeline(ff,0,0);
//      ddF_y = image_pipeline(ff,0,1) + image_pipeline(ff,0,-1) - 2*image_pipeline(ff,0,0);

		//Fourth order approximates
		dF_x = ( -1.0/12.0*image_pipeline(ff,2,0) + 2.0/3.0*image_pipeline(ff,1,0) - 2.0/3.0*image_pipeline(ff,-1,0) + 1.0/12.0*image_pipeline(ff,-2,0) );
		dF_y = ( -1.0/12.0*image_pipeline(ff,0,2) + 2.0/3.0*image_pipeline(ff,0,1) - 2.0/3.0*image_pipeline(ff,0,-1) + 1.0/12.0*image_pipeline(ff,0,-2) );
		ddF_x = ( -1.0/12.0*image_pipeline(ff,2,0) + 4.0/3.0*image_pipeline(ff,1,0) - 5.0/2.0*image_pipeline(ff,0,0) + 4.0/3.0*image_pipeline(ff,-1,0) - 1.0/12.0*image_pipeline(ff,-2,0) );
		ddF_y = ( -1.0/12.0*image_pipeline(ff,0,2) + 4.0/3.0*image_pipeline(ff,0,1) - 5.0/2.0*image_pipeline(ff,0,0) + 4.0/3.0*image_pipeline(ff,0,-1) - 1.0/12.0*image_pipeline(ff,0,-2) );

		dpos_x[ff] = dF_x/(ddF_x+1e-16);
		dpos_y[ff] = dF_y/(ddF_y+1e-16);

		/**Protection against large steps*/
		r_jump = sqrt(dpos_x[ff]*dpos_x[ff]+dpos_x[ff]*dpos_x[ff]);
		if( r_jump > _rconf._posref_maxjump ) {
			dpos_x[ff] *= _rconf._posref_maxjump/r_jump;
			dpos_y[ff] *= _rconf._posref_maxjump/r_jump;
		}

		_rdata._pos2D[ff][0] -= _rconf._lrate_posref*dpos_x[ff];
		_rdata._pos2D[ff][1] -= _rconf._lrate_posref*dpos_y[ff];
	}
	double d_std = sqrt( ( dpos_x*dpos_x + dpos_y*dpos_y ).sum() / (double)_rconf._num_frames );
	double d_avg = sqrt(   dpos_x*dpos_x + dpos_y*dpos_y ).sum() / (double)_rconf._num_frames;

	printf("Displacement statistics:\n");
	printf("\tAverage: %g\n",d_avg);
	printf("\tStdDev: %g\n",d_std);
	return 0.0;
}

double posref_data::image_pipeline(int ff,int dx, int dy) {
	int thID=omp_get_thread_num();
	/**Forward pass: from sample plane to reciprocal space**/
	int64_t xypos = _rdata._pos1D[ff] + dx + dy*_rconf._OX;
	passFORWARD(thID,ff,xypos );
	double frerr = diag_error_msqerr(thID,ff);
	return frerr;
}
