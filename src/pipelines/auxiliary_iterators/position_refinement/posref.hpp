#ifndef PTYREC_POSITION_REFINEMENT_HPP_
#define PTYREC_POSITION_REFINEMENT_HPP_

#include <iostream>
#include <string>
#include <vector>
#include <complex>
#include <fftw3.h>
#include <hdf5.h>
#include <omp.h>
#include <time.h>
#include <valarray>

#include "../../engines/parallelconfig.hpp"
#include "../../../utilities/colorconsole.hpp"
#include "../../../utilities/Field2D.hpp"
#include "../../../utilities/Field2D_ops.hpp"
#include "../../../utilities/wavefield.hpp"

#define Npx _rconf._num_pixels


class posref_data {
public:
	parformat _rconf;
	ptydata<double>   _rdata;

	/**Compact constructor and destructor**/
	posref_data(parformat &settings, ptydata<double> &data): _rconf(settings),_rdata(data) {
		init();
	};
	~posref_data() {
		finish();
	}

	double run_probeposition_refinement();

private:
	/**Detailed initializer and destructor**/
	void init();
	void finish();

	trifield<std::complex<double>>  BUFER;
	trifield<std::complex<double>>  FIELD;
	trifield<double>                SUMINT;

	void prepare_refinement();
	double image_pipeline(int,int,int);

	void passFORWARD(int64_t,int64_t,int64_t);
	void setView_OxP(int64_t,int64_t,int64_t,int64_t,int64_t,int64_t,int64_t);
	void Sum_Modes(int64_t);

	double diag_error_msqerr(int64_t,int64_t);

	/**FFT related functions**/
	fftw_allprec<double> *FFTplans;
	void normFFT(int thID) {
		double norm=1.0/(double)Npx;
		FIELD[0][thID]*=norm;
	};

};

#undef Npx
#endif
