#include "pipeline_double.hpp"
#include "../initialguess/Initial_guess.hpp"



void pipeline_fp64::run_pipeline() {
	this->_rdata = _edata;

	/**Making initial guess...**/
	printf("%sPIPELINE stage: initial guess...%s\n",A_C_MAGENTA,A_C_RESET);
	fflush(stdout);
	this->make_iguess(0);

	/**Setting up and run reconstruction motor**/
	printf("%sPIPELINE stage: first reconstruction...%s\n",A_C_MAGENTA,A_C_RESET);
	fflush(stdout);
	this->conf_receng(0);
	this->make_receng(1);

	printf("\tWriting first reconstruction results!\n"); fflush(stdout);
	print_double_data PRINTER_1( _wconf, _rdata,_refdata,_config.print_out);
	PRINTER_1.Print_Reconstruction_Results();

	/**Running auxiliary refinement steps**/
	if( _config.engin_in.auxcom._do_posref!=0) {
		printf("\nRepeating reconstruction after posref...\n\n");
		for( int64_t rr=0; rr<_config.engin_in.auxcom._posref_nstep; rr++) {
			make_posref(0,rr);
		}
	}

	/**Making new guess for final refinement**/

	/**Re-making initial guess...**/
	printf("%sPIPELINE stage: initializing refinement...%s\n",A_C_MAGENTA,A_C_RESET); fflush(stdout);
	this->make_iguess(1);

	/**Setting up and run refinement motor**/
	printf("%sPIPELINE stage: refinement reconstruction...%s\n",A_C_MAGENTA,A_C_RESET); fflush(stdout);
	this->conf_receng(1);
	this->make_receng(1);

	print_double_data PRINTER_2( _wconf, _rdata,_refdata,_config.print_out);
	PRINTER_2.Print_Reconstruction_Results();
}

void pipeline_fp64::make_receng( int rstatus ) {
	motor<double> REC_MOTOR(_wconf,_rdata,_config.engin_in);

	REC_MOTOR.run_reconstruction_worker();

    //Retrieve data
	this->_rdata = REC_MOTOR._rdata;
	if( rstatus==1 ) {
		this->_refdata = REC_MOTOR._Rdata;
	}
}

void pipeline_fp64::make_posref(int rstatus, int priter) {
	/**Creating custom configuration file...**/
	ptyconf _custom_wconf = conf_tag(_wconf,(char*)"PR",priter);
	_custom_wconf._num_curr_iter = _config.engin_in.auxcom._posref_niter;

	/**Making position refinement**/
	printf("%sStarting probe position refinement...%s\n",A_C_GREEN,A_C_RESET);
	fflush(stdout);
	parformat rconf( _custom_wconf, _config.engin_in.engcom, _config.engin_in.auxcom);
	posref_data POSREF(rconf,_rdata);
	POSREF.run_probeposition_refinement();
	printf("%s\tDone with position refinement%s\n",A_C_GREEN,A_C_RESET);

	/**Making new object guess**/
	this->make_iguess(2);

	/**Evaluating the refinement by a new reconstruction**/
	this->conf_receng(0);
	this->make_receng(1);

	/**Printing out the reconstruction status**/
	print_double_data PRINTER( _custom_wconf, _rdata,_refdata,_config.print_out);
	PRINTER.Print_Reconstruction_Results();
}

/**Make an initial guess for a new engine**/
void pipeline_fp64::make_iguess(int rstatus) {
	printf("Making new initial guess...\n");
	fflush(stdout);

	if( rstatus==0 ) {
		this->conf_iguess( _config.engin_in.algorithm_rec, rstatus );
		initial_guess<double> IGUESS(_wconf,_rdata,_config.guess_in);
		IGUESS.set_initlist(true,true,true);
		IGUESS.make_initial_guess();

		this->_wconf = IGUESS._Wconf;
		this->_rdata = IGUESS._rdata;
	} else if( rstatus==1 ) {
		this->conf_iguess( _config.engin_in.algorithm_ref, rstatus );
		initial_guess<double> IGUESS(_wconf,_rdata,_config.guess_in);
		IGUESS.set_initlist(false,false,true);
		IGUESS.make_initial_guess();

		this->_wconf = IGUESS._Wconf;
		this->_rdata = IGUESS._rdata;
	} else if( rstatus==2 ) {
		this->conf_iguess( _config.engin_in.algorithm_rec, rstatus );
		initial_guess<double> IGUESS(_wconf,_rdata,_config.guess_in);
		IGUESS.set_initlist(false,true,false);
		IGUESS.make_initial_guess();

		this->_wconf = IGUESS._Wconf;
		this->_rdata = IGUESS._rdata;
	}

	printf("Done with initial guess\n");
	fflush(stdout);
}




