#include "preproc_data.hpp"



/**Based on the assigned lists this provides the batches
*   with the corresponding raw data.
*/
void preprocessor::set_batch_data() {
    for(int ss=0; ss<_Wconf._tot_nscans; ss++) {
        for(size_t ff=0; ff<_rdata._imag_data[ss].size(); ff++) {
            _WorkerData._imgdata.push_back(_rdata._imag_data[ss][ff]);
        }
    }

    for(int ss=0; ss<_Wconf._tot_nscans; ss++) {
        for(size_t ff=0; ff<_rdata._imag_data[ss].size(); ff++) {
            _WorkerData._cordinates_2D.push_back(_rdata._pos_data[ss][ff]);
        }
    }

    for(int ss=0; ss<_Wconf._tot_nscans; ss++) {
        _WorkerData._maskdata.push_back(_rdata._mask_data[ss]);
    }

    /**Immediately clean up old data!*/
    _rdata._pos_data.resize(0);
    _rdata._imag_data.resize(0);
    _rdata._mask_data.resize(0);
}


/**Assign the appropriate objects to each scan position
*   depending on the a parameter setting, note that the
*   indices are assigned within each batch.
*/
void preprocessor::set_objlist() {
    /**Reconstruct a single object from multiple scans**/
    if( _Pconf._do_share_object ) {
        for(int ss=0; ss<_Wconf._tot_nscans; ss++) {
            for(uint64_t ff=0; ff<_rdata._imag_data[ss].size(); ff++) {
                _WorkerData._objlist.push_back(0);
            }
        }
    } else {
        /**Separate object for each scan*/
        for(int ss=0; ss<_Wconf._tot_nscans; ss++) {
            for(uint64_t ff=0; ff<_rdata._imag_data[ss].size(); ff++) {
                _WorkerData._objlist.push_back(ss);
            }
        }
    }
}


/**Assign the appropriate probes to each scan position
*   depending on the a parameter setting, note that the
*   indices are assigned within each batch.
*/
void preprocessor::set_prolist() {
    /**Simultaneously reconstruct multiple scans with a shared probe**/
    if( _Pconf._do_share_probe ) {
        for(int ss=0; ss<_Wconf._tot_nscans; ss++) {
            for(uint64_t ff=0; ff<_rdata._imag_data[ss].size(); ff++) {
                _WorkerData._prolist.push_back(0);
            }
        }
    } else {
        /**Separate probe for each scan**/
        for(int ss=0; ss<_Wconf._tot_nscans; ss++) {
            for(uint64_t ff=0; ff<_rdata._imag_data[ss].size(); ff++) {
                _WorkerData._prolist.push_back(ss);
            }
        }
    }
}


/**Assign the appropriate mask to each scan position
*   depending on the a parameter setting
*/
void preprocessor::set_msklist() {
    /**There will be always a scan-specific mask for simplicity**/
    for(int64_t ss=0; ss<_Wconf._tot_nscans; ss++) {
        for(uint64_t ff=0; ff<_rdata._imag_data[ss].size(); ff++) {
            _WorkerData._msklist.push_back(ss);
        }
    }
}

/**Separates the loaded data to independent tasks
*   and provides them with their respective raw data.
*/
void preprocessor::create_lists() {
	printf("%s\tSetting object and probe lists...%s\n",A_C_CYAN,A_C_RESET);
	fflush(stdout);
	set_batch_data();
	set_objlist();
	set_prolist();
	set_msklist();
}


/**Prints diagnostic information about the preprocessed data.
*/
void preprocessor::print_diag() {
	if(_Pconf._diag_print_lists) {
		_WorkerData.print_lists();
	}
	if(_Pconf._diag_print_xypos) {
		_WorkerData.print_coordinates();
	}
	if(_Pconf._diag_print_ampli) {
		_WorkerData.print_amplitudes();
	}
}


/**Runs the raw data preprocessor by first separating
*   it to independent tasks and performs the processing
*   on a per-task basis.
*/
int preprocessor::run_data_preprocessor() {
	printf("%sPrefprocessing data...%s\n",A_C_MAGENTA,A_C_RESET);
	fflush(stdout);
	create_lists();

	/**Running pre-batch preprocessor tasks*/
	_WorkerData._Wconf=_Wconf;
	_WorkerData._batch_id=0;

	_WorkerData.assign_sizes();
	_WorkerData.assign_coordinates();
	_WorkerData.assign_amplitudes();
	_WorkerData.assign_masks();

	/**Print diagnostics about the preprocessed data if needed*/
	try {
		this->print_diag();
	} catch(exception& ex) {
		printf("%sERROR: an exception occurred in function preprocessor/print_diag(): %s%s\n",A_C_RED,ex.what(),A_C_RESET );
		fflush(stdout);
	}

	/**Exporting scan data to pointers for high-speed access during reconstruction**/
	try {
		_WorkerData.export_to_pointers();
	} catch(exception& ex) {
		printf("%sERROR: an exception occurred in function batch_scandata/export_to_pointers(): %s%s\n",A_C_RED,ex.what(),A_C_RESET );
		fflush(stdout);
	}
	printf("%s\t...done (preprocessor)%s\n",A_C_GREEN,A_C_RESET);
	fflush(stdout);

	return 0;
}


