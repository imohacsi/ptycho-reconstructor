#include "preproc_data.hpp"
#include <algorithm>


/**Assign the number of objects, probes, frames,
*   masks, etc. to the particular batch.
*/
void batch_scandata::assign_sizes() {
	_Wconf._num_frames=_imgdata.size();
	_Wconf._num_obj=*std::max_element(_objlist.begin(),_objlist.end())+1;
	_Wconf._num_pro=*std::max_element(_prolist.begin(),_prolist.end())+1;
	_Wconf._num_msk=_maskdata.size();

//	_Wconf._scan_id=_scan_id;
	_Wconf._batch_id=_batch_id;
	sprintf(_Wconf._batch_name,"%s_S%05d_B%d",_Wconf._run_name.c_str(),_Wconf._scan_id,_Wconf._batch_id);

    printf("%s\t%d frames in %d objects %d probes %d masks%s\n",A_C_BLUE,(int)_Wconf._num_frames, (int)_Wconf._num_obj, (int)_Wconf._num_pro, (int)_Wconf._num_msk,A_C_RESET);
    fflush(stdout);
}

/**Evaluate and assign the object size to the
*   particular batch.
*/
void batch_scandata::assign_coordinates() {
	std::vector<double> tmpvec= {0,0 };
	_pos2D.resize(_Wconf._num_frames,tmpvec);
	_pos1D.resize(_Wconf._num_frames,0);

	/**Calculating the lowest coordinate values*/
	double min_coordinate[2]= { _cordinates_2D[0][0], _cordinates_2D[0][1] };
	double max_coordinate[2]= { _cordinates_2D[0][0], _cordinates_2D[0][1] };
	for(int ii=0; ii<_Wconf._num_frames; ii++) {
		min_coordinate[0] = min( min_coordinate[0],_cordinates_2D[ii][0] );
		min_coordinate[1] = min( min_coordinate[1],_cordinates_2D[ii][1] );
		max_coordinate[0] = max( max_coordinate[0],_cordinates_2D[ii][0] );
		max_coordinate[1] = max( max_coordinate[1],_cordinates_2D[ii][1] );
	}

	/**Shifting positions and converting into pixel coordinates**/
	max_coordinate[0]=(max_coordinate[0]-min_coordinate[0])/_Wconf._dPX;
	max_coordinate[1]=(max_coordinate[1]-min_coordinate[1])/_Wconf._dPX;
	for(int ii=0; ii<_Wconf._num_frames; ii++) {
		_pos2D[ii][0]=(_cordinates_2D[ii][0]-min_coordinate[0])/_Wconf._dPX;
		_pos2D[ii][1]=(_cordinates_2D[ii][1]-min_coordinate[1])/_Wconf._dPX;
	}

	printf("\t\tActually mapped area: %0.2f x %0.2f px\n",max_coordinate[0],max_coordinate[1]);

	/**Adding reconstruction margin to the actually scanned area*/
	max_coordinate[0]+=1.0+double(_Wconf._num_pixels);
	max_coordinate[1]+=1.0+double(_Wconf._num_pixels);
	_Wconf._OX=64*ceil(max_coordinate[0]/64.0);
	_Wconf._OY=64*ceil(max_coordinate[1]/64.0);

	/**Assigning 1D coordinates*/
	for(int ff=0; ff<_Wconf._num_frames; ff++) {
		_pos1D[ff]=(int)((int)floor(_pos2D[ff][0]))+(int)((int)floor(_pos2D[ff][1]))*_Wconf._OX;
	}
}

/**Transform measured amplitudes into intensities
*   including the cosmetic filtering of already
*   masked bad pixels.
*/
void batch_scandata::assign_amplitudes() {
	int ff,xy;
	printf("%s\tConverting intensities to amplitudes...%s\n",A_C_BLUE,A_C_RESET);
	fflush(stdout);

	/**Set negative pixel values to zero*/
	#pragma omp parallel for private(xy)
	for(ff=0; ff<_Wconf._num_frames; ff++) {
		for(xy=0; xy<_Wconf._num_pixels*_Wconf._num_pixels; xy++) {
			_imgdata[ff][xy]-=0.0;
			if(_imgdata[ff][xy]<0.0f) {
				_imgdata[ff][xy]=0.0f;
			}
		}
	}

	/**Mask bad pixels according to the mask*/
	#pragma omp parallel for private(xy)
	for(ff=0; ff<_Wconf._num_frames; ff++) {
		for(xy=0; xy<_Wconf._num_pixels*_Wconf._num_pixels; xy++) {
			if(_maskdata[_msklist[ff]][xy]==0) {
				_imgdata[ff][xy]=0.0f;
			}
		}
	}


	/**Mask bad pixels according to the cout cutoff*/
	int64_t xx,yy,xypos;

	printf("%s\tMasking with count cutoff: %g...%s\n",A_C_YELLOW,_Wconf._cutoff,A_C_RESET);
	fflush(stdout);
	#pragma omp parallel for private(xx,yy,xypos)
	for(ff=0; ff<_Wconf._num_frames; ff++) {
		for(xx=1; xx<_Wconf._num_pixels-1; xx++) {
		for(yy=1; yy<_Wconf._num_pixels-1; yy++) {
			xypos = (xx+yy*_Wconf._num_pixels);
			if( _imgdata[ff][xypos]>_Wconf._cutoff ){
				//_imgdata[ff][xypos] = 0.0; 				
				_imgdata[ff][xypos]=0.25*(_imgdata[ff][xypos-1]+_imgdata[ff][xypos+1]+_imgdata[ff][xypos-_Wconf._num_pixels]+_imgdata[ff][xypos+_Wconf._num_pixels] );
			}
		}
		}
	}

	/**Calculating average number of counts per image*/
	double totint=0.0;
	for(ff=0; ff<_Wconf._num_frames; ff++) {
		for(xy=0; xy<_Wconf._num_pixels*_Wconf._num_pixels; xy++) {
			totint+=_imgdata[ff][xy];
		}
	}
	_Wconf._avg_counts=totint/double(_Wconf._num_frames);


	/**Convert intensities into amplitudes*/
	#pragma omp parallel for private(xy)
	for(ff=0; ff<_Wconf._num_frames; ff++) {
		for(xy=0; xy<_Wconf._num_pixels*_Wconf._num_pixels; xy++) {
			_imgdata[ff][xy]=sqrt(_imgdata[ff][xy]);
		}
	}

	#pragma omp parallel for
	for(ff=0; ff<_Wconf._num_frames; ff++) {
		fftshift(_imgdata[ff].data(),_Wconf._num_pixels,_Wconf._num_pixels);
	}
}


/**Transform loaded valid pixel mask with FFT shift.
*/
void batch_scandata::assign_masks() {
	for(int mm=0; mm<_Wconf._num_msk; mm++) {
		fftshift(_maskdata[mm].data(),_Wconf._num_pixels,_Wconf._num_pixels);
	}
}


/**Export to C style pointers instead of vectors
*   that are used by the following components of
*   the program (they are easier to pass around)
*/
int batch_scandata::export_to_pointers() {
	printf("%s\tExporting to pointers...%s\n",A_C_BLUE,A_C_RESET);
	fflush(stdout);
	/**Copying images...*/
	_edata.IMG = calloc2D<float>(_Wconf._num_frames,_Wconf._num_pixels*_Wconf._num_pixels);
	for(int64_t ff=0; ff<_Wconf._num_frames; ff++) {
		memcpy(_edata.IMG[ff],_imgdata[ff].data(),_Wconf._num_pixels*_Wconf._num_pixels*sizeof(float));
	}
	_imgdata.resize(0);


	/**Copying masks...**/

	_edata.MSK = calloc2D<int>(_Wconf._num_msk,_Wconf._num_pixels*_Wconf._num_pixels);
	for(int mm=0; mm<_Wconf._num_msk; mm++) {
		memcpy(_edata.MSK[mm],_maskdata[mm].data(),_Wconf._num_pixels*_Wconf._num_pixels*sizeof(int));
	}
	_maskdata.resize(0);

	/**Copying 1D coordinates and converting to int64_t...*/
	_edata._pos1D=calloc1D<int64_t>(_Wconf._num_frames);
	for(int ff=0; ff<_Wconf._num_frames; ff++) {
		_edata._pos1D[ff] = _pos1D[ff];
	}
	_pos1D.resize(0);

	/**Copying 2D coordinates...*/
	_edata._pos2D=(double**)calloc((int)_Wconf._num_frames,sizeof(double*));
	for(int64_t ff=0; ff<_Wconf._num_frames; ff++) {
		_edata._pos2D[ff]=(double*)calloc(2,sizeof(double));
		memcpy(_edata._pos2D[ff],_pos2D[ff].data(),2*sizeof(double));
	}
	_pos2D.resize(0);

	/**Copying lists...*/
	_edata._plist=(int*)calloc(_Wconf._num_frames,sizeof(int));
	_edata._olist=(int*)calloc(_Wconf._num_frames,sizeof(int));
	_edata._msklist=(int*)calloc(_Wconf._num_frames,sizeof(int));
	memcpy(_edata._plist,_prolist.data(),_Wconf._num_frames*sizeof(int));
	memcpy(_edata._olist,_objlist.data(),_Wconf._num_frames*sizeof(int));
	memcpy(_edata._msklist,_msklist.data(),_Wconf._num_frames*sizeof(int));

	return 0;
}


/**If debugging mode active, prints out the lists
*   for each scan position in the batch.
*/
void batch_scandata::print_lists() {
	printf("Batch\tFrame\tMask\tObj\tPro\n");
	for(int ff=0; ff<_Wconf._num_frames; ff++) {
		printf("%d\t%d\t%d\t%d\t%d\n",_batch_id,ff,_msklist[ff],_objlist[ff],_prolist[ff]);
	}
}


/**If debugging mode active, prints out the converted
*   scan positions for each point of the batch.
*/
void batch_scandata::print_coordinates() {
	printf("Batch\tFrame\tX\tY\t1D\n");
	for(int ff=0; ff<_Wconf._num_frames; ff++) {
		printf("%d\t%d\t%0.3f\t%0.3f\t%d\n",_batch_id,ff,_pos2D[ff][0],_pos2D[ff][1],_pos1D[ff]);
	}
}

/**If debugging mode active, writes out the converted
*   amplitudes in image files for each point of the batch.
*/
void batch_scandata::print_amplitudes() {
	char filename[512];
	for(int ff=0; ff<_Wconf._num_frames; ff++) {
		sprintf(filename,"./output/Amplitude_B%d_F%05d.tif",_batch_id,ff);
		WriteImOut(_imgdata[ff].data(),_Wconf._num_pixels,_Wconf._num_pixels,"lin","int","TIFF",filename);
	}
	/**Also write out the used masks*/
	for(int mm=0; mm<_Wconf._num_msk; mm++) {
		sprintf(filename,"./output/ValidPixelMask_B%d_N%05d.tif",_batch_id,mm);
		WriteImOut(_maskdata[mm].data(),_Wconf._num_pixels,_Wconf._num_pixels,"lin","int","TIFF",filename);
	}
}









