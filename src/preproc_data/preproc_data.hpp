#ifndef DESYPTYCHO_PREPROC_DATA_H_
#define DESYPTYCHO_PREPROC_DATA_H_

#include <iostream>
#include <string>
#include <vector>
#include <complex>
#include <omp.h>
#include "../imageprocessing/imageprocessing.h"
#include "../reconstruction/worker_config.hpp"
#include "../read_data/data_format.hpp"
#include "../utilities/fft_utils.hpp"

class pproc_format {
public:
	bool _do_share_object;  /**Reconstruct a common object from multiple scans*/
	bool _do_share_probe;  /**Share the same probe across multiple separate objects*/

    bool _diag_print_ampli;
    bool _diag_print_xypos;
    bool _diag_print_lists;
};

class batch_scandata {
  public:
    int _batch_id;
    int _scan_id;
    ptyconf   _Wconf;
    expdata   _edata;
    batch_scandata(ptyconf wconf): _Wconf(wconf) {};

    std::vector<std::vector<float>>                 _imgdata;
    std::vector<std::vector<int>>                   _maskdata;

    std::vector<int>                                _msklist;
    std::vector<int>                                _prolist;
    std::vector<int>                                _objlist;
    std::vector<int>                                _pos1D;
    std::vector<std::vector<double>>                _pos2D;         //Coordinates in 2D in units of pixels (including fractional part)
    std::vector<std::vector<double>>                _cordinates_2D; //Coordinates as read in from the position data file

    void assign_sizes();
    void assign_coordinates();
    void assign_amplitudes();
    void assign_masks();

    /**Diagnostics...*/
    void print_coordinates();
    void print_amplitudes();
    void print_lists();

    /**Export data to reconstruction module*/
    int export_to_pointers();

    /**Cleanup*/
    void clearmem();
};

class preprocessor {
  private:
    pproc_format    _Pconf;
    ptyconf         _Wconf;
    raw_data        _rdata;
    batch_scandata  _WorkerData;

    int _num_effscans;
    int _num_batch;

    std::vector<std::vector<int>>      _objsize;
    std::vector<int>                   _frame_per_scan;
    std::vector<int>                   _batch_per_scan;

    std::vector<int>                   _msklist;
    std::vector<int>                   _objlist;
    std::vector<int>                   _prolist;

    int  get_scanofbatch(int bb);
    void create_lists();
    void set_batch_data();
    void set_objlist();
    void set_prolist();
    void set_msklist();
    void print_diag();

  public:
    int run_data_preprocessor();

    ptyconf get_ptyconf(){ return _WorkerData._Wconf; }
    expdata get_expdata(){ return _WorkerData._edata; }

    /**Constructor*/
    preprocessor(pproc_format pconf, ptyconf wconf, raw_data rdata ): _Pconf(pconf),_Wconf(wconf),_rdata(rdata),_WorkerData(wconf) {};
};
#endif

