#include "read_position_data.hpp"


/**Reads the scan position data for each scan using P11's
*   new data format including interferometric data. Collects
*   positions for each scan and pushes it into the container.
*/
void meta_data_reader::P11NEW_HeaderOpen(scan_format scan_par ) {
	std::vector<std::vector<double>> scanbuffer;
	std::vector<double> tmpos(2);
	char MetaFileName[512];
	char IntrPath_x[512];
	char IntrPath_y[512];
	struct stat filestat;

	/**Print filename in cstring (c++ string formating sucks)*/
	//sprintf(MetaFileName,"%s/%s/%s_%04d/scan.h5",_conf._position_path.c_str(),_conf._position_prefix.c_str(),_conf._position_prefix.c_str(),_conf._scan_list[ss]._scan_numb);
	sprintf(MetaFileName,"%s/%s_%04d/%s_%04d.h5",_conf._meta_path.c_str(),_conf._meta_prefix.c_str(),scan_par._scan_numbr,_conf._data_prefix.c_str(),scan_par._scan_numbr);

	/**Check that the image file exists*/
	if( stat(MetaFileName,&filestat)==true ) {
		std::cout << std::endl << " ERROR: Position data file " << MetaFileName << " does not exist!" << std::endl;
		exit(EXIT_FAILURE);
	}

	/**Set the targeted sensors*/
	if(_conf._position_sensor=="ENC") {
		sprintf(IntrPath_x,"/data/x_enc");
		sprintf(IntrPath_y,"/data/y_enc");
	} else if(_conf._position_sensor=="IFM") {
		sprintf(IntrPath_x,"/data/x_imf");
		sprintf(IntrPath_y,"/data/y_imf");
	}

	/**Finally open the HDF5 file...*/
	hid_t file_id=H5Fopen(MetaFileName,H5F_ACC_RDONLY, H5P_DEFAULT);

	hsize_t dims[2];
	H5LTget_dataset_info(file_id,IntrPath_x,dims,NULL,NULL);
	//std::cout << "Position data file dimensions: " << dims[0] << " " << dims[1] << " = " << dims[0]*dims[1] << " frames" << std::endl;

	float* rawpdata_x= new float[dims[0]*dims[1]];
	float* rawpdata_y= new float[dims[0]*dims[1]];
	H5LTread_dataset_float(file_id,IntrPath_x,rawpdata_x);
	H5LTread_dataset_float(file_id,IntrPath_y,rawpdata_y);

	int istop  = scan_par._stop_frame;
	int istart = scan_par._strt_frame;
	for(size_t xx=0; xx<dims[1]; xx++) {
		for(size_t yy=0; yy<dims[0]; yy++) {
			int64_t num_frame=yy+xx*dims[0];
			int64_t idx_frame=xx+yy*dims[1];
			if(num_frame>=istart && num_frame<istop) {
				tmpos[0]=1e-6*rawpdata_x[idx_frame];
				tmpos[1]=1e-6*rawpdata_y[idx_frame];
				scanbuffer.push_back(tmpos);
			}
		}
	}


	_pos_data.push_back(scanbuffer);
	delete[] rawpdata_x;
	delete[] rawpdata_y;
	H5Fclose(file_id);
}



/**Reads the scan position data for each scan using the proper CXI data format.
*   Collects positions for each scanpoint and pushes it into the container.
*/
void meta_data_reader::CXI_HeaderOpen(scan_format scan_par) {
	char MetaFileName[512];
	char IntrPath_x[512];
	char IntrPath_y[512];
	char IntrPath_xy[512];
	/**HDF5 identifiers and index variables**/
	hid_t       file_id;
	struct stat filestat;

	std::vector<std::vector<double>> scanbuffer;
	std::vector<double> tmpos(2);

	sprintf( IntrPath_x, "/entry_1/instrument_1/motor_positions/IFMX" );
	sprintf( IntrPath_y, "/entry_1/instrument_1/motor_positions/IFMY" );
	sprintf( IntrPath_xy, "/entry_1/data_1/translation" );

	/**Print filename in cstring (c++ string formating sucks)*/
	sprintf(MetaFileName,"%s/%s_%04d.cxi",_conf._meta_path.c_str(),_conf._meta_prefix.c_str(),scan_par._scan_numbr);

	/**Check that the image file exists*/
	if( stat(MetaFileName,&filestat)==true ) {
		fprintf(stderr, "%sERROR: Problem with reading position data from file: %s%s\n",A_C_RED,MetaFileName,A_C_RESET);
		exit(EXIT_FAILURE);
	}

	/**Open position data file and position data**/
	file_id=H5Fopen(MetaFileName,H5F_ACC_RDONLY, H5P_DEFAULT);


	if( H5Lexists(file_id, IntrPath_xy, H5P_DEFAULT) ) {
		/**Get dataset dimensions*/
		hsize_t     dims[2];
		H5LTget_dataset_info(file_id,IntrPath_xy,dims,NULL,NULL);
//        std::cout << "Position data dimensions from CXI file: " << dims[0] << " " << dims[1] << " = " << dims[0]*dims[1] << " frames" << std::endl;

		/**Allocate temporary storage**/
		double* rawpdata_xy= new double[ dims[0]*dims[1] ];
		int istop=min(int(dims[0]),scan_par._stop_frame);
		int istart = scan_par._strt_frame;

		/**Read the dataset**/
		try {
			H5LTread_dataset_double(file_id,IntrPath_xy,rawpdata_xy);
		} catch(exception& ex) {
			fprintf(stderr, "%sERROR: Problem with reading position data from file: %s %s\n",A_C_RED,MetaFileName,A_C_RESET);
			exit(EXIT_FAILURE);
		}

		/**Select relevant axes CXI here!!!**/
		for(int64_t ff=istart; ff<istop; ff++) {
//			tmpos[0]=-rawpdata_xy[0+ff*dims[1]]; //for mll
			tmpos[0]=rawpdata_xy[0+ff*dims[1]];
			tmpos[1]=rawpdata_xy[1+ff*dims[1]];
			scanbuffer.push_back(tmpos);
		}
		delete[] rawpdata_xy;

	} else {
		/**Get dataset dimensions*/
		hsize_t     dims[1];
		H5LTget_dataset_info(file_id,IntrPath_x,dims,NULL,NULL);
        	std::cout << "Position data dimensions from CXI file: " << dims[0] << " frames" << std::endl;

		/**Allocate temporary storage**/
		float* rawpdata_x= new float[dims[0]];
		float* rawpdata_y= new float[dims[0]];
		int istop=min(int(dims[0]),scan_par._stop_frame);
		int istart = scan_par._strt_frame;

		/**Read the dataset**/
		try {
			H5LTread_dataset_float(file_id,IntrPath_x,rawpdata_x);
			H5LTread_dataset_float(file_id,IntrPath_y,rawpdata_y);
		} catch(exception& ex) {
			fprintf(stderr, "%sERROR: Problem with reading position data from file %s !\n%s",A_C_RED,MetaFileName,A_C_RESET);
			exit(EXIT_FAILURE);
		}

		/**Select relevant axes**/
		for(ssize_t ff=istart; ff<istop; ff++) {
			tmpos[0]=-rawpdata_x[ff];
			tmpos[1]=-rawpdata_y[ff];
			scanbuffer.push_back(tmpos);
		}
		delete[] rawpdata_x;
		delete[] rawpdata_y;
	}

	H5Fclose(file_id);
	_pos_data.push_back(scanbuffer);

}




void meta_data_reader::LoadScan(int ss) {
	if(_conf._position_format=="P11NEW") {
		P11NEW_HeaderOpen(_conf._scan_list[ss]);
	}
	if(_conf._position_format=="CXI") {
		CXI_HeaderOpen(_conf._scan_list[ss]);
	}

}

/**Reads the scan position data for each scan from the
*   corresponding files. It is aimed to provide a uniform
*   output format from multiple beamline conventions.
*   To-do: add additional beamline conventions.
*/
void meta_data_reader::load_position_data() {
	printf("%sReading position data...%s\n",A_C_CYAN,A_C_RESET);
	fflush(stdout);

	for(int ss=0; ss<_conf._num_scans; ss++) {
		LoadScan(ss);
	}

	if(_conf._diag_print_meta) {
		print_position_data();
	}
	printf("%s\tDone with loading position data.%s\n",A_C_GREEN,A_C_RESET);
	fflush(stdout);
}


std::vector<std::vector<std::vector<double>>> meta_data_reader::get_position_data() {
	return _pos_data;
}


/**If debugging mode active, prints the read position
*   values for each scan and scanpoint.
*/
void meta_data_reader::print_position_data() {
	std::cout << " Inside printing metadata " << _pos_data.size() << std::endl;
	char filename[512];
	FILE *fp;

	for(size_t ss=0; ss<_pos_data.size(); ss++) {
		sprintf(filename,"output/posdata_s%d.dat",int(ss) );
		fp = fopen(filename, "w");
		for(size_t ff=0; ff<_pos_data[ss].size(); ff++) {
			fprintf(fp, "%d\t%0.3f\t%0.3f\n",int(ff),1e6*_pos_data[ss][ff][0],1e6*_pos_data[ss][ff][1]);
		}
		fclose(fp);
	}
}
