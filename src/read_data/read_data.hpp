#ifndef DESYPTYCHO_READ_DATA_H_
#define DESYPTYCHO_READ_DATA_H_
#include <iostream>
#include <string>
#include <vector>
#include "../preproc_data/preproc_data.hpp"
#include "data_format.hpp"
#include "read_mask_data.hpp"
#include "read_image_data.hpp"
#include "read_position_data.hpp"

/**Loads raw input data from files into memory.
*   Separate modules for loading image data, position data and masks.
*/
class read_data {
  protected:
    /**Configurations**/
    data_format _conf;
    ptyconf     _wconf;
    /**Main data buffer**/
    raw_data _data;

    /**These are the loader modules*/
    void Load_Imag_Data();
    void Load_Mask_Data();
    void Load_Meta_Data();
    void Load_Scan_Numb();
    void print_meminfo();

  public:
    /**Constructor**/
    read_data( data_format conf, ptyconf wconf ): _conf(conf),_wconf(wconf) {};
	/**Executor**/
    void run_read_data();
    /**Retrieving data**/
    raw_data get_data(){ return _data; }
};
#endif //DESYPTYCHO_READ_DATA_H_
