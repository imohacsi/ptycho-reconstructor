#include "read_image_data.hpp"
#include "./DectrisRead/lz4.h"



/**Loads the image data from TIFF files into the container.
*   Currently using P11's naming format, other styles can
*   be added later to the naming convention.
*   @param scan_par contains the scan number, number of
*   frames and the center coordinates
*/
void imag_data_reader::ScanLoad_TIFF(scan_format scan_par,int ss) {
	std::vector<std::vector<float>> read_scan;
	std::cout << "... TIFF from scan number: " << scan_par._scan_numbr << std::endl;

	char ImageName[512];
	struct stat filestat;
	#pragma omp parallel for private(ImageName,filestat)
	for(int ff=scan_par._strt_frame; ff<scan_par._stop_frame; ff++) {
		printf("\r\tReading image %i...",ff);
		fflush(stdout);

		/**Print filename in csting (c++ sucks in formating)*/
		sprintf(ImageName,"%s/scan_%04d/scan_%04d_%05d.tif",_conf._data_path.c_str(),scan_par._scan_numbr,scan_par._scan_numbr,ff);

		/**Check that the image file exists*/
		if( stat(ImageName,&filestat)==0 ) {
			/**Read in the actual image*/
			read_scan.push_back(imload_tiff(ImageName,scan_par._x_center,scan_par._y_center));
		} else {
			std::cout << std::endl << " ERROR: Image file no " << ImageName << " does not exists" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	_img_data.push_back(read_scan);
	read_scan.resize(0);
}

/**Actually reads the diffraction pattern from a TIFF file
*   and selects the region of interest as defined in the
*   configuration parameters.
*   Also performs the binning to keep memory usage at bay
*   @param filename : Image filename of the tiff file
*   @param cen_x and cen_y : Center pixel coordinates for
*   ROI selection.
*   @return recorded : The cut and binned diffraction
*   pattern in float format for lower memory consumption.
*/
std::vector<float> imag_data_reader::imload_tiff(char *filename, const int cen_x, const int cen_y) {
	const int Npx=_conf._num_pixels;
	const int bin=_conf._image_binvalue;
	int Nx,Ny;
	int xx,yy,bx,by;
	double *Map=nullptr;
	std::vector<float> recorded(Npx*Npx,0.0f);

	/**First load the image file*/
	Map=read_tiff(Nx,Ny,filename);
	/**Check if image coordinates are within bounds*/
	if( 0>((-Npx/2*bin)+cen_x) || Nx<((Npx/2*bin)+cen_x) ) {
		std::cout <<std::endl << "ERROR: image coordinates out of bounds in X" << std::endl;
		exit(EXIT_FAILURE);
	}
	if( 0>((-Npx/2*bin)+cen_y) || Ny<((Npx/2*bin)+cen_y) ) {
		std::cout <<std::endl << "ERROR: image coordinates out of bounds in Y" << std::endl;
		exit(EXIT_FAILURE);
	}

	/**Cut the region of interest from the recorded image*/
	for(yy=-Npx/2; yy<Npx/2; yy++) {
		for(xx=-Npx/2; xx<Npx/2; xx++) {
			for(by=0; by<bin; by++) {
				for(bx=0; bx<bin; bx++) {
					recorded[xx+Npx/2+(yy+Npx/2)*Npx]+=(float)Map[(bx+xx*bin)+cen_x+((by+yy*bin)+cen_y)*Nx];
				}
			}
		}
	}
	/**Free temporary buffer*/
	delete[] Map;

	/**Return the diffraction pattern*/
	return recorded;
}



/**Loads the image data from CBF files into the container.
*   Currently using P11's naming format, other styles can
*   be added later to the naming convention. This version
*   does not need to know the total number of scan points
*   or the length of the lines within the scan. (SLOW)
*   @param scan_par contains the scan number, number of
*   frames and the center coordinates
*/
void imag_data_reader::ScanLoad_CBF(scan_format scan_par,int ss) {
	std::vector<std::vector<float>> read_scan;
	char ImageName[512];
	struct stat filestat;

	int row_idx,col_idx;
	int num_frame=0;

	col_idx=0;
	do {
		row_idx=0;
		do {
			printf("\r\tReading image %i...",num_frame);
			fflush(stdout);
			/**Print filename in csting (c++ sucks in formating)*/
			sprintf(ImageName,"%s/%s_%04d/%s_%04d_%04d_%05d.cbf",_conf._data_path.c_str(),_conf._data_prefix.c_str(),scan_par._scan_numbr,_conf._data_prefix.c_str(),scan_par._scan_numbr,col_idx,row_idx);
			/**Check that the image file exists*/
			if( stat(ImageName,&filestat)==0 ) {
				/**Read in the actual image*/
				if(num_frame>=scan_par._strt_frame) {
					read_scan.push_back(imload_cbf(ImageName,scan_par._x_center,scan_par._y_center));
				}
				num_frame++;
				row_idx++;
			} else {
				break;
			}
		} while(num_frame<scan_par._stop_frame);
		if(row_idx==0) {
			break;
		} else {
			col_idx++;
		}
	} while(num_frame<scan_par._stop_frame);


	if( num_frame==0) {
		sprintf(ImageName,"%s/%s_%04d",_conf._data_path.c_str(),_conf._data_prefix.c_str(),scan_par._scan_numbr );
		printf("\n%sERROR: Unable to load CBF data from folder: %s %s\n",A_C_RED,ImageName, A_C_RESET);
		fflush(stdout);
		exit(EXIT_FAILURE);
	}

	printf("\r\tRead %d images...\n",num_frame);
	fflush(stdout);

	_img_data.push_back(read_scan);
	read_scan.resize(0);
}


/**Loads the image data from CBF files into the container.
*   Currently using P11's naming format, other styles can
*   be added later to the naming convention. This version
*   NEEDS TO KNOW the total number of scan points. (FAST)
*   @param scan_par contains the scan number, number of
*   frames and the center coordinates
*/
void imag_data_reader::ScanLoad_CBF_fast(scan_format scan_par,int ss) {
	std::vector<std::vector<float>> read_scan(scan_par._stop_frame-scan_par._strt_frame);

	char ImageName[512];
	struct stat filestat;
	int ll,pp;
	int num_frame=0;

	int strt_line=scan_par._strt_frame/_conf._frame_per_line;
	int stop_line=int(ceil(double(scan_par._stop_frame)/double(_conf._frame_per_line)));


	#pragma omp parallel for private(ImageName,filestat,num_frame,pp)
	for(ll=strt_line; ll<stop_line; ll++) {
		for(pp=0; pp<_conf._frame_per_line; pp++) {
			num_frame=pp+ll*_conf._frame_per_line;
			printf("\r\tReading image %i...",num_frame);
			fflush(stdout);

			if(num_frame>=scan_par._strt_frame && num_frame<scan_par._stop_frame) {
				/**Print filename in csting (c++ sucks in formating)*/
				sprintf(ImageName,"%s/%s_%04d/%s_%04d_%04d_%05d.cbf",_conf._data_path.c_str(),_conf._data_prefix.c_str(),scan_par._scan_numbr,_conf._data_prefix.c_str(),scan_par._scan_numbr,ll,pp);
				/**Check that the image file exists*/
				if( stat(ImageName,&filestat)==0 ) {
					/**Read in the actual image*/
					read_scan[num_frame-scan_par._strt_frame]=imload_cbf(ImageName,scan_par._x_center,scan_par._y_center);
				} else {
					std::cout << std::endl << " ERROR: Image file " << ImageName << " does not exists" << std::endl;
					exit(EXIT_FAILURE);
				}
			}
		}
	}

	printf("\r\tRead %d images...\n",num_frame);
	_img_data.push_back(read_scan);
	read_scan.resize(0);
}

/**Actually reads the diffraction pattern from a CBF file
*   and selects the region of interest as defined in the
*   configuration parameters.
*   Also performs the binning to keep memory usage at bay
*   @param filename : Image filename of the tiff file
*   @param cen_x and cen_y : Center pixel coordinates for
*   ROI selection.
*   @return recorded : The cut and binned diffraction
*   pattern in float format for lower memory consumption.
*/
std::vector<float> imag_data_reader::imload_cbf(char *filename, const int cen_x, const int cen_y) {
	const int Npx=_conf._num_pixels;
	const int bin=_conf._image_binvalue;
	int Nx,Ny;
	int xx,yy,bx,by;
	double *Map=nullptr;
	std::vector<float> recorded(Npx*Npx,0.0f);
	int cutoff;

	/**First load the image file*/
	Map=ReadCBF(Nx,Ny,cutoff,filename);
	/**Check if image coordinates are within bounds*/
	if( 0>((-Npx/2*bin)+cen_x) || Nx<((Npx/2*bin)+cen_x) ) {
		std::cout <<std::endl << "ERROR: image coordinates out of bounds in X" << std::endl;
		exit(EXIT_FAILURE);
	}
	if( 0>((-Npx/2*bin)+cen_y) || Ny<((Npx/2*bin)+cen_y) ) {
		std::cout <<std::endl << "ERROR: image coordinates out of bounds in Y" << std::endl;
		exit(EXIT_FAILURE);
	}

	/**Cut the region of interest from the recorded image*/
	for(yy=-Npx/2; yy<Npx/2; yy++) {
		for(xx=-Npx/2; xx<Npx/2; xx++) {
			for(by=0; by<bin; by++) {
				for(bx=0; bx<bin; bx++) {
					recorded[xx+Npx/2+(yy+Npx/2)*Npx]+=(float)Map[(bx+xx*bin)+cen_x+((by+yy*bin)+cen_y)*Nx];
				}
			}
		}
	}
	/**Free temporary buffer*/
	delete[] Map;

	CountLimit=sqrt(double(cutoff));
	/**Return the diffraction pattern*/
	return recorded;
}



/**Actually reads the diffraction pattern from an HDF5 file
*   and selects the region of interest as defined in the
*   configuration parameters.
*   Also performs the binning to keep memory usage at bay
*   @param scan_par : Container for the most important scan
    parameters: number, and center coordinates.
*   @return recorded : The cut and binned diffraction
*   patterns of the whole scan.
*/
void imag_data_reader::ScanLoad_HDF5(scan_format scan_par,int ss) {
	const int Npx=_conf._num_pixels;
	const int bin=_conf._image_binvalue;
	const int cen_x=scan_par._x_center;
	const int cen_y=scan_par._y_center;
	int xx,yy,bx,by;
	std::vector<std::vector<float>> read_scan;
	std::vector<float> recorded(Npx*Npx,0.0f);

	std::cout << "Scan number: " << scan_par._scan_numbr << std::endl;

	/**Print filename in cstring (c++ string formating sucks)
	    and check if the file really exists!*/
	char ImageName[512];
	struct stat filestat;
//	sprintf(ImageName,"%s/scan_%04d/scan_%d_master.h5",_conf._image_path.c_str(),scan_par._scan_numb,scan_par._scan_numb);
	sprintf(ImageName,"%s",_conf._data_path.c_str());
	if( stat(ImageName,&filestat)!=0 ) {
		std::cout << std::endl << " ERROR: Image data file " << ImageName << " does not exist!" << std::endl;
		exit(EXIT_FAILURE);
	}


	hid_t       file_id,dataset_id, dataspace_id;  /* identifiers */
	/**Register custom compression algorithm*/
	H5Zregister(H5Z_LZ4);
	/**Open image datafile*/
	file_id=H5Fopen(ImageName, H5F_ACC_RDONLY, H5P_DEFAULT);

	/**Exposure time is always nice to check*/
	double exptime;
	dataset_id = H5Dopen2(file_id, "/entry/instrument/detector/count_time", H5P_DEFAULT);
	H5Dread (dataset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,&exptime );
	int bitdepth;
	dataset_id = H5Dopen2(file_id, "/entry/instrument/detector/bit_depth_image", H5P_DEFAULT);
	H5Dread (dataset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,&bitdepth );
	std::cout<<" Exposure time: "<< exptime << " Bit depth: " << bitdepth <<std::endl;

	/**Getting sensor size*/
	int sizeX,sizeY;
	dataset_id = H5Dopen2(file_id, "/entry/instrument/detector/detectorSpecific/x_pixels_in_detector", H5P_DEFAULT);
	H5Dread (dataset_id,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,&sizeX );
	dataset_id = H5Dopen2(file_id, "/entry/instrument/detector/detectorSpecific/y_pixels_in_detector", H5P_DEFAULT);
	H5Dread (dataset_id,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,&sizeY );

	/**Check if image coordinates are within bounds*/
	if( 0>((-Npx/2*bin)+cen_x) || sizeX<((Npx/2*bin)+cen_x) ) {
		std::cout <<std::endl << "ERROR: image coordinates out of bounds in X" << std::endl;
		exit(EXIT_FAILURE);
	}
	if( 0>((-Npx/2*bin)+cen_y) || sizeY<((Npx/2*bin)+cen_y) ) {
		std::cout <<std::endl << "ERROR: image coordinates out of bounds in Y" << std::endl;
		exit(EXIT_FAILURE);
	}


	/**Count the number of image data blocks inside the file*/
	char ipath[512];
	bool status=true;
	int block_counter=1;
	while(status) {
		sprintf(ipath,"/entry/data/data_%06d",block_counter);
		htri_t exists = H5Lexists(file_id, ipath, H5P_DEFAULT);
		if(!exists) {
			status=false;
		}
		if(exists) {
			block_counter++;
		}
	}

	std::cout << "Found " << block_counter << " datablocks" << std::endl;

	int num_frames=0;
	/**Read in image data from the data file*/
	for(int bl=0; bl<block_counter-1; bl++) {
		sprintf(ipath,"/entry/data/data_%06d",bl+1);

		/**Get dataset dimensions*/
		dataset_id= H5Dopen(file_id,ipath,H5P_DEFAULT);
		dataspace_id=H5Dget_space(dataset_id);
		int ndims=H5Sget_simple_extent_ndims(dataspace_id);
		hsize_t     dims[ndims];
		H5Sget_simple_extent_dims(dataspace_id, dims, NULL);

		num_frames+=dims[0];
		int* rawidata= new int[dims[0]*dims[1]*dims[2]];
		dataset_id= H5Dopen(file_id,ipath,H5P_DEFAULT);
		H5Dread(dataset_id,H5T_NATIVE_UINT32,0,H5S_ALL,H5P_DEFAULT,rawidata);
		H5Dclose(dataset_id);

//		std::cout << "Dataset " << bl+1 << " dimensions: " << dims[0] << " " << dims[1] << " " << dims[2] << std::endl;

		for(size_t ff=0; ff<dims[0]; ff++) {
			/**Reset all values to zero*/
			memset(recorded.data(),0,recorded.size()*sizeof(float));
			/**Cut the region of interest from the recorded image*/
			#pragma omp parallel for private(xx,yy,bx,by)
			for(yy=-Npx/2; yy<Npx/2; yy++) {
				for(xx=-Npx/2; xx<Npx/2; xx++) {
					for(by=0; by<bin; by++) {
						for(bx=0; bx<bin; bx++) {
							recorded[xx+Npx/2+(yy+Npx/2)*Npx]+=(float)rawidata[(bx+xx*bin)+cen_x+((by+yy*bin)+cen_y)*sizeX+ff*dims[1]*dims[2]];
						}
					}
				}
			}
			read_scan.push_back(recorded);
		}
		delete[] rawidata;
		fflush(stdout);
		if(num_frames>scan_par._stop_frame) {
			break;
		}
	}
	printf("\tRead %d frames...\n",num_frames);
	H5Fclose(file_id);
	_img_data.push_back(read_scan);
	read_scan.resize(0);
}



/**Actually reads the diffraction pattern from a CXI file
*   and selects the region of interest as defined in the
*   configuration parameters.
*   Also performs the binning to keep memory usage at bay
*   @param scan_par : Container for the most important scan
    parameters: number, and center coordinates.
*   @return recorded : The cut and binned diffraction
*   patterns of the whole scan.
*/
void imag_data_reader::ScanLoad_CXI(scan_format scan_par,int ss) {
	const int64_t Npx=_conf._num_pixels;
	const int64_t bin=_conf._image_binvalue;
	const int64_t cen_x=scan_par._x_center;
	const int64_t cen_y=scan_par._y_center;
	int64_t xx,yy,bx,by;
	std::vector<std::vector<float>> read_scan;
	std::vector<float> recorded(Npx*Npx,0.0f);

	/**Print filename in cstring and check if the file really exists!*/
	char ImageName[512];
	sprintf(ImageName,"%s/%s_%04d.cxi",_conf._data_path.c_str(),_conf._data_prefix.c_str(),_conf._scan_list[ss]._scan_numbr);
	try {
		checkFileExist( ImageName );
	} catch(exception& ex) {
		exit(EXIT_FAILURE);
	}

	hid_t       file_id,dataset_id, dataspace_id,mspace_id;  /* identifiers */
	/**Open image datafile*/
	file_id=H5Fopen(ImageName, H5F_ACC_RDONLY, H5P_DEFAULT);

	dataset_id = H5Dopen(file_id, "/entry_1/data_1/data", H5P_DEFAULT);
	dataspace_id=H5Dget_space(dataset_id);

	hsize_t     dims[3];
	H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
	hsize_t start[3]= {0,0,0};
	hsize_t cell[3]= {1,dims[1],dims[2]};

	/**Check if image coordinates are within bounds*/
	if( 0>((-Npx/2*bin)+cen_x) || (int64_t)dims[2]<((Npx/2*bin)+cen_x) ) {
		std::cout <<std::endl << "ERROR: image coordinates out of bounds in X" << std::endl;
		exit(EXIT_FAILURE);
	}
	if( 0>((-Npx/2*bin)+cen_y) || (int64_t)dims[1]<((Npx/2*bin)+cen_y) ) {
		std::cout <<std::endl << "ERROR: image coordinates out of bounds in Y" << std::endl;
		exit(EXIT_FAILURE);
	}


//            std::cout << "CXI dataset dimensions are: " << dims[0] << "x" << dims[1] << "x" << dims[2] <<std::endl;
//        std::cout << "Center coordinates are: " << cen_x << "\t" << cen_y <<std::endl;

	int* rawidata= new int[dims[1]*dims[2]];
	hsize_t mem_dims[1]= {dims[1]*dims[2]};
	mspace_id = H5Screate_simple(1, mem_dims, NULL);
	int stopframe=min(int(dims[0]),int(scan_par._stop_frame));

	for(ssize_t ff=scan_par._strt_frame; ff<stopframe; ff++) {
		/**Reset all values to zero*/
		memset(recorded.data(),0,recorded.size()*sizeof(float));
		start[0]=ff;

		/**Reading the data block*/
		H5Sselect_hyperslab(dataspace_id,H5S_SELECT_SET,start,NULL,cell,NULL);
		H5Dread (dataset_id, H5T_NATIVE_INT, mspace_id, dataspace_id, H5P_DEFAULT,rawidata);
//        if(ff%100 ==0){
//        printf("NI %d\n",ff); fflush(stdout); }

		/**Cut the region of interest from the recorded image*/
		#pragma omp parallel for private(xx,yy,bx,by)
		for(yy=-Npx/2; yy<Npx/2; yy++) {
			for(xx=-Npx/2; xx<Npx/2; xx++) {
				for(by=0; by<bin; by++) {
					for(bx=0; bx<bin; bx++) {
						recorded[xx+Npx/2+(yy+Npx/2)*Npx]+=(float)rawidata[(bx+xx*bin)+cen_x+((by+yy*bin)+cen_y)*dims[2]];
					}
				}
			}
		}
		read_scan.push_back(recorded);
	}
	delete[] rawidata;

	printf("\tRead %d frames...\n",int(read_scan.size()));
	fflush(stdout);
	H5Dclose(dataset_id);
	H5Fclose(file_id);
	_img_data.push_back(read_scan);
	read_scan.resize(0);

	CountLimit=6e23;
}


/**Feeds the _scan_list one-by one to the actual diffraction
*   data loader and selects the file format.
*   To-do: add additional input formats
*/
void imag_data_reader::load_image_data() {
    printf("%sReading image data from %s%s\n", A_C_CYAN, _conf._image_format.c_str(), A_C_RESET);
    for(size_t ss=0; ss<_conf._scan_list.size(); ss++) {
        printf("%s\tReading scan %d with number: %d%s\n",A_C_BLUE,(int)ss,(int)_conf._scan_list[ss]._scan_numbr,A_C_RESET);
        fflush(stdout);

        if(_conf._image_format=="TIFF") {           ScanLoad_TIFF(_conf._scan_list[ss],ss);     }
        else if(_conf._image_format=="CBF") {       ScanLoad_CBF(_conf._scan_list[ss],ss);      }
        else if(_conf._image_format=="CBF_FAST") {  ScanLoad_CBF_fast(_conf._scan_list[ss],ss); }
        else if(_conf._image_format=="HDF5") {           ScanLoad_HDF5(_conf._scan_list[ss],ss);     }
        else if(_conf._image_format=="CXI") {            ScanLoad_CXI(_conf._scan_list[ss],ss);      }
        else{ printf("%s\tERROR: unknown image data format.%s\n",A_C_RED,A_C_RESET); fflush(stdout); exit(-1); }
    }

    if(_conf._diag_print_imag) {
        print_image_data();
    }
    printf("%s\tDone with loading image data.%s\n",A_C_GREEN,A_C_RESET);
    fflush(stdout);
}



std::vector<std::vector<std::vector<float>>> imag_data_reader::get_image_data() {
	return _img_data;
}

/**If debugging mode active, writes out the read
*   diffraction patterns in image files for each
*   scan and scanpoint.
*/
void imag_data_reader::print_image_data() {
	char filename[512];
	for(size_t ss=0; ss<_img_data.size(); ss++) {
		for(size_t ff=0; ff<_img_data[ss].size(); ff++) {
			sprintf(filename,"./output/Recorded_S%05d_F%05d.tif",int(ss),int(ff));
			WriteImOut(_img_data[ss][ff].data(),_conf._num_pixels,_conf._num_pixels,"lin","int","TIFF",filename);
		}
	}
}
