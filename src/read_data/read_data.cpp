#include <fstream>
#include "read_data.hpp"


void read_data::run_read_data(){
    /**Data reading module - independent from reconstruction datatype*/
    printf("%sReading data from filesystem...%s\n", A_C_MAGENTA, A_C_RESET);
    Load_Imag_Data();
    Load_Meta_Data();
    Load_Mask_Data();
    Load_Scan_Numb();
    print_meminfo();
}



/**This is a function that conveniently calls
*   other functions to load the valid pixel mask
*   of the detector into the memory.
*   Also performs the binning operation.
*   Uses only class member variables.
*/
void read_data::Load_Mask_Data() {
    mask_data_reader MaskReader(_conf);
    MaskReader.load_mask_data();
    _data._mask_data=MaskReader.get_mask_data();
}

/**This is a function that conveniently calls other
*   functions to load the recorded diffraction
*   patterns into the memory. Can provide data from
*   multiple image formats (TIFF) in a unified format.
*   Also performs the binning operation.
*   Uses only class member variables.
    */
void read_data::Load_Imag_Data() {
    imag_data_reader ImageReader(_conf);
    ImageReader.load_image_data();
    _data._imag_data = ImageReader.get_image_data();
    _data._countcutoff = ImageReader.CountLimit;
}

/**This is a function that conveniently calls
*   other functions to load the position values
*   for the individual scan points into the memory.
*   Supports multiple beamline conventions (P11).
*   Uses only class member variables.
*/
void read_data::Load_Meta_Data() {
    meta_data_reader PositionReader(_conf);
    PositionReader.load_position_data();
    _data._pos_data=PositionReader.get_position_data();
}

/**This function just prints the memory footprint
*   of the raw data to give a first estimeate of
*   what will come...
*/
void read_data::print_meminfo() {
    int tot_nscans=0;
    for(size_t ss=0; ss<_data._imag_data.size(); ss++) {
        tot_nscans+=_data._imag_data[ss].size();
    }
    double memory=double(tot_nscans+_data._imag_data.size())*double(_conf._num_pixels*_conf._num_pixels)*4.0;

    printf("%sReserved memory for the raw data alone: %.3f MB%s\n",A_C_YELLOW,memory/(1048576.0),A_C_RESET);
    fflush(stdout);
}

/**Loads the scan numbers**/
void read_data::Load_Scan_Numb(){
    std::vector<int> scan_num;
    for(size_t ss=0; ss<_conf._scan_list.size(); ss++) {
        scan_num.push_back(_conf._scan_list[ss]._scan_numbr);
    }
    _data._scan_num = scan_num;
}

