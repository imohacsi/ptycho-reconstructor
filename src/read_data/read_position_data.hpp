#ifndef DESYPTYCHO_LOAD_POSITIONDATA_H_
#define DESYPTYCHO_LOAD_POSITIONDATA_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <hdf5.h>
#include <hdf5_hl.h>
#include <sys/stat.h>
#include "../imageprocessing/imageprocessing.h"
#include "data_format.hpp"
#include "../utilities/colorconsole.hpp"


class stage_data {
public:
	double _reqx;  //Set positions
	double _reqy;  //Set positions
	double _encx;  //Actual positions
	double _ency;  //Actual positions
	double _ifmx;  //Interferometric positions
	double _ifmy;  //Interferometric positions

};


/**
* Class collecting procedures for the reading of diffraction patterns
*/
class meta_data_reader {
private:
	data_format _conf;
	std::vector<std::vector<std::vector<double>>> _pos_data;

	void P11NEW_HeaderOpen(scan_format);
	void CXI_HeaderOpen(scan_format);
	void LoadScan(int);

	void print_position_data();


public:
	std::vector<std::vector<std::vector<double>>> get_position_data();
	void load_position_data();
	meta_data_reader(data_format conf): _conf(conf) {};
//    ~meta_data_reader() {};
};









#endif
