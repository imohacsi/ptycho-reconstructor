#ifndef PTYREC_DATA_FORMAT_H_
#define PTYREC_DATA_FORMAT_H_

class scan_format {
public:
	int _scan_numbr;
	int _strt_frame;
	int _stop_frame;
	int _x_center;
	int _y_center;

};


class data_format {
public:
	int _num_pixels;
	int _num_scans;

	/**Diffraction pattern format*/
	std::string _image_format;
	bool _do_image_binning;
	int64_t _image_binvalue;
	int64_t _frame_per_line;

	/**Mask data format**/
	bool _do_load_mask;


	/**Metadata format**/
	std::string _position_format;
	std::string _position_sensor;

	/**Path and prefix to the data*/
	std::string _data_path;
	std::string _meta_path;
	std::string _data_prefix;
	std::string _meta_prefix;
	std::string _mask_file;

	/**List of to-be-loaded scans**/
	std::vector<scan_format> _scan_list;

	/**These are the debug settings*/
	bool _diag_print_imag;
	bool _diag_print_meta;
	bool _diag_print_mask;
};


class raw_data{
    public:
    /**These are the final data tables*/
    std::vector<std::vector<int>>                   _mask_data;
    std::vector<std::vector<std::vector<float>>>    _imag_data;
    std::vector<std::vector<std::vector<double>>>   _pos_data;
    std::vector<int> _scan_num;
    double _countcutoff;

    void clear(){
        _mask_data.clear();
        _imag_data.clear();
        _pos_data.clear();
        _scan_num.clear();
    }
};



#endif // PTYREC_DATA_FORMAT_H_
