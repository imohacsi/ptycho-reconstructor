#ifndef DESYPTYCHO_LOAD_MASKDATA_H_
#define DESYPTYCHO_LOAD_MASKDATA_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <hdf5.h>
#include <sys/stat.h>

#include "../imageprocessing/imageprocessing.h"
#include "data_format.hpp"
#include "../utilities/fileio.hpp"
#include "../utilities/colorconsole.hpp"


class mask_data_reader {
protected:
	data_format _conf;
	std::vector<std::vector<int>>  _full_mask;
	std::vector<std::vector<int>>  _mask_data;

	void loadmask_tiff( char* filename, int cen_x, int cen_y);
	void LoadScan( int);
	void print_mask_data();

public:
	mask_data_reader(data_format conf): _conf(conf) {};
	std::vector<std::vector<int>> get_mask_data();
	void load_mask_data();
//    ~load_mask();
};




#endif
