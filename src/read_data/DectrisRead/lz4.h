#ifndef __LZ4_H_COMPRESSION__
#define __LZ4_H_COMPRESSION__


/*
   LZ4 - Fast LZ compression algorithm
   Header File
   Copyright (C) 2011-2012, Yann Collet.
   BSD 2-Clause License (http://www.opensource.org/licenses/bsd-license.php)

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:

       * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
       * Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following disclaimer
   in the documentation and/or other materials provided with the
   distribution.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   You can contact the author at :
   - LZ4 homepage : http://fastcompression.blogspot.com/p/lz4.html
   - LZ4 source repository : http://code.google.com/p/lz4/
*/
#pragma once

#if defined (__cplusplus)
extern "C" {
#endif


//**************************************
// Compiler Options
//**************************************
#if defined(_MSC_VER) && !defined(__cplusplus)   // Visual Studio
#  define inline __inline           // Visual is not C99, but supports some kind of inline
#endif


//****************************
// Simple Functions
//****************************

int LZ4_compress   (const char* source, char* dest, int isize);
int LZ4_uncompress (const char* source, char* dest, int osize);

/*
LZ4_compress() :
    Compresses 'isize' bytes from 'source' into 'dest'.
    Destination buffer must be already allocated,
    and must be sized to handle worst cases situations (input data not compressible)
    Worst case size evaluation is provided by function LZ4_compressBound()

    isize  : is the input size. Max supported value is ~1.9GB
    return : the number of bytes written in buffer dest


LZ4_uncompress() :
    osize  : is the output size, therefore the original size
    return : the number of bytes read in the source buffer
             If the source stream is malformed, the function will stop decoding and return a negative result, indicating the byte position of the faulty instruction
             This function never writes outside of provided buffers, and never modifies input buffer.
    note : destination buffer must be already allocated.
           its size must be a minimum of 'osize' bytes.
*/


//****************************
// Advanced Functions
//****************************

static inline int LZ4_compressBound(int isize) {
	return ((isize) + ((isize)/255) + 16);
}
#define           LZ4_COMPRESSBOUND(    isize)            ((isize) + ((isize)/255) + 16)

/*
LZ4_compressBound() :
    Provides the maximum size that LZ4 may output in a "worst case" scenario (input data not compressible)
    primarily useful for memory allocation of output buffer.
	inline function is recommended for the general case,
	but macro is also provided when results need to be evaluated at compile time (such as table size allocation).

    isize  : is the input size. Max supported value is ~1.9GB
    return : maximum output size in a "worst case" scenario
    note : this function is limited by "int" range (2^31-1)
*/


int LZ4_compress_limitedOutput   (const char* source, char* dest, int isize, int maxOutputSize);

/*
LZ4_compress_limitedOutput() :
    Compress 'isize' bytes from 'source' into an output buffer 'dest' of maximum size 'maxOutputSize'.
    If it cannot achieve it, compression will stop, and result of the function will be zero.
    This function never writes outside of provided output buffer.

    isize  : is the input size. Max supported value is ~1.9GB
    maxOutputSize : is the size of the destination buffer (which must be already allocated)
    return : the number of bytes written in buffer 'dest'
             or 0 if the compression fails
*/


int LZ4_uncompress_unknownOutputSize (const char* source, char* dest, int isize, int maxOutputSize);

/*
LZ4_uncompress_unknownOutputSize() :
    isize  : is the input size, therefore the compressed size
    maxOutputSize : is the size of the destination buffer (which must be already allocated)
    return : the number of bytes decoded in the destination buffer (necessarily <= maxOutputSize)
             If the source stream is malformed, the function will stop decoding and return a negative result, indicating the byte position of the faulty instruction
             This function never writes beyond dest + maxOutputSize, and is therefore protected against malicious data packets
    note   : Destination buffer must be already allocated.
             This version is slightly slower than LZ4_uncompress()
*/


#if defined (__cplusplus)
}
#endif




#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <H5PLextern.h>

#ifndef INT32_MAX
#define INT32_MAX   0x7fffffffL  /// 2GB
#endif
#define LZ4_FILTER 32004


/// conversion macros: BE -> host, and host -> BE
#ifdef _WIN32
#include <Winsock2.h>
#else
#include <netinet/in.h>
#endif

#define htonll(x) ( ( (uint64_t)(htonl( (uint32_t)((x << 32) >> 32)))<< 32) | htonl( ((uint32_t)(x >> 32)) ))
#define ntohll(x) htonll(x)

#define htobe16t(x) htons(x)
#define htobe32t(x) htonl(x)
#define htobe64t(x) htonll(x)
#define be16toht(x) ntohs(x)
#define be32toht(x) ntohl(x)
#define be64toht(x) ntohll(x)


#define DEFAULT_BLOCK_SIZE 1<<30; /* 1GB. LZ4 needs blocks < 1.9GB. */

static size_t lz4_filter(unsigned int flags, size_t cd_nelmts,const unsigned int cd_values[], size_t nbytes, size_t *buf_size, void **buf) {
	void * outBuf = NULL;
	size_t ret_value;

	if (flags & H5Z_FLAG_REVERSE) {
		const char* rpos = (char*)*buf; /* pointer to current read position */


		const uint64_t * const i64Buf = (uint64_t *) rpos;
		const uint64_t origSize = (uint64_t)(be64toht(*i64Buf));/* is saved in be format */
		rpos += 8; /* advance the pointer */

		uint32_t *i32Buf = (uint32_t*)rpos;
		uint32_t blockSize = (uint32_t)(be32toht(*i32Buf));
		rpos += 4;
		if(blockSize>origSize)
			blockSize = origSize;

		if (NULL==(outBuf = malloc(origSize))) {
			printf("cannot malloc\n");
			goto error;
		}
		char *roBuf = (char*)outBuf;   /* pointer to current write position */
		uint64_t decompSize     = 0;
		/// start with the first block ///
		while(decompSize < origSize) {

			if(origSize-decompSize < blockSize) /* the last block can be smaller than blockSize. */
				blockSize = origSize-decompSize;
			i32Buf = (uint32_t*)rpos;
			uint32_t compressedBlockSize =  be32toht(*i32Buf);  /// is saved in be format
			rpos += 4;
			if(compressedBlockSize == blockSize) { /* there was no compression */
				memcpy(roBuf, rpos, blockSize);
			} else { /* do the decompression */
//                int compressedBytes = LZ4_uncompress(rpos, roBuf, blockSize); /**Original**/
				uint32_t compressedBytes = LZ4_uncompress(rpos, roBuf, blockSize);
				if(compressedBytes != compressedBlockSize) {
					printf("decompressed size not the same: %d, != %d\n", compressedBytes, compressedBlockSize);
					goto error;
				}
			}

			rpos += compressedBlockSize;   /* advance the read pointer to the next block */
			roBuf += blockSize;            /* advance the write pointer */
			decompSize += blockSize;
		}
		free(*buf);
		*buf = outBuf;
		outBuf = NULL;
		ret_value = (size_t)origSize;  // should always work, as orig_size cannot be > 2GB (sizeof(size_t) < 4GB)
	} else { /* forward filter */
		if (nbytes > INT32_MAX) {
			/* can only compress chunks up to 2GB */
			goto error;
		}


		size_t blockSize;
		if(cd_nelmts > 0 && cd_values[0] > 0) {
			blockSize = cd_values[0];
		} else {
			blockSize = DEFAULT_BLOCK_SIZE;
		}
		if(blockSize > nbytes) {
			blockSize = nbytes;
		}
		size_t nBlocks = (nbytes-1)/blockSize +1 ;
		if (NULL==(outBuf = malloc(LZ4_COMPRESSBOUND(nbytes)+ 4+8 + nBlocks*4))) {
			goto error;
		}

		char *rpos  = (char*)*buf;      /* pointer to current read position */
		char *roBuf = (char*)outBuf;    /* pointer to current write position */
		/* header */
		uint64_t * i64Buf = (uint64_t *) (roBuf);
		i64Buf[0] = htobe64t((uint64_t)nbytes); /* Store decompressed size in be format */
		roBuf += 8;

		uint32_t *i32Buf =  (uint32_t *) (roBuf);
		i32Buf[0] = htobe32t((uint32_t)blockSize); /* Store the block size in be format */
		roBuf += 4;

		size_t outSize = 12; /* size of the output buffer. Header size (12 bytes) is included */

		size_t block;
		for(block = 0; block < nBlocks; block++) {
			size_t origWritten = block*blockSize;
			if(nbytes - origWritten < blockSize) /* the last block may be < blockSize */
				blockSize = nbytes - origWritten;

			uint32_t compBlockSize = LZ4_compress(rpos, roBuf+4, blockSize); /// reserve space for compBlockSize
			if(!compBlockSize)
				goto error;
			if(compBlockSize >= blockSize) { /* compression did not save any space, do a memcpy instead */
				compBlockSize = blockSize;
				memcpy(roBuf+4, rpos, blockSize);
			}

			i32Buf =  (uint32_t *) (roBuf);
			i32Buf[0] = htobe32t((uint32_t)compBlockSize);  /* write blocksize */
			roBuf += 4;

			rpos += blockSize;     	/* advance read pointer */
			roBuf += compBlockSize;       /* advance write pointer */
			outSize += compBlockSize + 4;
		}

		free(*buf);
		*buf = outBuf;
		*buf_size = outSize;
		outBuf = NULL;
		ret_value = outSize;

	}
done:
	if(outBuf)
		free(outBuf);
	return ret_value;


error:
	if(outBuf)
		free(outBuf);
	outBuf = NULL;
	return 0;

}

const H5Z_class2_t H5Z_LZ4[1] = {{
		H5Z_CLASS_T_VERS,                 /* H5Z_class_t version */
		(H5Z_filter_t)32004,         /* Filter id number             */
		1,              /* encoder_present flag (set to true) */
		1,              /* decoder_present flag (set to true) */
		"HDF5 lz4 filter; see http://www.hdfgroup.org/services/contributions.html",
		/* Filter name for debugging    */
		NULL,           /* The "can apply" callback     */
		NULL,           /* The "set local" callback     */
		(H5Z_func_t)lz4_filter,         /* The actual filter function   */
	}
};





#endif











