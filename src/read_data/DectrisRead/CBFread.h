#ifndef DESYPTYCHO_LOAD_CBF_H_
#define DESYPTYCHO_LOAD_CBF_H_

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cmath>


using namespace std;

double *ReadCBF(int& N_x, int& N_y, int& cutoff, char *filename);


#endif
