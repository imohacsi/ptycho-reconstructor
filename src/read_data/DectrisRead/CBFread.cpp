#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cmath>


using namespace std;
int Debug=0;


unsigned int bseek(char *where, unsigned int from, unsigned int to, char *what) {
	unsigned int L=strlen(what);
	unsigned int ii,j;
	unsigned int gut;
	unsigned int pos=0;

	for(ii=from; ii<to; ii++) {
		gut=0;
		for(j=0; j<L; j++) {
			if(where[ii+j]==what[j]) {
				gut+=1;
			}
		}
		if(gut==L) {
			pos=ii;
			ii=to;
		}
	}
	return pos;
}


double *ReadCBF(int& N_x, int& N_y, int& cutoff, char *filename) {
	int errval;
	char *errvalc;
	int SizeOfCBF;
	int MaxHeaderLength=2048;
	char end_of_header_sign[5]= {12, 26, 4, 213-256, 0 };

	FILE *fp=NULL;
	fp=fopen(filename,"rb");
	if(fp==NULL) {
		printf("CBF file %s not found, closing\n",filename);
		exit(EXIT_FAILURE);
	}

	if(Debug>2) {
		if(fp!=NULL) {
			fprintf(stdout,"\nIts OPEN\n");
		} else {
			fprintf(stdout,"\nError Opening file %s\n",filename);
		}
	}

	fseek(fp, 0L, SEEK_END);
	SizeOfCBF = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	char line[256];
	char junk[256];
	char *CBFfile;
	CBFfile=(char*)calloc(SizeOfCBF+2,sizeof(char));

	errval=fread(CBFfile,1,SizeOfCBF,fp);
	fseek(fp, 0L, SEEK_SET);

	do {
		errvalc=fgets(line,255,fp);
		if(Debug>2) {
			fprintf(stdout,"%s",line);
		}
		if(strstr(line,"Count_cutoff")!=nullptr) {
			sscanf(line,"%s %s %d",junk,junk,&cutoff);
		}
	} while(strstr(line,"--CIF-BINARY-FORMAT-SECTION--")==0);



	char ContentType[256];
	errval=fscanf(fp,"%s%s;",junk,ContentType);
	if(Debug>2) {
		fprintf(stdout,"Content-Type: %s\n",ContentType);
	}

	char conversion[256];
	errval=fscanf(fp,"%[^\"]%c%[^\"]%c",junk,junk, conversion,junk);
	if(Debug>0) {
		fprintf(stdout,"     conversions= %s\n",conversion);
	}

	char encoding[256];
	errval=fscanf(fp,"%s %s",junk,encoding);
	if(Debug>2) {
		fprintf(stdout,"Content-Transfer-Encoding: %s\n",encoding);
	}

////////////////////////
//Important parameters//
////////////////////////
	int BinarySize;
	errval=fscanf(fp,"%s %d",junk,&BinarySize);
	if(Debug>1) {
		fprintf(stdout,"X-Binary-Size: %d\n",BinarySize);
	}

	int BinaryID;
	errval=fscanf(fp,"%s %d",junk,&BinaryID);
	if(Debug>2) {
		fprintf(stdout,"X-Binary-ID: %d\n",BinaryID);
	}

	char ElementType[256];
	errval=fscanf(fp,"%s \"%[^\"]\"",junk,ElementType);
	if(Debug>1) {
		fprintf(stdout,"X-Binary-Elemet-Type: %s\n",ElementType);
	}

	char ByteOrder[256];
	errval=fscanf(fp,"%s %s",junk,ByteOrder);
	if(Debug>2) {
		fprintf(stdout,"X-Binary-Byte-Order: %s\n",ByteOrder);
	}

	char MD5[256];
	errval=fscanf(fp,"%s %s\n",junk,MD5);
	if(Debug>2) {
		fprintf(stdout,"Content-MD5: %s\t(depends on coding)\n",MD5);
	}

////////////////////////
//Important parameters//
////////////////////////

	int N_elements;
	errval=fscanf(fp,"%s %d\n",junk,&N_elements);
	if(Debug>1) {
		fprintf(stdout,"X-Binary-Number-of-Elements: %d\n",N_elements);
	}
	errval=fscanf(fp,"%s %d\n",junk,&N_x);
	if(Debug>1) {
		fprintf(stdout,"X-Binary-Fastest-Dimension: %d\n",N_x);
	}
	errval=fscanf(fp,"%s %d\n",junk,&N_y);
	if(Debug>1) {
		fprintf(stdout,"X-Binary-Second-Dimension: %d\n",N_y);
	}

	int SizePadding;
	errval=fscanf(fp,"%s %d\n",junk,&SizePadding);
	if(Debug>2) {
		fprintf(stdout,"X-Binary-Size-Padding: %d\n",SizePadding);
	}

////////////////////////////////
//Closing file//////
	fclose(fp);

	int pstart=bseek(CBFfile,0,MaxHeaderLength,end_of_header_sign)+strlen(end_of_header_sign);

	if(Debug>2) {
		fprintf(stdout,"Start Position: %d\n",pstart);
	}

	unsigned char *CompData;
	CompData=(unsigned char *)calloc(BinarySize+32,sizeof(unsigned char));

	for(int i=pstart; i<=pstart+BinarySize; i++) {
		CompData[i-pstart]=static_cast<unsigned char>(CBFfile[i]);
	}

//if(Debug>0){ fprintf(stdout,"Img size: %dx%d=%d\n",Nx,Ny,Nx*Ny); }
	double *Data;
	Data=(double *)calloc(N_x*N_y,sizeof(double));


	int ind_out = 0;
	int ind_in = 0;
	double val_curr = 0;
	double difference = 0;

	while(ind_in<BinarySize) {
		difference = (double)(CompData[ind_in]);
		ind_in = ind_in +1;
		if (difference!=128.0) {
			//if not escaped as -128 (0x80=128) use the current byte as difference, with manual complement to emulate$
			if(difference >=128.0) {
				difference=difference-256;
			}

		} else {
			//otherwise check for 16-bit integer value
			if((CompData[ind_in]!= 0x00) || (CompData[ind_in+1]!=0x80)) {
				//if not escaped as -32768 (0x8000) use the current 16-bit integer as difference
				difference=(double)CompData[ind_in]+256*(double)CompData[ind_in+1];
				//manual complement to emulate the sign
				if(difference>=32768) {
					difference=difference-65536;
				}
				ind_in = ind_in +2;
			} else {
				ind_in = ind_in +2;
				//if everything else failed use the current 32-bit value as difference
				difference=(double)CompData[ind_in]+256*(double)CompData[ind_in+1]+65536*(double)CompData[ind_in+2]+16777216*(double)CompData[ind_in+3];
				//manual complement to emulate the sign
				if(difference>=2147483648.0) {
					difference=difference-4294967296.0;
				}
				ind_in = ind_in +4;
			}
		}
		val_curr = val_curr + difference;
		if(val_curr<1.0) {
			val_curr=0.0;
		}
		Data[ind_out] =(double)val_curr;
		ind_out++;
	}
	Data[0]=0.0;
	if(Debug>0) {
		printf("Extracted: %d pixels\n",ind_out);
	}


	free(CBFfile);
	free(CompData);
	return Data;
} /*EoF*/

double *ReadRAW(int N_x, int N_y, char *filename) {
	FILE *fp=NULL;
	fp=fopen(filename,"rb");
	unsigned int tmp;
	double *Data;
	size_t errval=0;
	Data=(double *)calloc(N_x*N_y,sizeof(double));
	for(int ii=0; ii<N_x*N_y; ii++) {
		errval+=fread(&tmp,4,1,fp);
		Data[ii]=(double)tmp;
	}
	fclose(fp);
	return Data;
}

