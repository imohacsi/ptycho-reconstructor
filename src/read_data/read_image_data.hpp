#ifndef DESYPTYCHO_LOAD_IMAGEDATA_H_
#define DESYPTYCHO_LOAD_IMAGEDATA_H_

#include <iostream>
#include <string>
#include <vector>
#include <valarray>
#include <hdf5.h>
#include <sys/stat.h>
#include "./DectrisRead/CBFread.h"
#include "../utilities/colorconsole.hpp"
#include "../utilities/fileio.hpp"
#include "../imageprocessing/imageprocessing.h"
#include "data_format.hpp"

/**
* Class collecting procedures for the reading of diffraction patterns
*/
class imag_data_reader {
protected:
	data_format _conf;
	std::vector<float> imload_tiff(char*,const int,const int);
	std::vector<float> imload_cbf(char*,const int,const int);
	void ScanLoad_TIFF(scan_format,int);
	void ScanLoad_HDF5(scan_format,int);
	void ScanLoad_CXI(scan_format,int);
	void ScanLoad_CBF(scan_format,int);
	void ScanLoad_CBF_fast(scan_format,int);
	void print_image_data();
	std::vector<std::vector<std::vector<float>>>  _img_data;

public:
	std::vector<std::vector<std::vector<float>>> get_image_data();
	void load_image_data();
	imag_data_reader(data_format conf): _conf(conf) {};
	double CountLimit;
//    ~imag_data_reader() {};
};

#endif
