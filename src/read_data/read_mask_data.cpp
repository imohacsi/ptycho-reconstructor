#include "read_mask_data.hpp"
#include "../imageprocessing/imageprocessing.h"

/**Read valid pixel mask for a given scan**/
void mask_data_reader::LoadScan(int idxscan) {
	char imgfilename[512];
	/**Load mask from separate tiff file**/
	sprintf( imgfilename, "%s", _conf._mask_file.c_str() );
	try {
		checkFileExist(imgfilename);
	} catch(exception& ex) {
	}

	if( this->_conf._do_load_mask>0){
        loadmask_tiff(imgfilename, _conf._scan_list[idxscan]._x_center, _conf._scan_list[idxscan]._y_center ); }
	else{
        std::vector<int> detector_mask(_conf._num_pixels*_conf._num_pixels,1);
        _mask_data.push_back(detector_mask);
	}
}



/**Read a pixel mask from an 8bit TIFF file to eliminate dead or hot pixels and tiling. **/
void mask_data_reader::loadmask_tiff(char* filename, int cen_x, int cen_y) {
	int xx,yy,bx,by;
	int bin = _conf._image_binvalue;
	double *Map=nullptr;

	/**Check if the file really exists!*/
	struct stat filestat;
	if( stat(filename,&filestat)!=0 ) {
		fprintf(stderr, "%sERROR: Mask file %s does not exist!\n%s",A_C_RED,filename,A_C_RESET);
		exit(EXIT_FAILURE);
	}


	/**Load the image to know its size*/
	int Nx,Ny;
	Map=read_tiff(Nx,Ny,filename);

	/**Collecting full detector masks**/
	std::vector<int> frame_mask(Nx*Ny,1);
	for(int64_t xy=0; xy<Nx*Ny; xy++) {
		frame_mask[xy]=Map[xy];
	}
	_full_mask.push_back(frame_mask);

	/**Selecting region of interest**/
	std::vector<int> detector_mask(_conf._num_pixels*_conf._num_pixels,1);
	#pragma omp parallel for private(xx,yy,bx,by)
	for(yy=-_conf._num_pixels/2; yy<_conf._num_pixels/2; yy++) {
		for(xx=-_conf._num_pixels/2; xx<_conf._num_pixels/2; xx++) {
			for(by=0; by<bin; by++) {
				for(bx=0; bx<bin; bx++) {
					if( (int)Map[(bx+xx*bin)+cen_x+((by+yy*bin)+cen_y)*Nx]!=255 ) {
						detector_mask[xx+_conf._num_pixels/2+(yy+_conf._num_pixels/2)*_conf._num_pixels]=0;
					}
				}
			}
		}
	}
	_mask_data.push_back(detector_mask);


	/**Free temporary buffer*/
	delete[] Map;
}

void mask_data_reader::load_mask_data() {
	if(!_conf._do_load_mask){
		for(int ss=0;ss<_conf._num_scans;ss++){
			std::vector<int> detector_mask(_conf._num_pixels*_conf._num_pixels,1);
			_mask_data.push_back(detector_mask);
		}
	return;
	}
	
		
	printf("%sReading masks...%s\n",A_C_CYAN,A_C_RESET);
	for(int ss=0; ss<_conf._num_scans; ss++) {
		LoadScan(ss);
	}
	if( _conf._diag_print_mask) {
		print_mask_data();
	}
	printf("%s\tDone with reading masks%s\n",A_C_GREEN,A_C_RESET);
}



std::vector<std::vector<int>> mask_data_reader::get_mask_data() {
	return _mask_data;
}

/**If debugging mode active, writes out the read
*   masks for each scan.
*/
void mask_data_reader::print_mask_data() {
	char filename[512];
	for(size_t ss=0; ss<_mask_data.size(); ss++) {
		sprintf(filename,"./output/ValidPixelMask_S%05d.tif",int(ss));
		WriteImOut(_mask_data[ss].data(),_conf._num_pixels,_conf._num_pixels,"lin","int","TIFF",filename);
	}
}
