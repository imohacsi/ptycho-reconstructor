#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "config.hpp"


/**Main configuration file loader, that is completely hand written
*   to parse an input file in ".ini" file format. Unspecified
*   parameters are left alone and not overwritten.
*   @param filename
*/
void configure::parse_inifile(std::string filename) {
	/**Check if file exists**/
	std::ifstream confile(filename);
	if( confile.fail() ) {
		printf("%sERROR: initialization file %s not found, exiting.%s\n",A_C_RED,filename.c_str(),A_C_RESET);
		fflush(stdout);
		exit(EXIT_FAILURE);
	}

	std::string cline;
	std::string category;
	char h_cat=91;  //Special characer "["
	char h_com=35;  //Special character "#"

	std::string stmp;

	/**Reading and parsing the config file line by line**/
	while (std::getline(confile,cline)) {
		if(!cline.empty()) {
			std::istringstream instr (cline);

			if( cline[0]==h_cat ) {
				instr >> category;
				category.erase(0,1);
				category.pop_back();
			} else if(cline[0]==h_com ) {
				continue;
			} else {
				parse_iniline(cline,category);
			}
		}
	}/**Until EoF*/

	/**Passing some parameters around*/
	wconfig._lambda=1.0e-9*1240.0/wconfig._energy;
	wconfig._dPX=wconfig._det_distance*wconfig._lambda/(wconfig._det_pixelsize*wconfig._num_pixels);
	data_in._num_pixels=wconfig._num_pixels;
	data_in._num_scans=wconfig._tot_nscans;
	printf("%sDone config file parsing...%s\n",A_C_GREEN,A_C_RESET);
	fflush(stdout);
}


/**Function to parse a single line of input**/
void configure::parse_iniline(std::string cline,std::string category) {
	std::string parameter_name;
	std::istringstream instr (cline);

	std::string equal_sign;
	instr >> parameter_name >> equal_sign;

	/**Reconstruction parameters*/
	if(category=="reconstruction") {
		if(parameter_name=="run_name") {
			instr >> wconfig._run_name;
		} else if(parameter_name=="num_threads") {
			instr >> wconfig._num_THREADS;
		} else if(parameter_name=="num_pixels") {
			instr >> wconfig._num_pixels;
		} else if(parameter_name=="batch_size") {
			instr >> wconfig._batch_size;
		} else if(parameter_name=="obj_border") {
			instr >> wconfig._border;
		} else if(parameter_name=="mode_obj") {
			instr >> wconfig._num_omodes;
		} else if(parameter_name=="mode_pro") {
			instr >> wconfig._num_pmodes;
		} else if(parameter_name=="num_rec_iter") {
			instr >> wconfig._num_rec_iter;
		} else if(parameter_name=="num_ref_iter") {
			instr >> wconfig._num_ref_iter;
		} else if(parameter_name=="energy") {
			instr >> wconfig._energy;
		} else if(parameter_name=="distance") {
			instr >> wconfig._det_distance;
		} else if(parameter_name=="pixelsize") {
			instr >> wconfig._det_pixelsize;
		} else if(parameter_name=="count_cutoff") {
			instr >> wconfig._cutoff;
			printf("\tRead cutoff: %g...\n",wconfig._cutoff);
		} else {
            printf("%sWARNING: Unknown tag in domain \"reconstruction\": %s%s\n",A_C_YELLOW,parameter_name.c_str(),A_C_RESET );
		}
	}

	/**Data loading parameters*/
	if(category=="datainput") {
		if(parameter_name=="num_scans") {
			instr >> wconfig._tot_nscans;
			data_in._num_scans = wconfig._tot_nscans;
		}
		if(parameter_name=="image_format") {
			instr >> data_in._image_format;
		}
		if(parameter_name=="data_prefix") {
			instr >> data_in._data_prefix;
			data_in._meta_prefix = data_in._data_prefix;
		}
		if(parameter_name=="data_path") {
			instr >> data_in._data_path;
			data_in._meta_path = data_in._data_path;
		}
		if(parameter_name=="image_prefix") {
			instr >> data_in._data_prefix;
		}
		if(parameter_name=="image_path") {
			instr >> data_in._data_path;
		}
		if(parameter_name=="meta_prefix") {
			instr >> data_in._meta_prefix;
		}
		if(parameter_name=="meta_path") {
			instr >> data_in._meta_path;
		}
		if(parameter_name=="image_do_bining") {
			instr >> data_in._do_image_binning;
		}
		if(parameter_name=="image_binvalue") {
			instr >> data_in._image_binvalue;
		}
		if(parameter_name=="frame_per_line") {
			instr >> data_in._frame_per_line;
		}

		if(parameter_name=="mask_file") {
			instr >> data_in._mask_file;
		}
		if(parameter_name=="detector_do_pixmask") {
			instr >> data_in._do_load_mask;
		}
		if(parameter_name=="detector_mask") {
			instr >> data_in._mask_file;
		}

		/**Metadata loading parameters*/
		if(parameter_name=="meta_format") {
			instr >> data_in._position_format;
		}
		if(parameter_name=="meta_sens") {
			instr >> data_in._position_sensor;
		}
		/**Debuging parameters*/
		if(parameter_name=="dbg_imag") {
			instr >> data_in._diag_print_imag;
		}
		if(parameter_name=="dbg_meta") {
			instr >> data_in._diag_print_meta;
		}
		if(parameter_name=="dbg_mask") {
			instr >> data_in._diag_print_mask;
		}
	}

	/**Data preprocessor parameters**/
	if(category=="preprocessor") {
		if(parameter_name=="shareobj") {
			instr >> pproc_in._do_share_object;
		} else if(parameter_name=="sharepro") {
			instr >> pproc_in._do_share_probe;
		} else if(parameter_name=="dbg_ampli") {
			instr >> pproc_in._diag_print_ampli;
		} else if(parameter_name=="dbg_xypos") {
			instr >> pproc_in._diag_print_xypos;
		} else if(parameter_name=="dbg_lists") {
			instr >> pproc_in._diag_print_lists;
		} else {
            printf("%sWARNING: Unknown tag in domain \"preprocessor\": %s%s\n",A_C_YELLOW,parameter_name.c_str(),A_C_RESET );
		}
	}

	/**Initial guess parameters*/
	if(category=="initguess") {
		if(parameter_name=="ipro_probe_type") {
			instr >> guess_in._ipro_init_type;
		} else if(parameter_name=="ipro_mmodescale") {
			instr >> guess_in._ipro_scaling_rule;
		} else  if(parameter_name=="iprov_mmodeplace") {
			instr >> guess_in._ipro_mmode_placement;
		} else if(parameter_name=="ipro_diameter") {
			instr >> guess_in._ipro_diameter;
		} else if(parameter_name=="ipro_distance") {
			instr >> guess_in._ipro_distance;
        } else if(parameter_name=="ipro_prop_distance") {
			instr >> guess_in._ipro_prop_distance;
		} else if(parameter_name=="ipro_scanspeed") {
			instr >> guess_in._ipro_mmode_speed;
		} else if(parameter_name=="ipro_scanfocus") {
			instr >> guess_in._ipro_mmode_focal;
		} else if(parameter_name=="ipro_do_propagate") {
			instr >> guess_in._ipro_do_propagate;
        } else if(parameter_name=="ipro_do_salt_modes") {
			instr >> guess_in._ipro_do_saltmodes;
		} else if(parameter_name=="ipro_do_orth_modes") {
			instr >> guess_in._ipro_do_orthmodes;
		} else if(parameter_name=="ipro_do_renorm") {
			instr >> guess_in._ipro_do_intnorm;
		} else if(parameter_name=="iobj_object_type") {
			instr >> guess_in._iobj_init_type;
		} else if(parameter_name=="ipsi_views_type") {
			instr >> guess_in._ipsi_init_type;
		}
	}

	/**General reconstruction engine parameters, algorithm, regularization, etc...*/
	if(category=="engines") {
		if(parameter_name=="algorithm_rec") {           //Algorithm selection
			instr >> engin_in.algorithm_rec;
		} else if(parameter_name=="algorithm_ref") {
			instr >> engin_in.algorithm_ref;
		} else if(parameter_name=="alg_precision") {
			instr >> engin_in.precision;
		} else if(parameter_name=="opupd_niter") {      //Object/ptobe update
			instr >> engin_in.engcom._opupd_niter;
		} else if(parameter_name=="proupd_reg") {
			instr >> engin_in.engcom._opupd_regpro;
		} else if(parameter_name=="start_proupd") {
			instr >> engin_in.engcom._start_proupd;
		} else if(parameter_name=="optimizer_alg") {    //Optimization parameters
			instr >> engin_in.engcom._optim_alg;
		} else if(parameter_name=="optimizer_decay") {
			instr >> engin_in.engcom._optim_decay;
		} else if(parameter_name=="start_objupd") {
			instr >> engin_in.engcom._start_objupd;
		} else if(parameter_name=="lrate_obj") {
			instr >> engin_in.engcom._lrate_obj;
		} else if(parameter_name=="lrate_pro") {
			instr >> engin_in.engcom._lrate_pro;
		} else if(parameter_name=="reg_grad") {
			instr >> engin_in.engcom._l2reg_objgrad;
		} else if(parameter_name=="dm_magupd_reg") {    //Magnitude update settings
			instr >> engin_in.engcom._magupdate_reg;
		} else if(parameter_name=="dm_magupd_lvl") {
			instr >> engin_in.engcom._magupdate_lvl;
		}


		/**Probe support**/
		if(parameter_name=="do_prsup_r") {
			instr >> engin_in.engcom._do_prsup_r;
		} else if(parameter_name=="prsup_r") {
			instr >> engin_in.engcom._prsup_r;
		} else if(parameter_name=="do_prsup_q") {
			instr >> engin_in.engcom._do_prsup_q;
		} else if(parameter_name=="prsup_q") {
			instr >> engin_in.engcom._prsup_q;
		}

		/**Diagnostic parameters**/
		if(parameter_name=="print_every") {
			instr >> engin_in.engcom._WriteEveryNthIter;
		} else if(parameter_name=="do_calc_lover") {
			instr >> engin_in.engcom._do_calc_lover;
		} else if(parameter_name=="do_calc_error") {
			instr >> engin_in.engcom._do_calc_error;
		} else if(parameter_name=="do_calc_imode") {
			instr >> engin_in.engcom._do_calc_print;
		}


		/**Exotic parameter refinements...**/
		if(parameter_name=="do_pscale") {
			instr >> engin_in.engcom._do_patt_scale;
		} else if(parameter_name=="do_bgref") {
			instr >> engin_in.engcom._do_bgref_q;
		} else if(parameter_name=="bgref_q") {
			instr >> engin_in.engcom._bgref_q;
		} else if(parameter_name=="bgref_start") {
			instr >> engin_in.engcom._bgref_start;
		} else if(parameter_name=="bgref_every") {
			instr >> engin_in.engcom._bgref_every;
		} else if(parameter_name=="bgref_stop") {
			instr >> engin_in.engcom._bgref_stop;
		} else if(parameter_name=="bgref_alg") {
			instr >> engin_in.engcom._bgref_alg;
		}
	}

	if(category=="posref") {
		if(parameter_name=="do_posref") {
			instr >> engin_in.auxcom._do_posref;
		} else if(parameter_name=="posref_num_iter") {
			instr >> engin_in.auxcom._posref_niter;
		} else if(parameter_name=="posref_num_step") {
			instr >> engin_in.auxcom._posref_nstep;
		} else if(parameter_name=="posref_lrate") {
			instr >> engin_in.auxcom._lrate_posref;
		} else if(parameter_name=="posref_max_step") {
			instr >> engin_in.auxcom._posref_maxjump;
		} else{
            printf("%sWARNING: Unknown tag in domain \"posref\": %s%s\n",A_C_YELLOW,parameter_name.c_str(),A_C_RESET );
		}
	}

	/**Data output parameters, format and location**/
	if(category=="dataoutput") {
		if(parameter_name=="results_path") {
			instr >> print_out._path_results;
		} else if(parameter_name=="results_tiff") {
			instr >> print_out._do_results_tiff;
		} else if(parameter_name=="results_hdf5") {
			instr >> print_out._do_results_hdf5;
		} else if(parameter_name=="results_binf") {
			instr >> print_out._do_results_binf;
		} else {
            printf("%sWARNING: Unknown tag in domain \"dataoutput\": %s%s\n",A_C_YELLOW,parameter_name.c_str(),A_C_RESET );
		}
	}



}






