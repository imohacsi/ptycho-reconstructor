#include "config.hpp"


/**
*   ParseScanList: reads the list of reconstructed scans
*   @param filename Filename of the file containing the
*       list of scans, the number of frames per scan and
*       the center positions in X and Y as tab or space
*       separated values.
*   @param num_scans Specifies the number of
*       actually read scans, so it reads only the first
*       few scans from the file.
*   To-do: add proper detection in case of too short config file
*/
void configure::parse_scanlist(std::string filename, int num_scans) {
	std::ifstream inpfile(filename.c_str() );
	if( inpfile.fail() ) {                                                       /*Check if file exists*/
		printf("%s ERROR: scan list file %s not found, exiting.%s\n",A_C_RED,filename.c_str(),A_C_RESET);
		printf("\t%sThis can also be due to Windows/Unix newlines!%s\n",A_C_RED,A_C_RESET);
		fflush(stdout);
		exit(EXIT_FAILURE);
	}

	data_in._scan_list.resize(0);
	scan_format tmp;                                                                                  /*Create temporary buffer*/
	for(int ss=0; ss<num_scans; ss++) {
		if( inpfile.eof() ){
            printf("%sERROR: end of scan list reached in %d lines ( %d expected).%s\n",A_C_RED,ss,num_scans,A_C_RESET);
            fflush(stdout);
		}
		inpfile >> tmp._scan_numbr;                                                                      /*Fill up temporary buffer*/
		inpfile >> tmp._strt_frame;
		inpfile >> tmp._stop_frame;
		inpfile >> tmp._x_center;
		inpfile >> tmp._y_center;
		data_in._scan_list.push_back(tmp);                                                                  /*Push the new scan to the back of _scan_list*/
	}
	inpfile.close();
}

