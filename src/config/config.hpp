#ifndef PTYREC_CONFIG_CONFIG_H_
#define PTYREC_CONFIG_CONFIG_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sys/stat.h>
#include <algorithm>
#include <exception>

#include "../reconstruction/worker_config.hpp"
#include "../read_data/data_format.hpp"
#include "../initialguess/guess_format.hpp"
#include "../read_data/data_format.hpp"
#include "../preproc_data/preproc_data.hpp"
#include "../pipelines/engines/engine_format.hpp"
#include "../print_data/print_data.hpp"


class configure {
  public:
    /**Constructor with default parameter values**/
    configure(){ set_defaults(); }

    /**Formatting various modules**/
    ptyconf         wconfig;
    data_format     data_in;
    pproc_format    pproc_in;
    guess_format    guess_in;
    engine_format   engin_in;
    print_format    print_out;

    /**Run a full reconfiguration**/
    void run_reconfig(std::string,std::string);

  protected:
    void set_defaults();
    void parse_inifile(std::string);
    void parse_iniline(std::string,std::string);
    void parse_scanlist(std::string,int);
    void check_config();
    bool contains(std::vector<std::string> my_list, std::string my_string) {
        return (std::find(my_list.begin(), my_list.end(), my_string) != my_list.end());
    }
};

#endif
