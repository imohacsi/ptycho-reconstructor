#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sys/stat.h>
#include "config.hpp"


/**Run a full reconfiguration**/
void configure::run_reconfig(std::string configfile, std::string scanfile) {
	this->parse_inifile(configfile);
	this->parse_scanlist(scanfile,data_in._num_scans);
	this->check_config();
	this->wconfig._scan_id = this->data_in._scan_list[0]._scan_numbr;

}


/**SanityCheck: ensures that the read parameters do not
*   contradict each other or no key parameters are missing.
*   Always gives preference to conditional parameters
*   compared to the related parameters with actual values.
*   DOES NOT CHECK FOR EVERYTHING!
*/
void configure::check_config() {

	/**We need at least one scan to reconstruct**/
	if( !(data_in._num_scans>0) ) {
		data_in._num_scans=1;
	}

	/**If not binning, reset binvalue**/
	if( data_in._do_image_binning==false ) {
		data_in._image_binvalue=1;
	}
	/**If binning, change effective pixel size**/
	if( data_in._do_image_binning==true ) {
		wconfig._det_pixelsize=double(data_in._image_binvalue)*wconfig._det_pixelsize;
		wconfig._dPX=wconfig._det_distance*wconfig._lambda/(wconfig._det_pixelsize*wconfig._num_pixels);
	}

	/**If the line length was not given, revert to the more flexible algorithm*/
	if( data_in._image_format=="CBF_FAST" ) {
		if( data_in._frame_per_line<=0 ) {
			data_in._image_format=="CBF";
		}
	}

	/**Declare valid parameter values**/
	std::vector<std::string> valid_metaformat {"P11NEW","CXI"};
	std::vector<std::string> valid_metasensor {"ENC","IFM"};
	std::vector<std::string> valid_imagformat {"CBF","CBF_FAST","TIFF","HDF5","CXI"};
	std::vector<std::string> valid_initprobetype {"ZP","MLL","PH","GS","RND","LOAD"};
	std::vector<std::string> valid_initmodescale {"FLAT","LINEAR","QUADR","SYM_LINEAR","SYM_QUADR"};
	std::vector<std::string> valid_initobjectype {"FLAT","RND","LOAD"};
	std::vector<std::string> valid_initpsitype {"PRO","OXP"};
	std::vector<std::string> valid_algorithm {"UNST_CPU","DM_CPU","EPIE_CPU","EPIE_GPU","MAXLIKE_CPU","MAXLIKE_GPU"};

	if( !contains(valid_metaformat,data_in._position_format)) {
		printf("ERROR: Unknown metadata style/format!\n");
		exit(EXIT_FAILURE);
	}

	if( !contains(valid_metasensor,data_in._position_sensor)) {
		printf("ERROR: Unknown sensor type!\n");
		exit(EXIT_FAILURE);
	}

	if( !contains(valid_imagformat,data_in._image_format)) {
		printf("ERROR: Unknown image format!\n");
		exit(EXIT_FAILURE);
	}

	if( !contains(valid_initprobetype,guess_in._ipro_init_type)) {
		printf("ERROR: Unknown initial probe type!\n");
		exit(EXIT_FAILURE);
	}

	if( !contains(valid_initmodescale,guess_in._ipro_scaling_rule)) {
		printf("ERROR: Unknown initial probe mode scaling %s!\n",guess_in._ipro_scaling_rule.c_str());
		exit(EXIT_FAILURE);
	}

	if( !contains(valid_initobjectype,guess_in._iobj_init_type)) {
		printf("ERROR: Unknown initial object type!\n");
		exit(EXIT_FAILURE);
	}

	if( !contains(valid_initpsitype,guess_in._ipsi_init_type)) {
		printf("ERROR: Unknown initial view type!\n");
		exit(EXIT_FAILURE);
	}

	if( !contains(valid_algorithm,engin_in.algorithm_rec)) {
		printf("%sERROR: Unknown reconstruction algorithm!%s\n",A_C_RED,A_C_RESET);
		exit(EXIT_FAILURE);
	}
	if( !contains(valid_algorithm,engin_in.algorithm_ref)) {
		printf("%sERROR: Unknown refinement algorithm!%s\n",A_C_RED,A_C_RESET);
		exit(EXIT_FAILURE);
	}
}

