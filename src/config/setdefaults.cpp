#include "config.hpp"

/**Set default parameter values if they wont be reassigned later**/
void configure::set_defaults() {

	/**Reconstruction parameters*/
	wconfig._run_name="run";
	wconfig._num_THREADS=4;
	wconfig._num_pixels=128;
	wconfig._border=32;
	wconfig._num_omodes=1;
	wconfig._num_pmodes=1;
	wconfig._num_rec_iter=100;
	wconfig._num_ref_iter=0;
	wconfig._batch_size = 16;
	wconfig._cutoff = 1e26;

	/**Loading experimental conditions*/
	wconfig._energy=6200;
	wconfig._det_distance=4.2;
	wconfig._det_pixelsize=172e-6;


	/**Image loading parameters*/
	wconfig._tot_nscans=1;
	data_in._image_format="TIFF";        /**Image input format: TIFF, CBF or HDF5*/
	data_in._do_image_binning=false;
	data_in._image_binvalue=1;
	data_in._frame_per_line=-1;
	/**Detector masking parameters*/
	data_in._mask_file ="./input/ValidPixelMask.tif";

	/**Metadata loading parameters*/
	data_in._position_format = "P11NEW";  /**Position input format: CXI, P11NEW*/
	data_in._position_sensor = "ENC";       /**Position sensor: ENC, IFM*/
	/**Debuging parameters*/
	data_in._diag_print_imag = false;
	data_in._diag_print_meta = false;
	data_in._diag_print_mask = false;

	/**Seting preprocessor conditions, including stitching*/
	pproc_in._do_share_object=false;
	pproc_in._do_share_probe=false;
	pproc_in._diag_print_ampli=false;
	pproc_in._diag_print_xypos=false;
	pproc_in._diag_print_lists=false;

	/**Initial guess parameters*/
	guess_in._ipro_init_type = "ZP";      /**Initial probe guess type: ZP, PH, GS or LOAD*/
	guess_in._ipro_scaling_rule = "FLAT";    /**Scaling of initial probe modes: FLAT, LINEAR, QUADRATIC*/
	guess_in._ipro_mmode_placement = "CROSS";    /**Placement of initial probe modes: HOR, VER, FOC, CROSS*/
	guess_in._ipro_diameter =  1.0e-6;
	guess_in._ipro_distance = 1.0e-3;
	guess_in._ipro_mmode_speed = 100.0e-9;
	guess_in._ipro_mmode_focal = 100.0e-6;
	guess_in._ipro_do_saltmodes = false;
	guess_in._ipro_do_orthmodes = false;
	guess_in._ipro_do_intnorm =true;
	guess_in._iobj_init_type = "FLAT";
	guess_in._ipsi_init_type = "PRO";

	/**Reconstruction engine parameters*/
	engin_in.algorithm_rec = "NEW_D_DM";
	engin_in.algorithm_ref = "NEW_D_MAXLIKE";

//	engin_in.engcom._opupd_niter = 10;
//	engin_in.engcom._opupd_regpro = 0.1;
//	engin_in.engcom._start_proupd = 1;
//	engin_in.engcom._start_objupd = 0;
//	engin_in.engcom._lrate_obj = 0.01;
//	engin_in.engcom._lrate_pro = 0.01;
//	engin_in.engcom._optim_alg = "MOMENTUM";
//	engin_in.engcom._optim_decay = 100;
//	engin_in.engcom._magupdate_reg=0.001;

	engin_in.engcom._magupdate_reg = 0.9;
	engin_in.engcom._magupdate_lvl = 0;
	engin_in.engcom._viewsel_lvl = 1;
	engin_in.engcom._WriteEveryNthIter = 5;
	engin_in.engcom._do_calc_lover = true;
	engin_in.engcom._do_calc_error = true;
	engin_in.engcom._do_calc_print = true;

	engin_in.auxcom._do_posref = true;
	engin_in.auxcom._posref_maxjump = 10;
	engin_in.auxcom._posref_nstep = 5;
	engin_in.auxcom._posref_niter = 10;
	engin_in.auxcom._lrate_posref = 0.25;


	/**TODO:
	* Implement these functions!
	**/
	engin_in.engcom._do_weak_obj = false;
	engin_in.engcom._do_prsup_r = false;
	engin_in.engcom._prsup_r = 0.8;
	engin_in.engcom._do_prsup_q = false;
	engin_in.engcom._prsup_q = 128;
	engin_in.engcom._do_bgref_q = false;
	engin_in.engcom._bgref_alg = "paper";
	engin_in.engcom._bgref_q = 32;
	engin_in.engcom._bgref_start = 49;
	engin_in.engcom._bgref_stop = 149;
	engin_in.engcom._bgref_every = 10;
	engin_in.engcom._do_patt_scale = false;
	engin_in.engcom._mbatch_alg = 0;

	/**Loading output parameters*/
	print_out._path_results="./Reconstructions";
	print_out._do_results_tiff=true;
	print_out._do_results_hdf5=false;
	print_out._do_results_binf=true;

	/**Passing some parameters around*/
	wconfig._lambda=1.0e-9*1240.0/wconfig._energy;
	wconfig._dPX=wconfig._det_distance*wconfig._lambda/(wconfig._det_pixelsize*wconfig._num_pixels);
	data_in._num_pixels=wconfig._num_pixels;
	data_in._num_scans=wconfig._tot_nscans;
}




