#include "./print_data.hpp"
#include "../utilities/fileio.hpp"
#include "../utilities/fft_utils.hpp"
#include <fftw3.h>
#include "../utilities/h5_utils.hpp"


void print_double_data::WriteHDF5out() {

	std::vector<int64_t> dsize;
	char filename[1024];
	char ipath[1024];

	/* Create a new file using default properties. */
	sprintf(filename,"%s/OnP_%s.h5",_pconf._path_results.c_str(),_Wconf._batch_name);
	h5py h5savefile(filename,"w");

	dsize= { _Wconf._OY,_Wconf._OX };
	for(int nob=0; nob<_Wconf._num_obj; nob++) {
		for(int oo=0; oo<_Wconf._num_omodes; oo++) {
			sprintf(ipath,"/object_o%d_m%d",nob,oo);
			h5savefile.dset_write_cdouble(_rdata.OBJ[nob][oo].dvec,dsize,ipath);
			sprintf(ipath,"/object_ann_o%d_m%d",nob,oo);
			h5savefile.dset_write_cdouble(_Rdata.OBJ[nob][oo].dvec,dsize,ipath);
		}
	}

	dsize= { _Wconf._num_pixels,_Wconf._num_pixels };
	for(int npr=0; npr<_Wconf._num_obj; npr++) {
		for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
			sprintf(ipath,"/probe_p%d_m%d",npr,pp);
			h5savefile.dset_write_cdouble(_rdata.PRO[npr][pp].dvec,dsize,ipath);
			sprintf(ipath,"/probe_ann_p%d_m%d",npr,pp);
			h5savefile.dset_write_cdouble(_Rdata.PRO[npr][pp].dvec,dsize,ipath);
		}
	}

	/**Vectorize 2D position data**/
	dsize = {2, _Wconf._num_frames };
	double *vpos2D = new double[2*_Wconf._num_frames];
	for(int ff=0; ff<_Wconf._num_frames; ff++) {
		vpos2D[ff] = _rdata._pos2D[ff][0];
		vpos2D[ff+_Wconf._num_frames] = _rdata._pos2D[ff][1];
	}
	h5savefile.dset_write_double(vpos2D,dsize,(char*)"/pos_2d");

	/**Vectorize 1D position data**/
	dsize = {1, _Wconf._num_frames };
	int *vpos1D =  new int[_Wconf._num_frames];
	for(int ff=0; ff<_Wconf._num_frames; ff++) {
		vpos1D[ff]= _rdata._pos1D[ff];
	}
	h5savefile.dset_write_int(vpos1D,dsize,(char*)"/pos_1d");

	/**Add single value parameters**/
	h5savefile.dset_write_double( _Wconf._dPX, (char*)"/pixel_size");
	h5savefile.dset_write_double( _Wconf._energy, (char*)"/energy");
	h5savefile.dset_write_int( _Wconf._num_pixels, (char*)"/num_pixels");
	h5savefile.dset_write_int( _Wconf._OX, (char*)"/num_ox");
	h5savefile.dset_write_int( _Wconf._OY, (char*)"/num_oy");
}


void print_double_data::WriteBINout() {
    char filename[1024];
    for(int nob=0; nob<_Wconf._num_obj; nob++) {
        for(int oo=0; oo<_Wconf._num_omodes; oo++) {
            sprintf(filename,"%s/AnnObject_%s_O%d_M%d.bin",_pconf._path_results.c_str(),_Wconf._batch_name,nob,oo);
            WriteBinFile<std::complex<double>>(_Rdata.OBJ[nob][oo].dvec,_Wconf._OX*_Wconf._OY,filename);
        }
    }

    for(int npr=0; npr<_Wconf._num_pro; npr++) {
        for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
            sprintf(filename,"%s/Probe_%s_P%d_M%d_N%d.bin",_pconf._path_results.c_str(),_Wconf._batch_name,(int)npr,(int)pp,(int)_Wconf._num_pixels);
            WriteBinFile<std::complex<double>>(_Rdata.PRO[npr][pp].dvec,_Wconf._num_pixels*_Wconf._num_pixels,filename);
        }
    }
}

void print_double_data::WriteTIFFout() {
	char filename[1024];
	printf("Printing tiff...\n");
	fflush(stdout);
	/**Writing relaxed objects out to 16 bit uncompressed TIFF files*/
	for(int nob=0; nob<_Wconf._num_obj; nob++) {
		for(int oo=0; oo<_Wconf._num_omodes; oo++) {
			sprintf(filename,"%s/AnnObject_%s_O%d_M%d_I.tif",_pconf._path_results.c_str(),_Wconf._batch_name,nob,oo);
			ImWrite(_Rdata.OBJ[nob][oo],"lin","int","TIFF",filename);
			sprintf(filename,"%s/AnnObject_%s_O%d_M%d_P.tif",_pconf._path_results.c_str(),_Wconf._batch_name,nob,oo);
			ImWrite(_Rdata.OBJ[nob][oo],"lin","arg","TIFF",filename);
		}
	}

	/**Writing relaxed probes out to 16 bit uncompressed TIFF files*/
	for(int npr=0; npr<_Wconf._num_pro; npr++) {
		for(int pp=0; pp<_Wconf._num_pmodes; pp++) {
			sprintf(filename,"%s/AnnProbe_%s_O%d_M%d_I.tif",_pconf._path_results.c_str(),_Wconf._batch_name,npr,pp);
			ImWrite(_Rdata.PRO[npr][pp],"lin","int","TIFF",filename);
			sprintf(filename,"%s/AnnProbe_%s_O%d_M%d_P.tif",_pconf._path_results.c_str(),_Wconf._batch_name,npr,pp);
			ImWrite(_Rdata.PRO[npr][pp],"lin","arg","TIFF",filename);
		}
	}
}



/**Writes out the Fourier transform of the recovered probes in liear and logarithmic scale.*/
void print_double_data::WriteFTprobe() {
	char filename[1024];

	std::complex<double> *FIELD=(std::complex<double>*)calloc(_Wconf._num_pixels*_Wconf._num_pixels,sizeof(std::complex<double>));
	fftw_plan PlanFW;
	PlanFW=fftw_plan_dft_2d(_Wconf._num_pixels,_Wconf._num_pixels,reinterpret_cast<fftw_complex*>(FIELD),reinterpret_cast<fftw_complex*>(FIELD),FFTW_FORWARD,FFTW_ESTIMATE);


	for(int npr=0; npr<_Wconf._num_pro; npr++) {
		for(int pp=0; pp<_Wconf._num_pmodes; pp++) {

			for(int xy=0; xy<_Wconf._num_pixels*_Wconf._num_pixels; xy++) {
				FIELD[xy]=_Rdata.PRO[npr][pp][xy];
			}
			fftw_execute(PlanFW);
			fftshift(FIELD,_Wconf._num_pixels,_Wconf._num_pixels);

			sprintf(filename,"%s/FTProbe_%s_O%d_M%d_Ilin.tif",_pconf._path_results.c_str(),_Wconf._batch_name,npr,pp);
			WriteImOut(FIELD,_Wconf._num_pixels,_Wconf._num_pixels,"lin","int","TIFF",filename);
			sprintf(filename,"%s/FTProbe_%s_O%d_M%d_Ilog.tif",_pconf._path_results.c_str(),_Wconf._batch_name,npr,pp);
			WriteImOut(FIELD,_Wconf._num_pixels,_Wconf._num_pixels,"log","int","TIFF",filename);
		}
	}
}


void print_double_data::Print_Reconstruction_Results() {

	if(_pconf._do_results_tiff) {
		WriteTIFFout();
		WriteFTprobe();
	}
	if(_pconf._do_results_binf) {
		WriteBINout();
	}
	if(_pconf._do_results_hdf5) {
		WriteHDF5out();
	}
}

