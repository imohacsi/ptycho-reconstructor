#ifndef DESYPTYCHO_PRINT_DATA_H_
#define DESYPTYCHO_PRINT_DATA_H_

#include <iostream>
#include <string>
#include <vector>
#include <complex>
#include <fftw3.h>
#include <hdf5.h>
#include <omp.h>
#include "../reconstruction/worker_config.hpp"
#include "../imageprocessing/imageprocessing.h"


class print_format {
public:
	std::string _path_results;
	bool _do_results_hdf5;
	bool _do_results_tiff;
	bool _do_results_binf;
};


class print_double_data {
  public:
    ptyconf _Wconf;
    ptydata<double>   _rdata;
    ptydata<double>   _Rdata;
	print_format  _pconf;

    void Print_Reconstruction_Results();
    void WriteHDF5out();
    void WriteBINout();
    void WriteTIFFout();
    void WriteFTprobe();
    void OpenHSVmap();

//    print_double_data(ptyconf wconf, ptydata<double> rdata, ptydata<double> Rdata, dir_path path, print_format pconf): _Wconf(wconf),_rdata(rdata),_Rdata(Rdata),_path(path),_pconf(pconf) {};
    print_double_data(ptyconf wconf, ptydata<double> rdata, ptydata<double> Rdata, print_format pconf): _Wconf(wconf),_rdata(rdata),_Rdata(Rdata),_pconf(pconf) {};

};

class print_float_data {
  public:
    ptyconf _Wconf;
    ptydata<float>   _rdata;
    ptydata<float>   _Rdata;
    print_format  _pconf;

	void Print_Reconstruction_Results();
	void WriteHDF5out();
	void WriteBINout();
	void WriteTIFFout();
	void WriteFTprobe();
	void OpenHSVmap();

    print_float_data(ptyconf wconf, ptydata<float> rdata, ptydata<float> Rdata, print_format pconf): _Wconf(wconf),_rdata(rdata),_Rdata(Rdata),_pconf(pconf) {};

};


#endif

