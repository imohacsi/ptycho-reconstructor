See LICENCE for distribution conditions.

Ptychographic image reconstruction package 
by Istvan Mohacsi, 
Paul Scherrer Institute 2011-2016
University of Hamburg 2016-2018

This software is distributed without any warranty and was not used in 
any actual publication!

=== Usage information ===

==Basics==

The software was intended to be built on Linux systems using the gcc 
compiler and C++14 or later. It can be operated from the command line 
either by using only the default parameter files or by supplying 
additional parameter files that overwrite the default settings.
The first config file sets the reconstruction parameters, the second 
one sets the list of scans.

Example:
./DESY_reconstructor ./input/basicconfig.ini ./input/ScanList.dat

The config files follow the style of windows ini files and can be 
generated also from script. Results are also written out to files. 
Multiple simulations can be paralellized in a Map-Reduce paradigm.

==Functionality===

The code may contain elements of ongoing research projects and temporary solutions 


is not intended to be used by third parties or by non-expert users.







